### Tests for old bugs

# Bug report Michael Wallner 22/04/2024
rec := {(-a(n) -(n - 2)*a(n - 2)/(n - 1) + (2*n^2 - n + 2*p - 4)*a(n - 1)/((n - 1)*(n + 1)) - (-1 + p)/(n - 1)),a(1)=3/2}:
TestTools:-Try(1,gfun:-rectoproc(rec,a(n))(2),2);

# Bug report Michael Wallner 22/04/2024
rec2 := subs(p=2/3,{(-a(n) -(n - 2)*a(n - 2)/(n - 1) + (2*n^2 - n + 2*p - 4)*a(n - 1)/((n - 1)*(n + 1)) - (-1 + p)/(n - 1)),a(1)=3/2}):
TestTools:-Try(1,gfun:-rectoproc(rec2,a(n))(2),2);

### Improvements in holexprtodiffeq (Nov. 2024)

#### Test if two differential equations are the same
testdiffeq:=proc(deq1,deq2,y,z)
local eq1,eq2,Y;
    if type(deq1,set) and type(deq2,set) then
   		eq1:=op(select(has,deq1,z));
        eq2:=op(select(has,deq2,z));
    elif type(deq1,set) or type(deq2,set) then RETURN(false)
    else eq1:=deq1; eq2:=deq2 fi;
    eq1:=convert(eq1,D); eq2:=convert(eq2,D);
    RETURN(evalb(type(normal(subs((D@@3)(y)(z)=Y^4,(D@@2)(y)(z)=Y^3,
        (D)(y)(z)=Y^2,y(z)=Y,eq1/eq2)),rational)))
end:

rho:=sqrt(1-2*t*x+t^2);
F:=2^(alpha+beta)/rho*(1-t+rho)^(-alpha)*(1+t+rho)^(-beta);
deq1:=gfun:-holexprtodiffeq(F,y(t));
vals:=[alpha=1/70,beta=17/2]:
deq2:=gfun:-holexprtodiffeq(subs(vals,F),y(t));
Try[testdiffeq, y, t](3,subs(vals,deq1),deq2);

F:=sqrt(x);
deq:=gfun:-holexprtodiffeq(F,y(x));
deq2:=2*x*diff(y(x), x) - y(x);
Try[testdiffeq,y,x](4,deq,deq2);

# This used to miss the initial conditions (fixed Feb. 2025)
rho:=sqrt(1-2*t*x+t^2):
alpha:=1/3:beta:=2/7:_gamma:=11/13:
F:=hypergeom([_gamma,alpha+beta-_gamma+1],[alpha+1],(1-t-rho)/2)*hypergeom([_gamma,alpha+beta-_gamma+1],[beta+1],(1+t-rho)/2):
deq:=gfun:-holexprtodiffeq(F,y(t)):
ini:=remove(has,deq,t);
inigood:={y(0) = 1, D(y)(0) = (127655*x)/255528 + 2321/255528, (D@@2)(y)(0) = 517589963/529006842*x^2 + 5335979/264503421*x - 142948069/529006842}:
Try(5,ini,inigood);

#end test

