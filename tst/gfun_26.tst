### Tests for guessing

#### Test if two differential equations are the same
testdiffeq:=proc(deq1,deq2,y,z)
local eq1,eq2,Y;
    if type(deq1,set) and type(deq2,set) then
    eq1:=op(select(has,deq1,z));
        eq2:=op(select(has,deq2,z));
    if not testini(deq1 minus {eq1},deq2 minus {eq2}) then return false fi
    elif type(deq1,set) or type(deq2,set) then RETURN(false)
    else eq1:=deq1; eq2:=deq2 fi;
    eq1:=convert(eq1,D); eq2:=convert(eq2,D);
    RETURN(evalb(type(normal(subs((D@@3)(y)(z)=Y^4,(D@@2)(y)(z)=Y^3,
        (D)(y)(z)=Y^2,y(z)=Y,eq1/eq2)),rational)))
end:

### Tests if  L1=L2
testini:=proc(L1,L2)
local s1, s2, i, j, n, N, g1, g2, inds, var, S, vars, c0;
    if L1=L2 then RETURN(true) fi;
    # c0 because of a bug in Groebner[Basis] (maple6)
    s1:=subs(_C[0]=c0,indets(L1,_C[anything]));
    s2:=subs(_C[0]=c0,indets(L2,_C[anything]));
    if s1={} and s2={} then RETURN(false) fi;
    n:=nops(s1);
    if n<>nops(s2) then RETURN(false) fi;
    S:={seq(op(1,i),i=L1)};
    if S<>{seq(op(1,i),i=L2)} then RETURN(false) fi;
    N:=nops(L1);
    vars:=[seq(op(i,S)=var[i],i=1..nops(S))];
    g1:=remove(has,Groebner[Basis](subs(vars,_C[0]=c0,
	[seq(op(1,i)-op(2,i),i=L1)]),
	lexdeg([op(s1)],[seq(var[i],i=1..N)])),s1);
    g2:=remove(has,Groebner[Basis](subs(vars,_C[0]=c0,
	[seq(op(1,i)-op(2,i),i=L2)]),
	lexdeg([op(s2)],[seq(var[i],i=1..N)])),s1);
    evalb(g1=g2)
end:


# Bug report Alin Bostan, 27/12/2019
S:=series(cosh(w*arcsinh(x))/sqrt(x^2+1),x,20):
TestTools:-Try[testdiffeq, y, x](1,gfun:-seriestodiffeq(S,Y(x))[1],
{(-w^2+1)*Y(x)+3*x*diff(Y(x),x)+(x^2+1)*diff(diff(Y(x),x),x), Y(0) = 1, D(Y)(0) = 0}):

S:=gfun:-algeqtoseries(y-(u+z*y^2),z,y,20,pos_slopes)[1]:
TestTools:-Try[testdiffeq,y,z](2,gfun:-seriestodiffeq(S,y(z))[1],
{2*u*y(z)+(10*u*z-2)*diff(y(z),z)+z*(4*u*z-1)*diff(diff(y(z),z),z), y(0) = u, D(y)(0) = u^2});

rec:={a(n) = l*(a(n-1)+l), a(0) = l}:
L:=gfun:-rectoproc(rec,a(n),list)(20):
S:=series(add(L[i]*z^(i-1),i=1..nops(L))+O(z^(nops(L))),z,nops(L)):
TestTools:-Try[testdiffeq,y,z](3,gfun:-seriestodiffeq(S,y(z))[1],
{l*(l*z^2-z^2+2*z-2)*y(z)+(l^2*z^3-l^2*z^2-l*z^3+l*z^2+z^2-2*z+1)*diff(y(z),z),
y(0) = l});




#end test

