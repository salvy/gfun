# test

# from algebraic series
rec := gfun:-diffeqtorec(gfun:-algeqtodiffeq( z-y(z)-y(z)^5, y(z), {y(0)=0} ), y(z), u(n)):
procu := gfun:-rectoproc ( rec, u(n), remember ):
smallrec := gfun:-proctorec(procu, u, n, 64, 128):

TestTools:-Try(1, collect(smallrec,u,normal),
               [{(3125*n^4+12500*n^3+13750*n^2+2500*n-1155)*u(n)+(256*n^4+2560*n^3+8960*n^2+12800*n+6144)*u(n+4), u(0) = 0, u(1) = 1, u(2) = 0, u(3) = 0}, 64]);
