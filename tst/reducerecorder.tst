# Basic tests on reduction of order

# rec from poltorec
reca := {-n*a(n)+a(1+n)+(n+3)*a(n+2), a(0) = 1, a(1) = -1/2}:
rec := gfun:-poltorec( a(n+3)*a(n+2)^2*a(n+4), [reca], [a(n)], u(n)):
smallrec := gfun:-reducerecorder(rec, u, n, 64):

expected := {-(n+3)*(2*n^2+19*n+43)*(n+2)^2*u(n)-(2*(n+4))*(n^3+13*n^2+54*n+68)*u(1+n)+(n+5)*(7+n)*(6+n)*(2*n^2+15*n+26)*u(n+2), u(0) = -1/2160, u(1) = -1/3600}:

TestTools:-Try( 1, collect(smallrec,u,factor), expected );

# rec from series
rec := gfun:-diffeqtorec( gfun:-algeqtodiffeq(
   (z+y(z)+y(z)^3+y(z)^4+y(z)^5), y(z),{y(0)=0}), y(z), u(n)):
smallrec := gfun:-reducerecorder(rec, u, n):

expected := {(5*(5*n+1))*(5*n+2)*(5*n+3)*(5*n-1)*(19301875*n^3+93716025*n^2+146110520*n+73716864)*u(n)-(n+1)*(30767188750*n^6+195534126975*n^5+468140126530*n^4+531784963011*n^3+294250399054*n^2+72165835776*n+5783442624)*u(n+1)+(3*(n+2))*(n+1)*(12604124375*n^5+73800688700*n^4+153826812785*n^3+136979122642*n^2+48750580514*n+5497455144)*u(n+2)+(n+3)*(n+2)*(n+1)*(2740866250*n^4+11937242425*n^3+16759805015*n^2+8320276838*n+1186877592)*u(n+3)+(257*(n+4))*(n+3)*(n+2)*(n+1)*(19301875*n^3+35810400*n^2+16584095*n+2020494)*u(n+4), u(0) = 0, u(1) = -1, u(2) = 0, u(3) = 1, u(4) = -1}:

TestTools:-Try( 2, collect(smallrec,u,factor), expected );
