#test gfun:-pade2 
#

# These are the tests from ?numapprox,hermite_pade
ARGS:=[sin(x),cos(x),exp(x)],x=0,[3,2,5]:
TestTools:-Try(1,gfun:-pade2(ARGS),numapprox[hermite_pade](ARGS));

ARGS:=[sin(x),cos(x)],x=Pi,7:
TestTools:-Try(2,gfun:-pade2(ARGS),numapprox[hermite_pade](ARGS));

ARGS:=[cos(2*x)*(x+1)+3,cos(x)^2+x*cos(x)+1,cos(2*x)+1,cos(x)],x=0,20:
TestTools:-Try(3,gfun:-pade2(ARGS),-numapprox[hermite_pade](ARGS));

#end test
                       