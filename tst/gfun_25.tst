### Tests for rectoproc

# several problems fixed in 3.79 - 3.80
# bug reports by Alexandre Benoit, Sébastien Maulat, Michael Wallner
# plus other bugs found while fixing.

## options:
#        'remember'
#        'list'    
#        'params'=[n1,n2,...] 
#        'evalfun'=name       
#        'preargs'=[a1,...]   
#        'postargs'=[b1,...]  
#        'whilecond'=boolean  
#        'errorcond'=boolean  
#        'index'   
#        'rhs'     
#        'evalinicond' 
#        'extralocal'=[a=$a,b=$b...] 
#        'nosymbolsubs'  
#        'optimize'       
#        'copyright'=string
#        'maximalindex = nonnegint' 
#        'plain' 

rec := {u(n)*a+u(n+2), u(0) = 1,u(1) = 2,u(2)=3,u(3)=4}:

pp:=gfun:-rectoproc(rec,u(n),list):
L:=[1, 2, 3, 4, -3*a, -4*a, 3*a^2, 4*a^2, -3*a^3, -4*a^3, 3*a^4]:

TestTools:-Try(1,pp(10),L);
ind:=1:

# ind = 2 to 11
for opt in [remember,evalfun=expand,optimize,plain,nosymbolsubs] do
ind:=ind+1;
    TestTools:-Try(ind,gfun:-rectoproc(rec,u(n),list,opt)(10),L);
ind:=ind+1;
    pp:=gfun:-rectoproc(rec,u(n),opt);
    TestTools:-Try(ind,[seq(pp(i),i=0..10)],L);
od;

# Same with polynomial coefficients
rec := {u(n)*n+u(n+2), u(0) = 1,u(1) = 2,u(2)=3,u(3)=4}:
L:=[1, 2, 3, 4, -6, -12, 24, 60, -144, -420, 1152]:

ind:=ind+1;
pp:=gfun:-rectoproc(rec,u(n),list):
TestTools:-Try(ind,pp(10),L);

# ind = 13 to 24
for opt in [remember,evalfun=expand,optimize,plain,nosymbolsubs] do
ind:=ind+1;
    TestTools:-Try(ind,gfun:-rectoproc(rec,u(n),list,opt)(10),L);
ind:=ind+1;
    pp:=gfun:-rectoproc(rec,u(n),opt);
    TestTools:-Try(ind,[seq(pp(i),i=0..10)],L);
od;

# Same with constant coefficients
rec := {u(n)-u(n+1)+u(n+2), u(0) = 1,u(1) = 2,u(2)=3,u(3)=4}:
L:=[1, 2, 3, 4, 1, -3, -4, -1, 3, 4, 1, -3, -4, -1, 3, 4, 1, -3, -4, -1, 3]:

ind:=ind+1;
pp:=gfun:-rectoproc(rec,u(n),list):
TestTools:-Try(ind,pp(20),L);

# ind = 26 to 37
for opt in [remember,evalfun=expand,optimize,plain,nosymbolsubs] do
ind:=ind+1;
    TestTools:-Try(ind,gfun:-rectoproc(rec,u(n),list,opt)(20),L);
ind:=ind+1;
    pp:=gfun:-rectoproc(rec,u(n),opt);
    TestTools:-Try(ind,[seq(pp(i),i=0..20)],L);
od;

## option index
rec := {u(n)*a+u(n+2), u(0) = 1,u(1) = 2,u(2)=3,u(3)=4}:

pp:=gfun:-rectoproc(rec,u(n),list):
L:=[1, 2, 3, 4, -3*a, -4*a, 3*a^2, 4*a^2, -3*a^3, -4*a^3, 3*a^4]:
L:=[seq([i,L[i+1]],i=0..nops(L)-1)]:

ind:=ind+1:
pp:=gfun:-rectoproc(rec,u(n),index):
TestTools:-Try(ind,[seq(pp(i),i=0..nops(L)-1)],L);

ind:=ind+1:
TestTools:-Try[testerror](ind,gfun:-rectoproc(rec,u(n),index,remember),
    "index and remember cannot be used simultaneously"):

ind:=ind+1:
TestTools:-Try[testerror](ind,gfun:-rectoproc(rec,u(n),remember,index),
    "index and remember cannot be used simultaneously"):

ind:=ind+1:
rec := {(t+1)*w[0](t)+(-3*n-t*n)*w[0](t+1)+(n^3*t-n*t^3-9*t^2*n-27*t*n+3*n^3-27*n)*w[0](t+3)+(-t*n^2+8*t+5*t^2+t^3-n^2+4)*w[0](t+2), w[0](0) = 1, w[0](1) = -n/(-1+n^2), w[0](2) = 1/(n^2-4)}:

# used to be a division by 0
TestTools:-Try(ind,normal(gfun:-rectoproc(rec,w[0](t))(4)),1/(n^2-16)/(n^2-4));

ind:=ind+1:
TestTools:-Try[testerror](ind,gfun:-rectoproc(1,u(n)),"not a recurrence");

ind:=ind+1:
r1 := {HH(k+2) = 18*HH(k), HH(0) = -(16/3)*x^3, HH(1) = (256/735)*x^7}:
# These two options did not work well together
f:=gfun:-rectoproc(r1,HH(k),evalfun=expand,remember,list):
L:=[-(16*x^3)/3, (256*x^7)/735, -96*x^3, (1536*x^7)/245, -1728*x^3, (27648*x^7)/245, -31104*x^3, (497664*x^7)/245, -559872*x^3, (8957952*x^7)/245, -10077696*x^3]:
TestTools:-Try(ind,f(10),L);


#end test

