# Bug fixed by MM. May 2012.

p := gfun:-rectoproc((n-1)*u(n), u(n)):
TestTools:-Try(1,[seq(p(i),i=0..5)],[0, _C[0], 0, 0, 0, 0]);

p := gfun:-rectoproc(u(n), u(n)):
TestTools:-Try(1,[seq(p(i),i=0..5)],[0, 0, 0, 0, 0, 0]);

# Bug in NumGfun affecting rectoproc. Fixed by MM. Jul 2012.

unassign(uu);
p := gfun:-rectoproc({n*uu[0](n)+uu[0](n+2), uu[0](0)=1, uu[0](1)=0},uu[0](n));
TestTools:-Try(1,assigned(uu),false);

#end test

