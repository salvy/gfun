#Fix a problem with Laplace being both a local and a global, which made
#listtoseries/listtolist/... fail depending on whether gfun had been
#loaded using "with" or not.

#test

TestTools:-Try(1,gfun[listtolist]([1,2,3],Laplace),[1,2,6]);
TestTools:-Try(2,gfun[listtolist]([1,2,3],gfun:-Laplace),[1,2,6]);
with(gfun):
TestTools:-Try(3,gfun[listtolist]([1,2,3],Laplace),[1,2,6]);
TestTools:-Try(4,listtolist([1,2,3],Laplace),[1,2,6]);
TestTools:-Try(5,listtolist([1,2,3],gfun:-Laplace),[1,2,6]);

#end test
