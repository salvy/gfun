### Test if two recurrences are the same
testrec:=proc(rec1,rec2,u,n)
local eq1,eq2,Y;
    if type(rec1,set) and type(rec2,set) then
        eq1:=op(select(has,rec1,n));
        eq2:=op(select(has,rec2,n));
	if not testini(rec1 minus {eq1},rec2 minus {eq2}) then return false fi
    elif type(rec1,set) or type(rec2,set) then RETURN(false)
    else eq1:=rec1; eq2:=rec2 fi;
    RETURN(evalb(type(normal(subs(u(n+3)=Y^4,u(n+2)=Y^3,
        u(n+1)=Y^2,u(n)=Y,eq1/eq2)),rational)))
end:

### Tests if  L1=L2
testini:=proc(L1,L2) 
local s1, s2, i, j, n, N, g1, g2, inds, var, S, vars, c0;
    if L1=L2 then RETURN(true) fi;
    # c0 because of a bug in Groebner[Basis] (maple6)
    s1:=subs(_C[0]=c0,indets(L1,_C[anything]));
    s2:=subs(_C[0]=c0,indets(L2,_C[anything]));
    if s1={} and s2={} then RETURN(false) fi;
    n:=nops(s1);
    if n<>nops(s2) then RETURN(false) fi;
    S:={seq(op(1,i),i=L1)};
    if S<>{seq(op(1,i),i=L2)} then RETURN(false) fi;
    N:=nops(L1);
    vars:=[seq(op(i,S)=var[i],i=1..nops(S))];
    g1:=remove(has,Groebner[Basis](subs(vars,_C[0]=c0,
	[seq(op(1,i)-op(2,i),i=L1)]),
	lexdeg([op(s1)],[seq(var[i],i=1..N)])),s1);
    g2:=remove(has,Groebner[Basis](subs(vars,_C[0]=c0,
	[seq(op(1,i)-op(2,i),i=L2)]),
	lexdeg([op(s2)],[seq(var[i],i=1..N)])),s1);
    evalb(g1=g2)
end:

#### Test if two differential equations are the same
testdiffeq:=proc(deq1,deq2,y,z)
local eq1,eq2,Y;
    if type(deq1,set) and type(deq2,set) then
    eq1:=op(select(has,deq1,z));
        eq2:=op(select(has,deq2,z));
    if not testini(deq1 minus {eq1},deq2 minus {eq2}) then return false fi
    elif type(deq1,set) or type(deq2,set) then RETURN(false)
    else eq1:=deq1; eq2:=deq2 fi;
    eq1:=convert(eq1,D); eq2:=convert(eq2,D);
    RETURN(evalb(type(normal(subs((D@@3)(y)(z)=Y^4,(D@@2)(y)(z)=Y^3,
        (D)(y)(z)=Y^2,y(z)=Y,eq1/eq2)),rational)))
end:

# Bug reported by Sébastien Maulat. Fixed BS. Feb 2014.
H := [x^2, -(4/9)*x^3, (4/25)*x^4, -(64/1225)*x^5, (64/3969)*x^6,
-(256/53361)*x^7, (256/184041)*x^8, -(16384/41409225)*x^9,
(16384/147744025)*x^10, -(65536/2133423721)*x^11,
(65536/7775536041)*x^12, -(1048576/457028729521)*x^13,
(1048576/1690195005625)*x^14, -(4194304/25145962430625)*x^15]:

# used to give a wrong recurrence due to a weakness in the probabilistic check:
# the prime used for x and for checking were too close so that x mod p = 30 and this
# was a wrong factor in the recurrence.

TestTools:-Try[testrec,u,n](1,gfun:-listtorec(H,u(n),[ogf])[1],
	{(n^2*x+4*n*x+4*x)*u(n)+(4*n^2+12*n+9)*u(n+1), u(0) = x^2});

# Bug reported by Paul Zimmermann and S. Maulat. Fixed BS. Feb 2014

# used to return u(n+1)
TestTools:-Try[testrec, u, n](2,gfun:-listtorec([0$20],u(n),[ogf])[1],{u(n),u(0)=0});

# Used to end with "unable to execute seq". Fixed BS. March 2014.
TestTools:-Try[testerror](3,gfun:-poltorec(a(n)*b(n),[a(n)+2*a(n+1),b(n+1)-3*b(n)],[b(n),a(n)],u(n)),"invalid recurrence or unknown");

# Used to end with "unable to execute seq". Fixed BS. March 2014.
TestTools:-Try[testerror](4,gfun:-rectoproc(1,u(n),list)(5),"not a recurrence");

# Reported in March 2011 by Alexandre Benoit. Fixed BS. July 2014.
rec:= {(3+4*n^2+8*n)*u(n)+(-33-40*n-12*n^2)*u(n+1)+(36*n+40+8*n^2)*u(n+2),u(0) = I*2^(1/2), u(1) = 5/12*I*2^(1/2)}:
v := gfun[rectoproc](rec, u(n), list)(20):
TestTools:-Try[testrec,u,n](5,gfun[listtorec](v, u(n),[ogf])[1],
{(4*n^2+8*n+3)*u(n)+(-12*n^2-40*n-33)*u(n+1)+(8*n^2+36*n+40)*u(n+2), u(0) = I*2^(1/2), u(1) = ((5/12)*I)*2^(1/2)});

# variant
L:=[series(-(((2/3)*I)*Pi)*z-((4/15)*Pi^2)*z^3+(((2/35)*I)*Pi^3)*z^5+((8/945)*Pi^4)*z^7-(((2/2079)*I)*Pi^5)*z^9-((4/45045)*Pi^6)*z^11+(((2/289575)*I)*Pi^7)*z^13+((16/34459425)*Pi^8)*z^15-(((2/72747675)*I)*Pi^9)*z^17-((4/2749862115)*Pi^10)*z^19+O(z^20),z,20), series(1-(((1/3)*I)*Pi)*z^2-((1/15)*Pi^2)*z^4+(((1/105)*I)*Pi^3)*z^6+((1/945)*Pi^4)*z^8-(((1/10395)*I)*Pi^5)*z^10-((1/135135)*Pi^6)*z^12+(((1/2027025)*I)*Pi^7)*z^14+((1/34459425)*Pi^8)*z^16-(((1/654729075)*I)*Pi^9)*z^18-((1/13749310575)*Pi^10)*z^20+O(z^21),z,21), 1]:
testsimple:=proc(a,b) evalb(expand(a)=expand(b)) end:
TestTools:-Try[testsimple](6,gfun:-pade2(L,z,20),
[z,1+I*Pi*z^2,-1]);

nb:=20:
S:=series(hypergeom([a,b],[a+b+1/2],z),z,nb):
L:=[S,map(normal,series(subs(a=a+1,S)+subs(b=b+1,S),z,nb)),map(normal,series(subs(a=a+2,S)+subs(b=b+2,S)+2*subs(a=a+1,b=b+1,S),z,nb))]:
res:=[-16*a^6*z+16*a^4*b^2*z+16*a^2*b^4*z-16*b^6*z+32*a^6+64*a^5*b-56*a^5*z+96*a^4*b^2+8*a^4*b*z+128*a^3*b^3+48*a^3*b^2*z+96*a^2*b^4+48*a^2*b^3*z+
64*a*b^5+8*a*b^4*z+32*b^6-56*b^5*z+160*a^5+416*a^4*b-68*a^4*z+704*a^3*b^2+24*a^3*b*z+704*a^2*b^3+88*a^2*b^2*z+416*a*b^4+24*a*b^3*z+160*b^5-68*
b^4*z+400*a^4+1280*a^3*b-34*a^3*z+1856*a^2*b^2+34*a^2*b*z+1280*a*b^3+34*a*b^2*z+400*b^4-34*b^3*z+704*a^3+2080*a^2*b-6*a^2*z+2080*a*b^2+12*a*b*
z+704*b^3-6*b^2*z+810*a^2+1644*a*b+810*b^2+486*a+486*b+108, 16*a^6*z-16*a^4*b^2*z-16*a^2*b^4*z+16*b^6*z-16*a^6-32*a^5*b+56*a^5*z-48*a^4*b^2-40
*a^4*b*z-64*a^3*b^3-48*a^3*b^2*z-48*a^2*b^4-48*a^2*b^3*z-32*a*b^5-40*a*b^4*z-16*b^6+56*b^5*z-80*a^5-208*a^4*b+44*a^4*z-352*a^3*b^2-120*a^3*b*z
-352*a^2*b^3-88*a^2*b^2*z-208*a*b^4-120*a*b^3*z-80*b^5+44*b^4*z-200*a^4-640*a^3*b-38*a^3*z-928*a^2*b^2-122*a^2*b*z-640*a*b^3-122*a*b^2*z-200*b
^4-38*b^3*z-352*a^3-1040*a^2*b-60*a^2*z-1040*a*b^2-60*a*b*z-352*b^3-60*b^2*z-405*a^2-822*a*b-18*a*z-405*b^2-18*b*z-243*a-243*b-54, 2*(8*a^4*b^
2+8*a^2*b^4+20*a^4*b+24*a^3*b^2+24*a^2*b^3+20*a*b^4+12*a^4+60*a^3*b+44*a^2*b^2+60*a*b^3+12*b^4+36*a^3+61*a^2*b+61*a*b^2+36*b^3+33*a^2+30*a*b+
33*b^2+9*a+9*b)*z]:
TestTools:-Try[testsimple](7,gfun[pade2](L,z,nb-1),res);


# Used to return 0. Fixed BS in 3.71. June 2015.
rec:={(n+beta+1)*(alpha+1+n)*(alpha+beta+2*n+4)*(N-n)*(N+n+2+
alpha+beta)*Wbis(n)+(2*n+3+alpha+beta)*(N*alpha^2+N*alpha*beta+2*N*alpha*n+2*N*beta*n+2*N*n^2-
alpha^2*n-alpha^2*x-2*alpha*beta*x-alpha*n^2-4*alpha*n*x+beta^2*n-beta^2*x+beta*n^2-4*beta*n*x-4
*n^2*x+3*N*alpha+3*N*beta+6*N*n-alpha^2-3*alpha*n-6*alpha*x+beta^2+3*beta*n-6*beta*x-12*n*x+4*N-\
2*alpha+2*beta-8*x)*Wbis(n+1)+(n+2)*(n+2+alpha+beta)*(2*n+alpha+beta+2)*Wbis(n+2), Wbis(0) = 1,
Wbis(1) = (2+alpha+beta)*x-(alpha+1)*N}:
# used not to return an initial condition. Fixed BS. Nov. 2024
deq:=
{(N^2*alpha^2*beta*t+N^2*alpha*beta^2*t+N*alpha^3*beta*t+2*N*alpha^2*beta^2*t+N*alpha*beta^3*t+N^
2*alpha^2*t+6*N^2*alpha*beta*t+N^2*beta^2*t+N*alpha^3*t+9*N*alpha^2*beta*t+9*N*alpha*beta^2*t+N*
beta^3*t+5*N^2*alpha*t+5*N^2*beta*t+N*alpha^3+2*N*alpha^2*beta+7*N*alpha^2*t+N*alpha*beta^2+22*N
*alpha*beta*t+7*N*beta^2*t-alpha^3*x-3*alpha^2*beta*x-3*alpha*beta^2*x-beta^3*x+4*N^2*t+2*N*
alpha^2+3*N*alpha*beta+14*N*alpha*t+N*beta^2+14*N*beta*t-3*alpha^2*x-6*alpha*beta*x-3*beta^2*x+N
*alpha+N*beta+8*N*t-2*alpha*x-2*beta*x)*y(t)+(N^2*alpha^2*t^2+4*N^2*alpha*beta*t^2+N^2*beta^2*t^
2+N*alpha^3*t^2+5*N*alpha^2*beta*t^2+5*N*alpha*beta^2*t^2+N*beta^3*t^2-alpha^3*beta*t^2-2*alpha^
2*beta^2*t^2-alpha*beta^3*t^2+11*N^2*alpha*t^2+11*N^2*beta*t^2+13*N*alpha^2*t^2+30*N*alpha*beta*
t^2+13*N*beta^2*t^2-2*alpha^3*t^2-15*alpha^2*beta*t^2-15*alpha*beta^2*t^2-2*beta^3*t^2+20*N^2*t^
2+4*N*alpha^2*t+6*N*alpha*beta*t+42*N*alpha*t^2+2*N*beta^2*t+42*N*beta*t^2-alpha^3*t-alpha^2*
beta*t-22*alpha^2*t^2-6*alpha^2*t*x+alpha*beta^2*t-62*alpha*beta*t^2-12*alpha*beta*t*x+beta^3*t-\
22*beta^2*t^2-6*beta^2*t*x+12*N*alpha*t+12*N*beta*t+40*N*t^2-5*alpha^2*t-72*alpha*t^2-24*alpha*t
*x+5*beta^2*t-72*beta*t^2-24*beta*t*x+12*N*t+alpha^2+2*alpha*beta-6*alpha*t+beta^2+6*beta*t-72*t
^2-24*t*x+alpha+beta)*diff(y(t),t)+(3*N^2*alpha*t^3+3*N^2*beta*t^3+3*N*alpha^2*t^3+6*N*alpha*
beta*t^3+3*N*beta^2*t^3-alpha^3*t^3-6*alpha^2*beta*t^3-6*alpha*beta^2*t^3-beta^3*t^3+14*N^2*t^3+
20*N*alpha*t^3+20*N*beta*t^3-23*alpha^2*t^3-60*alpha*beta*t^3-23*beta^2*t^3+6*N*alpha*t^2+6*N*
beta*t^2+28*N*t^3-3*alpha^2*t^2-132*alpha*t^3-12*alpha*t^2*x+3*beta^2*t^2-132*beta*t^3-12*beta*t
^2*x+18*N*t^2-9*alpha*t^2+9*beta*t^2-216*t^3-36*t^2*x+3*alpha*t+3*beta*t+4*t)*diff(diff(y(t),t),
t)+(2*N^2*t^4+2*N*alpha*t^4+2*N*beta*t^4-4*alpha^2*t^4-10*alpha*beta*t^4-4*beta^2*t^4+4*N*t^4-52
*alpha*t^4-52*beta*t^4+4*N*t^3-2*alpha*t^3+2*beta*t^3-148*t^4-8*t^3*x+2*t^2)*diff(diff(diff(y(t)
,t),t),t)+(-5*alpha*t^5-5*beta*t^5-32*t^5)*diff(diff(diff(diff(y(t),t),t),t),t)-2*t^6*diff(diff(
diff(diff(diff(y(t),t),t),t),t),t),y(0)=1}:
TestTools:-Try(8,gfun[rectodiffeq](rec,Wbis(n),y(t),homogeneous=true),deq);

# Used to fail. Fixed BS in 3.72. June 2015.
f := exp(Int(a/((1+sqrt(-2*t+1))*sqrt(-2*t+1)), t)):
deq:={(a^2+a)*y(t)+(4*a*t-2*a+6*t-2)*(diff(y(t), t))+(4*t^2-2*t)*(diff(y(t), t, t)), y(0) = 1}:
TestTools:-Try[testdiffeq,y,t](9,gfun[holexprtodiffeq](f,y(t)),deq);

# Similar problem fixed in 3.72.
F:=(2/(1+sqrt(-2*t+1)))^a:
deq:={(a^2+a)*y(t)+(4*a*t-2*a+6*t-2)*(diff(y(t), t))+(4*t^2-2*t)*(diff(y(t), t, t)), y(0) = 1}:
TestTools:-Try[testdiffeq,y,t](9,gfun[holexprtodiffeq](F,y(t)),deq);



#end test

