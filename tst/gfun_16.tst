# test for SCR 41660
#
#test

# This did not work for versions <= 3.08
answer:={y(0) = sin(cos(tt+1/4*Pi)), x^2*y(w[1])+diff(diff(y(w[1]),w[1]),w[1]), D(y)(0) = cos(cos(tt+1/4*Pi))*x};

TestTools:-Try(1,gfun[holexprtodiffeq](sin(w[1]*x+cos(tt+Pi/4)),y(w[1])),answer);
TestTools:-Try(2,gfun[holexprtodiffeq](sin(w[1]*x+cos(tt+Pi/4)),z(w[1])),eval(answer,y=z));

#end test
