MatrixAlea := proc(a,b,d,x) LinearAlgebra[RandomMatrix](a,b,generator=proc()randpoly(x,dense,degree=d) end) end:

test:=proc(n,d)
local M, v, st1,st2, S1,S2, i;
    M := MatrixAlea(n,n,d,x):
    v := MatrixAlea(n,1,d-1,x):
    v := Vector([seq(v[i],i=1..n)]);
    st1:=time();
    S1 := gfun:-Storjohann(M,v,x);
    st1:=time()-st1;
    st2:=time();
    S2 := LinearAlgebra[LinearSolve](M,v);
    st2:=time()-st2;
    for i to n do if normal(S1[i]-S2[i])<>0 then print(M,v,S1,S2,S1[i],S2[i],normal(S1[i]-S2[i])); error "bug" fi od;
    st1,st2
end:

#kernelopts(profile=true);
#writeto("/tmp/toto13");
#res:=test(14,20);
#if res[1]>4*res[2] then error "too slow",res[1],res[2] fi;
quit

