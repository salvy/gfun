infolevel[gfun]:=4:
algeqtodiffeq(y=1+z*y^2,y(z));
algeqtodiffeq(y^2-x^2,y(x));
algeqtodiffeq(-28*x**2+3*y*x-31*y+y^2,y(x));
algeqtodiffeq(y=1+z*(y^2+y^3),y(z));
algeqtodiffeq(y^5*(1-x)=1,y(x));
algeqtodiffeq(x*y^4-1,y(x));
