{VERSION 4 0 "HELP" "4.0"}
{USTYLETAB {PSTYLE "Normal" -1 0 1 {CSTYLE "" -1 -1 "" 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 }0 0 0 -1 -1 10 0 0 0 0 0 0 -1 0 }}
{SECT 0 {PARA 3 "> " 0 "" {TEXT -1 19 "gfun[listtoratpoly]" }
{TEXT 30 39 " - find a rational generating function " }
}
{PARA 3 "> " 0 "" {TEXT -1 21 "gfun[seriestoratpoly]" }
{TEXT 30 30 " - find a rational approximant" }
}
{PARA 4 "> " 0 "usage" {TEXT -1 16 "Calling Sequence" }
}
{PARA 0 "> " 0 "" {TEXT -1 20 "     listtoratpolym(" }
{TEXT 35 1 "l" }
{TEXT -1 2 ", " }
{TEXT 35 1 "x" }
{TEXT -1 2 ", " }
{TEXT 35 12 "<[typelist]>" }
{TEXT -1 1 ")" }
}
{PARA 0 "> " 0 "" {TEXT -1 21 "     seriestoratpoly(" }
{TEXT 35 1 "s" }
{TEXT -1 2 ", " }
{TEXT 35 12 "<[typelist]>" }
{TEXT -1 2 ") " }
}
{PARA 4 "> " 0 "" {TEXT -1 10 "Parameters" }
}
{PARA 0 "> " 0 "" {TEXT -1 5 "     " }
{TEXT 23 13 "l          - " }
{TEXT -1 6 "a list" }
}
{PARA 0 "> " 0 "" {TEXT -1 5 "     " }
{TEXT 23 13 "s          - " }
{TEXT -1 8 "a series" }
}
{PARA 0 "> " 0 "" {TEXT -1 5 "     " }
{TEXT 23 13 "x          - " }
{TEXT -1 20 "the unknown variable" }
}
{PARA 0 "> " 0 "" {TEXT -1 5 "     " }
{TEXT 23 13 "[typelist] - " }
{TEXT -1 46 "(optional) a list of generating function types" }
}

{SECT  0 {PARA 4 "> " 0 "info" {TEXT -1 11 "Description" }
}
{PARA 15 "> " 0 "" {TEXT -1 15 "The procedures " }
{TEXT 35 13 "listtoratpoly" }
{TEXT -1 5 " and " }
{TEXT 35 15 "seriestoratpoly" }
{TEXT -1 32 " compute a rational function in " }
{TEXT 35 1 "x" }
{TEXT -1 51 " for the generating function of the expressions in " }
{TEXT 35 1 "l" }
{TEXT -1 4 " or " }
{TEXT 35 1 "s" }
{TEXT -1 66 ", this generating function being of one of the types specified by " }
{TEXT 35 8 "typelist" }
{TEXT -1 92 " for example, ordinary (ogf) or exponential (egf). For a full list of available choices see " }
{HYPERLNK 17 "gftypes" 2 "gfun[gftypes]" "" }{TEXT -1 3 "). " }
}
{PARA 15 "> " 0 "" {TEXT -1 33 "These functions are frontends to " }
{HYPERLNK 17 "convert[ratpoly]" 2 "convert[ratpoly]" "" }{TEXT -1 40 " which performs the actual computation. " }
}
{PARA 15 "> " 0 "" {TEXT -1 3 "If " }
{TEXT 35 8 "typelist" }
{TEXT -1 68 " contains more than one element, these types are tried in order. If " }
{TEXT 35 8 "typelist" }
{TEXT -1 28 " is not provided, a default " }
{TEXT 35 9 "optionsgf" }
{TEXT -1 1 "=" }
{TEXT 35 13 "['ogf','egf']" }
{TEXT -1 9 " is used." }
}
{PARA 15 "> " 0 "" {TEXT -1 136 "The output is a list whose second element is the type for which a solution was found, and whose first element is the rational function. " }
}
{PARA 15 "> " 0 "" {TEXT -1 54 "One should give as many terms as possible in the list " }
{TEXT 35 1 "l" }
{TEXT -1 15 " or the series " }
{TEXT 35 1 "s" }
{TEXT -1 2 ". " }
}
}
{SECT  0 {PARA 4 "> " 0 "examples" {TEXT -1 8 "Examples" }
}
{PARA 0 "> " 0 "" {TEXT -1 132 "If the input is the first few elements of the Fibonacci sequence, the the output is the generating series for the Fibonacci numbers." }
}
{EXCHG {PARA 0 "> " 0 "" {MPLTEXT 1 0 32 "with(gfun):\nl:=[1,1,2,3,5,8,13];" }
}
{PARA 11 "> " 1 "" {XPPMATH 20 "6#>%\"lG7)\"\"\"F&\"\"#\"\"$\"\"&\"\")\"#8
"}}}{EXCHG {PARA 0 "> " 0 "" {MPLTEXT 1 0 19 "listtoratpoly(l,x);" }
}
{PARA 11 "> " 1 "" {XPPMATH 20 "6#7$,$*$,(!\"\"\"\"\"%\"xGF(*$F)\"\"#F(F'F'%$ogfG
"}}}{EXCHG {PARA 0 "> " 0 "" {MPLTEXT 1 0 87 "seriestoratpoly(series(1+x+2*x^2*2!+3*x^3*3!+5*x^4*4!+8*x^5*5!+13*x^6*6!,x,8),['egf']);" }
}
{PARA 11 "> " 1 "" {XPPMATH 20 "6#7$,$*$,(!\"\"\"\"\"%\"xGF(*$F)\"\"#F(F'F'%$egfG
"}}}
}
{SECT  0 {PARA 4 "> " 0 "seealso" {TEXT -1 9 "See Also " }
}
{PARA 0 "> " 0 "" {HYPERLNK 17 "gfun" 2 "gfun" "" }{TEXT -1 2 ", " }
{HYPERLNK 17 "gfun[parameters]" 2 "gfun[parameters]" "" }{TEXT -1 2 ", " }
{HYPERLNK 17 "convert[ratpoly]" 2 "convert[ratpoly]" "" }{TEXT -1 2 "  " }
}
}}
