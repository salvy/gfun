mwstomw:=proc(dir::string:=".")
local lookIn, ws, fullpath;
    if not FileTools:-Exists(dir) then error "directory %1 does not exist",dir fi;
    lookIn:=FileTools:-AbsolutePath(dir);
    if not FileTools:-IsDirectory(lookIn) then error "not a directory: %1",lookIn fi;
    for ws in FileTools:-ListDirectory(lookIn,'returnonly'="*.mws") do
        fullpath:=cat(lookIn,kernelopts('dirsep'),ws);
print("converting",fullpath);
        Worksheet[WriteFile](cat(fullpath[1 .. -4],"mw"),
            Worksheet[ReadFile](fullpath),format="mw")
    od
end:

mwstomw(".");
mwstomw("NumGfun/help");

