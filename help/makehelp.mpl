## The variable THE_HDB has to be assigned before running this Maple code.

#basedir := cat("Mathematics/Discrete Mathematics/Combinatorics/gfun-",
#    convert(gfun:-version, 'string'), "/"):

#basedir := "Mathematics/Discrete Mathematics/Combinatorics/gfun/":
basedir := "gfun/":
basedirNumGfun := cat(basedir,"NumGfun Subpackage/"):
basedirContFrac := cat(basedir,"ContFrac Subpackage/"):

## (semi-)automatically generated using i2makedoc.plx
makehelp(
  `gfun`,
  `help/gfun.mw`,
  THE_HDB,
  'aliases'=["gfun/overview"," overview/gfun"],
  'browser'=[cat(basedir, "Overview")]):
makehelp(
  `gfun[algebraicsubs]`,
  `help/algebraicsubs.mw`,
  THE_HDB,
  'aliases'=["algebraicsubs"],
  'browser'=[cat(basedir, "algebraicsubs")]):
makehelp(
  `gfun[algeqtodiffeq]`,
  `help/algeqtodiffeq.mw`,
  THE_HDB,
  'aliases'=["algeqtodiffeq"],
  'browser'=[cat(basedir, "algeqtodiffeq")]):
makehelp(
  `gfun[algeqtoseries]`,
  `help/algeqtoseries.mw`,
  THE_HDB,
  'aliases'=["algeqtoseries"],
  'browser'=[cat(basedir, "algeqtoseries")]):
makehelp(
  `gfun/algfuntoalgeq`,
  `help/algfuntoalgeq.mw`,
  THE_HDB,
  'aliases'=["algfuntoalgeq"],
  'browser'=[cat(basedir, "algfuntoalgeq")]):
makehelp(
  `gfun[borel]`,
  `help/borel.mw`,
  THE_HDB,
  'aliases'=["borel"],
  'browser'=[cat(basedir, "borel")]):
makehelp(
  `gfun[rec+rec]`,
  `help/cauchyproduct.mw`,
  THE_HDB,
  'aliases'=["borel","rec*rec","gfun/`rec*rec`"],
  'browser'=[cat(basedir, "rec+rec"), cat(basedir, "rec*rec"), cat(basedir, "borel")]):
makehelp(
  `gfun[diffeqtohomdiffeq]`,
  `help/diffeqtohomdiffeq.mw`,
  THE_HDB,
  'aliases'=["gfun[rectohomrec]", "diffeqtohomdiffeq", "rectohomrec"],
  'browser'=[cat(basedir, "diffeqtohomdiffeq"), cat(basedir, "rectohomrec")]):
makehelp(
  `gfun[diffeqtorec]`,
  `help/diffeqtorec.mw`,
  THE_HDB,
  'aliases'=["diffeqtorec"],
  'browser'=[cat(basedir, "diffeqtorec")]):
makehelp(
  `gfun/gftypes`,
  `help/gftypes.mw`,
  THE_HDB,
  'browser'=[cat(basedir, "gftypes"), cat(basedir, "diffeqtorec")]):
makehelp(
  `gfun[guessgf]`,
  `help/guessgf.mw`,
  THE_HDB,
  'aliases'=["gfun[guesseqn]","guessgf","guesseqn"],
  'browser'=[cat(basedir, "guessgf"), cat(basedir, "guesseqn")]):
makehelp(
  `gfun[diffeq+diffeq]`,
  `help/hadamardproduct.mw`,
  THE_HDB,
  'aliases'=["diffeq+diffeq", "gfun[`diffeq*diffeq`]", "diffeq*diffeq",
            "gfun[hadamardproduct]", "hadamardproduct"],
  'browser'=[cat(basedir, "diffeq+diffeq"), cat(basedir, "diffeq*diffeq"),
            cat(basedir, "hadamardproduct")]):
makehelp(
  `gfun[holexprtodiffeq]`,
  `help/holexprtodiffeq.mw`,
  THE_HDB,
  'aliases'=["holexprtodiffeq"],
  'browser'=[cat(basedir, "holexprtodiffeq")]):
makehelp(
  `gfun[invborel]`,
  `help/invborel.mw`,
  THE_HDB,
  'aliases'=["gfun[Laplace]","invborel","Laplace","laplace","gfun/laplace"],
  'browser'=[cat(basedir, "invborel"), cat(basedir, "laplace")]):
makehelp(
  `gfun[listtoalgeq]`,
  `help/listtoalgeq.mw`,
  THE_HDB,
  'aliases'=["gfun[seriestoalgeq]","listtoalgeq","seriestoalgeq"],
  'browser'=[cat(basedir, "listtoalgeq"), cat(basedir, "seriestoalgeq")]):
makehelp(
  `gfun[listtodiffeq]`,
  `help/listtodiffeq.mw`,
  THE_HDB,
  'aliases'=["gfun[seriestodiffeq]","listtodiffeq","seriestodiffeq"],
  'browser'=[cat(basedir, "listtodiffeq"), cat(basedir, "seriestodiffeq")]):
makehelp(
  `gfun[listtohypergeom]`,
  `help/listtohypergeom.mw`,
  THE_HDB,
  'aliases'=["gfun[seriestohypergeom]","listtohypergeom","seriestohypergeom"],
  'browser'=[cat(basedir, "listtohypergeom"), cat(basedir, "seriestohypergeom")]):
makehelp(
  `gfun[listtoratpoly]`,
  `help/listtoratpoly.mw`,
  THE_HDB,
  'aliases'=["gfun[seriestoratpoly]","listtoratpoly","seriestoratpoly"],
  'browser'=[cat(basedir, "listtoratpoly"), cat(basedir, "seriestoratpoly")]):
makehelp(
  `gfun[listtorec]`,
  `help/listtorec.mw`,
  THE_HDB,
  'aliases'=["gfun[seriestorec]","listtorec","seriestorec"],
  'browser'=[cat(basedir, "listtorec"), cat(basedir, "seriestorec")]):
makehelp(
  `gfun[listtoseries]`,
  `help/listtoseries.mw`,
  THE_HDB,
  'browser'=[cat(basedir, "listtoseries")]):
makehelp(
  `gfun/parameters`,
  `help/parameters.mw`,
  THE_HDB,
  'aliases'=["gfun/Parameters"],
  'browser'=[cat(basedir, "Parameters")]):
makehelp(
  `gfun[poltodiffeq]`,
  `help/poltodiffeq.mw`,
  THE_HDB,
  'aliases'=["polytodiffeq"],
  'browser'=[cat(basedir, "poltodiffeq")]):
makehelp(
  `gfun[poltorec]`,
  `help/poltorec.mw`,
  THE_HDB,
  'aliases'=["poltorec"],
  'browser'=[cat(basedir, "poltorec")]):
makehelp(
  `gfun[ratpolytocoeff]`,
  `help/ratpolytocoeff.mw`,
  THE_HDB,
  'aliases'=["ratpolytocoeff"],
  'browser'=[cat(basedir, "ratpolytocoeff")]):
makehelp(
  `gfun[rectodiffeq]`,
  `help/rectodiffeq.mw`,
  THE_HDB,
  'aliases'=["rectodiffeq"],
  'browser'=[cat(basedir, "rectodiffeq")]):
makehelp(
  `gfun[rectoproc]`,
  `help/rectoproc.mw`,
  THE_HDB,
  'aliases'=["rectoproc"],
  'browser'=[cat(basedir, "rectoproc")]):
## end (semi-)automatically generated entries
makehelp(
  `gfun[reducerecorder]`,
  `help/reducerecorder.mw`,
  THE_HDB,
  'aliases'=["reducerecorder"],
  'browser'=[cat(basedir, "reducerecorder")]);
makehelp(
  `gfun[proctorec]`,
  `help/proctorec.mw`,
  THE_HDB,
  'aliases'=["proctorec"],
  'browser'=[cat(basedir, "proctorec")]);
makehelp(
  `gfun[nth_term]`,
  `help/nth_term.mw`,
  THE_HDB,
  'aliases'=["NumGfun/fnth_term", "nth_term", "fnth_term"],
  'browser'=[cat(basedir, "nth_term"), cat(basedir, "fnth_term"), cat(basedirNumGfun, "nth_term"), cat(basedirNumGfun, "fnth_term")]):
makehelp(
  `NumGfun`,
  `NumGfun/help/NumGfun.mw`,
  THE_HDB,
  'aliases'=["gfun[NumGfun]","gfun/NumGfun/overview", " overview/NumGfun",
        "NumGfun/Settings"],
  'browser'=[cat(basedirNumGfun, "Overview")]):
makehelp(
  `NumGfun[evaldiffeq]`,
  `NumGfun/help/evaldiffeq.mw`,
  THE_HDB,
  'aliases'=["NumGfun/analytic_continuation", "NumGfun/transition_matrix",
        "evaldiffeq", "analytic_continuation"],
  'browser'=[cat(basedirNumGfun, "evaldiffeq"), cat(basedirNumGfun,
        "analytic_continuation"), cat(basedirNumGfun,"transition_matrix")]):
makehelp(
  `NumGfun[diffeqtoproc]`,
  `NumGfun/help/diffeqtoproc.mw`,
  THE_HDB,
  'aliases'=["NumGfun/diffeqtoproc", "diffeqtoproc"],
  'browser'=[cat(basedirNumGfun, "diffeqtoproc")]):
makehelp(
  `NumGfun[bound_ratpoly]`,
  `NumGfun/help/bound_ratpoly.mw`,
  THE_HDB,
  'aliases'=["bound_ratpoly"],
  'browser'=[cat(basedirNumGfun, "bound_ratpoly")]):
makehelp(
  `NumGfun[bound_diffeq]`,
  `NumGfun/help/bound_diffeq.mw`,
  THE_HDB,
  'aliases'=["bound_diffeq", "NumGfun[bound_diffeq_tail]", "bound_diffeq_tail"],
  'browser'=[cat(basedirNumGfun, "bound_diffeq")]):
makehelp(
  `NumGfun[bound_rec]`,
  `NumGfun/help/bound_rec.mw`,
  THE_HDB,
  'aliases'=["NumGfun/bound_rec_tail", "bound_rec", "bound_rec_tail"],
  'browser'=[cat(basedirNumGfun, "bound_rec"), cat(basedirNumGfun,"bound_rec_tail")]):
makehelp(
  `NumGfun[dominant_root]`,
  `NumGfun/help/dominant_root.mw`,
  THE_HDB,
  'aliases'=["dominant_root"],
  'browser'=[cat(basedirNumGfun, "dominant_root")]):
makehelp(
  `NumGfun[local_basis]`,
  `NumGfun/help/local_basis.mw`,
  THE_HDB,
  'aliases'=["local_basis"],
  'browser'=[cat(basedirNumGfun, "local_basis")]):
makehelp(
  `NumGfun[plot_path]`,
  `NumGfun/help/plot_path.mw`,
  THE_HDB,
  'aliases'=["plot_path"],
  'browser'=[cat(basedirNumGfun, "plot_path")]):
makehelp(
  ContFrac,
  `ContFrac/help/ContFrac.mw`,
  THE_HDB,
  'aliases'=["gfun[ContFrac]"],
  'browser'=[cat(basedirContFrac, "Overview")]):
makehelp(
  `ContFrac[expr_to_cfrac]`,
  `ContFrac/help/expr_to_cfrac.mw`,
  THE_HDB,
  'aliases'=["expr_to_cfrac"],
  'browser'=[cat(basedirContFrac, "expr_to_cfrac")]):
makehelp(
  `ContFrac[riccati_to_cfrac]`,
  `ContFrac/help/expr_to_riccati.mw`,
  THE_HDB,
  'aliases'=["riccati_to_cfrac"],
  'browser'=[cat(basedirContFrac, "riccati_to_cfrac")]):
makehelp(
    `ContFrac/Handbook`,
    `ContFrac/help/Cuyt_Riccati_CFracs.mw`,
    THE_HDB,
    'browser'=[cat(basedirContFrac, "Handbook of continued fractions")]):
makehelp(
  `gfun[minimizediffeq]`,
  `help/minimizediffeq.mw`,
  THE_HDB,
  'aliases'=["minimizediffeq"],
  'browser'=[cat(basedir, "minimizediffeq")]):
makehelp(
  `gfun[pade2]`,
  `help/pade2.mw`,
  THE_HDB,
  'aliases'=["pade2"],
  'browser'=[cat(basedir, "pade2")]):



HelpTools:-Database:-SetAttribute(THE_HDB,'priority',"1"):

#HelpTools:-Database:-ConvertAll();

