# Majorant series for rational polynomials
# Author: Marc Mezzarobba

bound_ratpoly := module()


local parse_exppoly_term, ratabove_algebraic, ratabove_algebraic_doit;

export bound_term, bound_tail, doit, ModuleApply;

# Input:  expression of the form c·(n+1)···(n+d)·a^(-n)
# Output: [c, d, a]
parse_exppoly_term := proc(Term, n, $)::[complexcons,nonnegint,anything];
    local t, c, p, e, d, a;
    if type(Term, `*`) then t := [op(Term)] else t := [Term] end if;
    c, t := selectremove(type, t, 'constant');
    p, t := selectremove(type, t, 'linear'(n));
    e, t := selectremove(type, t, 'specop'('anything', `^`));
    if t <> [] or nops(e) > 1 or nops(e) = 1 and op(2,op(e)) <> -n then
        error "unable to parse %1", Term;
    end if;
    d := nops(p);
    a := `*`(op(map[2](op,1,e)));
    ASSERT( `*`(op(subs(n=0,p))) = d! );
    [`*`(op(c)), d, a];
end proc:

# TODO: use the generic ratabove instead?
# Input:  zeta algebraic, abs(zeta) <= 1
# Output: r rational s.t. abs(zeta) <= r <= 1 with r < 1 if abs(zeta) < 1
ratabove_algebraic := proc(zeta)
    option cache;
    Digits := trunc(evalhf(Digits));
    ratabove_algebraic_doit(zeta);
end proc:

ratabove_algebraic_doit := proc(zeta)
    local num, r, a;
    fail_if_Digits_too_large("ratabove_algebraic");
    if type(zeta, 'complex'('rational')) then
        a := abs(zeta);
        if a^2 > 1 then error "something is going wrong here" end if;
        return a;
    end if;
    num := abs(evalf(zeta)); # no cancellation here
    r := num + Float(1, 2-Digits);
    if r < 1 then
        convert(r, 'rational', 'exact');
    elif num > 1 +  Float(1, 2-Digits) then
        error "expected abs(zeta) <= 1, received zeta = %1", zeta;
    else  # it seems that abs(zeta) = 1
        a := abs_with_RootOf(zeta);
        if a = 1 then
            1
        else
            Digits := 2 * Digits;
            procname(a);
        end if;
    end if;
end proc;

# Helper function for bound_ratpoly:-doit.
#
# Input:  'Term' is one term of a sum returned by ratpolytocoeff, that is, an
#   expression of the form Sum(rat(zeta,n)·zeta^(-n), zeta=RootOf(P_zeta)) with
#   possible degenerate cases ranging from a single rat(n)·zeta^(-n) to a
#   constant
# Output: 'bound' and 'n0' such that for n >= n0, abs(Term(n)) <= [z^n]
#   bound/(1-alpha·z)^m.
bound_term := proc(Term, n, alpha, m, $)
    local expr, eq, zeta, P_zeta, zetas, alg, npart, rat, infroot, cste, deg, t,
        lambda, bound, k, n0;
    if type(Term, 'Or'('constant','linear'(n))) then
        # there may remain a linear part <> n+1...
        if Term = 0 then return [0, 1] end if; # even if alpha > 1
        ASSERT(m >= degree(Term, n), "m too small");
        lambda := ratabove_algebraic(1/alpha); 
        npart := abs(Term) assuming n::nonnegint;
        bound := (m-1)! * normal(npart/mul(n+k,k=1..m-1)) * lambda^n;
        return [bound, 1];
    elif type(Term, 'specfunc'('anything', 'Sum')) then
        expr, eq := op(Term);
        ASSERT(type(rhs(eq), 'RootOf'), "malformed expression");
        zeta := lhs(eq); P_zeta := eval(subs(_Z=zeta, op(rhs(eq))));
        if type(expr, `*`) then expr := [op(expr)] else expr := [expr] end if;
        npart, rat := selectremove(has, expr, n);
        npart := `*`(op(npart)); rat := `*`(op(rat));
        t := parse_exppoly_term(npart, n);
        t := evala(Normal(t));
        ASSERT(t[1] = 1); ASSERT(t[3] = zeta);
        zetas := map(
            dominant_root:-irreducible_solve,
            op(2, evala(Factors(P_zeta, zeta))),
            zeta);
        zetas := ListTools:-Flatten(zetas);
        ASSERT(add(alg['mult'], alg in zetas) = degree(P_zeta, zeta));
        cste := add(
            alg['mult']*above_abs(subs(zeta=alg['iv'], rat)),
            alg in zetas);
        cste := ratabove(cste);
        userinfo(5, 'gfun', 'cste'=cste);
        # one of the ζ's closest to 0
        infroot := dominant_root(P_zeta, zeta, 'rootof')[1];
        deg := t[2];
    else
        t := parse_exppoly_term(Term, n);
        t := evala(Normal(t));
        cste := abs(t[1]); deg := t[2]; infroot := t[3];
    end if;
    lambda := ratabove_algebraic(1/(alpha*infroot));
    bound := cste*(m-1)!*normal(mul(n+k,k=1..deg)/mul(n+k,k=1..m-1))*lambda^n;
    if m > deg then
        n0 := 1
    else
        ASSERT(lambda < 1);
        n0 := max(1, ceil(above((deg-m)/log(1/lambda))));
    end if;
    [bound, n0];
end proc:

bound_tail := proc(rat, z, alpha, m, n, $)
    local den, polypart, num, rofn, term_bounds, tofn, n0;
    den := denom(rat); polypart := quo(numer(rat), den, z, 'num');
    # Compute the general coefficient of the series expansion of rat, as a sum
    # (of Sums over the roots of some squarefree factor of den) of terms of the
    # form cste·(n+1)···(n+k)·zeta^(-n) (where cste usually involves the root
    # over which the Sum if taken).  Trivial piecewise's may appear,
    # convert/piecewise gets rid of them.
    rofn := convert(ratpolytocoeff(num/den, z, n),'piecewise');
    if type(rofn, `+`) then rofn:=[op(rofn)] else rofn:=[rofn] end if;
    # Compute a formal expression >= abs(rofn)/([z^n]1/(1-alpha·z)^m) and a
    # positive integer n0 such that our bound is nonincreasing for n >= n0.
    term_bounds := map(bound_term, rofn, n, alpha, m);
    tofn := `+`(op(map[2](op, 1, term_bounds)));
    n0 := max(op(map[2](op, 2, term_bounds)));
    # Increase n0 so that |[z^n]rat| <= tofn(n)/(1-αz)^m for n >= n0.
    n0 := max(1, degree(polypart) + 1, n0);
    [tofn, n0]
end proc:

# Input:  a rational function rat(z), an algebraic number alpha, a positive
#   integer m; with abs(α⁻¹) <= abs(dominant singularity of rat) and
#   abs(α⁻¹) = abs(dominant sing) => m >= multiplicity(dominant sing)
# Output: A, [b0, b1, ..., b(n0-1)]] s.t. |[z^n]rat| <= A·([z^n]1/(1-alpha·z))
#   for n >= n0, and |[z^n]rat| <= A·([z^n]1/(1-α·z)) + b_n for n < n0
# Note to self: Also provide a version of bound_ratpoly that returns bounds of
#   the form M/(1-α·z)^m (without polynomial part)?  Permit negative terms in
#   the polynomial part???
#
# TODO: Check that α satisfies the above constraints?
doit := proc(Rat, z, alpha, m, $)
    local rat, tofn, n0, tail_bound, next_tail_bound, max_ratio, prev_max_ratio,
        i, j, l_alpha, _head, abs_head, head_bound, y, u, n, b, delta, hpol;
    rat := evala(Normal(Rat)); # not strictly necessary (even if rat=0
        # mathematically but not syntactically, we should have max_ratio > 0
        # due to overestimations, and the code below should work fine)
    if type(rat, 'polynom') then
        tail_bound := 0;
        head_bound := [seq(abs(coeff(rat,z,i)), i=0..degree(rat))];
    else
        ASSERT(evalf(abs(alpha)) > 0);
        tofn, n0 := op(bound_tail(rat, z, alpha, m, n));
        l_alpha := below_abs(make_RootOfs_indexed(alpha), 'rational');
        # WARNING: head now represents the leading terms of rat(z/l_alpha), not
        # rat(z) itself!
        _head := rectoproc(
            diffeqtorec(y(z)=subs(z=z/l_alpha, rat), y(z), u(n)),
            u(n), 'remember', 'evalfun'=evalrC);
        abs_head := proc()
            Digits := Digits_plus(Settings:-bound_ratpoly_digits); # TBI?
            evalrC(above_abs(_head(_passed)))
        end proc;
        # Increase n0 as long as doing so significantly improves A and the
        # corresponding tail bound remains valid except perhaps for the first
        # few terms. The idea is that, if we are looking for an asymptotically
        # tight bound, tofn will tend to a positive limit and we hope to end up
        # with A within a constant factor of that limit; while if, say, α was
        # underestimated, then tofn → 0 and we want to see how small we can make
        # it without handling lots of initial terms separately.
        next_tail_bound := infinity; tail_bound := infinity; max_ratio := 0;
        ASSERT(rat <> 0); # so that the loop terminates
        for j from  Settings:-bound_ratpoly_pol_part_deg_bound
              while tail_bound >= Settings:-bound_ratpoly_tail_prec
                                  * next_tail_bound
                and next_tail_bound >= max_ratio
        do
            prev_max_ratio := max_ratio;
            max_ratio := max(max_ratio, above(abs_head(j)/binomial(j+m-1, m-1)));
            tail_bound := next_tail_bound;
            if j >= n0 then next_tail_bound := above(subs(n=j, tofn)) end if;
            userinfo(10, 'gfun', "j"=j, "abs_head"=map(evalf[3],abs_head(j)),
                "max_ratio"=max_ratio,
                "tail_bound"=tail_bound, "next_tail_bound"=next_tail_bound);
        end do;
        userinfo(6, 'gfun', 'NoName', "bound_ratpoly: expansion order" = j);
        # We may exit while tail_bound still equals ∞ either because max_ratio
        # is already smaller than tofn for j=n0, or because tofn and max_ratio
        # are very close to each other and the loop condition fails due to
        # overestimation.
        ASSERT(tail_bound < infinity
               or j in {n0, Settings:-bound_ratpoly_pol_part_deg_bound+1});
        if tail_bound = infinity then
            tail_bound := max(max_ratio, next_tail_bound);
        end if;
        ASSERT(tail_bound >= prev_max_ratio);
        tail_bound := convert(tail_bound, 'rational', 'exact');
        delta := seq(
            above(abs_head(j)*l_alpha^j - tail_bound * binomial(j+m-1, m-1)),
            j = 0..Settings:-bound_ratpoly_pol_part_deg_bound-1);
        head_bound := [seq(max(0, b), b in delta)];
    end if;
    for j to nops(head_bound) while head_bound[-j] = 0 do end do;
    head_bound := head_bound[..-j];
    head_bound := convert(head_bound, 'rational', 'exact');
    hpol := PolynomialTools:-FromCoefficientList(evalf[5](head_bound), z);
    userinfo(4, 'gfun', 'NoName', sprintf("bound_ratpoly: bounded %a=%a by "
        "%a/(1-%a*%a)^%a + %a = %a",
        rat, evalf[2](series(rat, z=0)),
        evalf[5](tail_bound), abs(evalf[5](alpha)), z, m, hpol,
        evalf[2](series(tail_bound/(1-alpha*z)+hpol, z))));
    tail_bound, head_bound;
end proc:

ModuleApply := proc(Rat::ratpoly(complex(numeric)), z::name, $) # exported
    description "Compute a majorant of the form z^t*M/(1-a·z)^m+P(z) for rat.";
    local rat, mult0, dompole, mult, M, P, maj;
    rat := normal(Rat);
    mult0 := bounds:-common_root_multiplicity(denom(rat), z, z);
    rat := normal(z^mult0 * rat);
    dompole, mult := op(dominant_root(denom(rat), z, 'rootof'));
    M, P := bound_ratpoly:-doit(rat, z, 1/dompole, mult);
    maj := M * 1/(1-z/abs_with_RootOf(dompole))^mult
            + PolynomialTools:-FromCoefficientList(P, z);
    z^(-mult0) * maj;
end proc:

end module:
