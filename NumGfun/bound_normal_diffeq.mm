# Parameter computation for majorant series (normal case)
# Author: Marc Mezzarobba

# Compute the parameters of a majorant for the solution of deq specified by
# deq. The differential equation is assumed to be normalized.  The
# code actually works in the following degenerate cases where α=0:
# · Deq independent on z when written on (z,θ): Q(θ)·y(z)=0 (→ Q(n)·u(n)=0,
#   ← Q(n)·u(n+k)=0 after simplification); κ=-∞ with my current conventions;
# · Order 0: p(z)·y(z)=0 (rec with cst coeffs, valid on the relative integers);
#   could be considered normalized but simplifies to just y(z)=0, which is of
#   the previous form except that κ=0 when p≠0 (unless we choose to simplify
#   the rec before computing κ). Note that normalization can result in
#   equations of this form, i.e. they can also occur with a finite nonzero κ.

# TODO: Extend to allow computing a bound on [N,∞) only?

bound_normal_diffeq := module()

local n, y, z, ordeq;  # names shared by all functions

export
    # to ease debug
    rewrite_diffeq, bound_coefficients, reduce_order, doit,
    posints_around_iv, bound_modified_inv_indeq, reduce_order_logs,
    # really public
    bound_ratpoly_at_posints, ModuleApply;

posints_around_iv := proc(iv, $)
    ASSERT(type(iv, 'specfunc'('anything', 'INTERVAL')) and nops(iv) = 4);
    if op(2, iv) > 0. and op(4, iv) < 0. then
        # Non-real => nothing
        NULL;
    else
        $ max(1, floor(op(1, iv))) .. ceil(op(3, iv));
    end if;
end proc:

# Given a rational function rat with complex coefficients, of nonpositive
# degree D, and an integer d with D <= -d, compute M such that |rat(n)| <= M/n^d
# for all n ∈ ℕ \ {0, poles of rat}.
bound_ratpoly_at_posints := proc(Rat, x, d, $)
    local rat, num, den, dnum, critical, candidates, above_abs_at_int,
        above_abs_lim;
    rat := normal(Rat/x^d);
    num, den := numer(rat), denom(rat);
    ASSERT(degree(num, x) - degree(den, x) <= 0);
    dnum := numer(diff(rat, x));
    # Roots, poles, local extrema
    # (Real root isolation in Maple only works for numeric coefficients...)
    critical := map(
        proc(pol)
            local i;
            seq(evalrC(RootOf(pol, index=i)), i=1..degree(pol));
        end proc,
        {num, den, dnum});
    candidates := {1} union map(posints_around_iv, critical);
    above_abs_at_int := proc(n, $)
        local den_at_n, iv_den;
        den_at_n := subs(x=n, den);
        iv_den := evalrC(den_at_n);
        if not iv_contains_zero(iv_den) then
            above_abs(subs(x=n, num)/iv_den);
        elif normal(den_at_n) = 0 then
            NULL
        else
            Digits := 2*Digits;
            fail_if_Digits_too_large("bound_ratpoly_integers");
            bound_ratpoly_integers(_passed);
        end if;
    end proc:
    above_abs_lim := above_abs(limit(rat, x=infinity));
    max(seq(above_abs_at_int(n), n in candidates), above_abs_lim);
end proc:

# Rewrite deq as a polynomial in (z,theta) and isolate the coefficient of z^0
rewrite_diffeq := proc(deq, $)
    local p, k, rat, c, a, theta;
    p := thetadeq(deq, y(z));
    for k from 0 to ordeq do
        rat := evala(Normal(p[k]/p[ordeq]));
        if eval(denom(rat), z=0) = 0 then
            error "irregular singular points are not supported"
        end if;
        c[k] := eval(rat, z=0);
        a[k] := normal((rat - c[k])/z);
    end do;
    userinfo(5, 'gfun',
        sprintf("differential equation rewritten as (%a)y = z*(%a)y",
        sort(add(c[k]*theta^k, k=0..ordeq),theta),
        add(a[k].theta^k,  k=0..ordeq-1) ));
    a, c;
end proc:

# Compute bounds for the rational functions a[k]
# Return value: [mu, T, MM, PP] such that each a[k] is bounded (in the sense of
# majorant series) by
# binom(r-1,k) * (MM*(1-α)^(r-k+T) + sum((j+1)^(r-1-k)*P_j*z^j, n>=0)).
bound_coefficients := proc(a, $)
    local lcden, mu, mult, candidates, cand, refpolys, alpha, T, k, M, P, MM,
        PP, j, t, degPP, PPjk, refpoly;
    # seems ok even with algnums, needs initial 1 in maple2023
    lcden := lcm(1, seq(denom(a[k]),k=0..ordeq-1));  
    # mult is not used
    mu, mult, candidates := op(dominant_root:-doit(convert(lcden, 'RootOf'), z,
                                                       'return_internal_data'));
    if ordeq = 0 then return mu, 0, 0, [] end if;
    if numeric_mode then
        # Make sure that mu is smaller than all other *approximate* roots of
        # the denominators (as required by bound_ratpoly)
        mu := (1-10^(-Digits_plus(-2)))*mu;
        mu := convert(rndz(abs(mu)), rational, 'exact');
    end if;
    # Irregularity (NOT at the origin, hence the "- ordeq + k" even with θ).
    # Note that the pole maximizing mult-ordeq+k may be different from the one
    # maximizing mult itself, i.e. from the dominant singularity. (Could our
    # definitions of these concepts be improved to prevent this phenomenon?)
    # To compute the irregularity, we look for the maximum multiplicity of an
    # irreducible factor having a root of the right modulus in each denominator.
    refpolys := {seq(cand:-pol, cand in candidates)}; # irreducible
    T := max(0, seq(seq(
        bounds:-common_root_multiplicity(denom(a[k]), refpoly, z) - ordeq + k,
        k = 0..ordeq-1), refpoly in refpolys ));
    alpha := 1/mu;  # WARNING: alpha need not be real!
    for k from 0 to ordeq-1 do
        M[k], P[k] := bound_ratpoly:-doit(a[k], z, alpha, T+ordeq-k);
        P[k] := PolynomialTools:-FromCoefficientList(P[k], z)
    end do;
    MM := max(seq(M[k]/binomial(ordeq-1,k), k=0..ordeq-1));
    ASSERT(type(MM, 'rational'));
    if type(alpha, 'rational') then  # TBI
        t := abs(alpha)
    else
        t := below_abs(make_RootOfs_indexed(alpha));
    end if;
    # 'New' formula for majorants with polynomial parts.  We need
    # a <| binom(r-1,k) * (MM*(1-α)^(r-k+T) + sum((j+1)^(r-1-k)*P_j*z^j, n>=0)).
    # There is an opportunity to absorb part of the P^[k] in
    # (MM-M[k]/binom(...))*(1-α)^(r-k+T).
    degPP := max(seq(degree(P[k]), j=0..ordeq-1)); # max w/o seq is too clever
    PPjk := proc(j, k)
        ( binomial(j+ordeq-k+T-1, j) * t^j  # <= [z^j] (1-α)^(r-k+T)
                    * (M[k] - binomial(ordeq-1, k)*MM)
          + coeff(P[k], z, j) )
        / binomial(ordeq-1, k) * j^(ordeq-1-k);
    end proc:
    PP := [seq( max(0, seq(PPjk(j, k), k=0..ordeq-1)), j=0..degPP)];
    userinfo(4, 'gfun', "abs(alpha)" = evalf[5]('abs'(alpha)), "irreg" = T,
        "cst"=MM, "deg_poly"=nops(PP));
    mu, T, MM, PP;
end proc:

# Compute K,N s.t. n>=N => K·q(n) >= M·mu·n^ordeq, in order to bound the
# diffeq by one of order 1.
#
# With qrest(n) >= |Q(n)-n^r| and K0 >= M·mu, it is enough to have
# qrest(n) <= (1-K0/K)·n^r.
#
# NOTE: this seems quite pessimistic for differential equations with large
# rational coefficients (such as, typically, those obtained by algebraicsubs +
# normalize_diffeq).
reduce_order := proc(c, mu, M, $)
    local qrest, k, K0, K, lfac, N;
    qrest := add(above_abs(c[k], 'rational')*n^k, k=0..ordeq-1);
    if M = 0 then # degenerate case, see comment above
        # The condition on N reduces to Q(N) > 0.
        ASSERT(mu = infinity);
        K := 0; lfac := 1;
    else
        K0 := M * above_abs(mu, 'rational');
        # Heuristic to try to keep N reasonable.  All we really need is K>K0
        # (K=K0 would be okay for qrest=0).
        K := ceil(K0 * (1 + abs(evalf(subs(n=50,qrest)/50^ordeq))));
        lfac := 1-K0/K;
    end if;
    N := ordeq;
    _Envsignum0 := 0;  # not 1 => equality below is ok
    while signum(abs(subs(n=N, qrest)) - lfac * N^ordeq) = 1 do
        N := 2*N;
        if N > Settings:-max_indicial_eq_tail_index then
            error "unable to compute a reasonable bound "
                "(increase Settings:-max_indicial_eq_tail_index to proceed)";
        end if;
    end do;
    userinfo(5, 'gfun', "M" = M, "K" = K, "N" = N);
    if K > Settings:-max_bound_exponent then
        error "unable to compute a reasonable bound "
            "(increase Settings:-max_bound_exponent to proceed)";
    end if;
    K, N, 1; # 1 = factor by which to multiply P, for compat with log case
end proc:

# Helper function for reduce_order_logs. Bound the coefficients t^[p]_q(n0) of
# the recurrence operator T[n0](Sk)*(n0+Sk)^p mod Sk^(t-1), where T[n0](Sk) is
# the "pseudo-inverse" of indicial_equation(n0+Sk). Can return either a bound
# valid for generic n0 or one for a specific, possibly singular, value.
#
# NOTE: in principle, we could probably ignore the high-order terms wrt Sk for
# n0 smaller than some of the singular shifts (i.e., increase Sk progressively)
bound_modified_inv_indeq := proc(indeq, n, p, ordSk, algeb_shift, int_idx, mult, $)
    local Sk, n0, rat_n0_Sk, bound_rat_n0_Sk, bounds, q, b;
    n0 := algeb_shift+int_idx;
    rat_n0_Sk := Sk^mult*subs(n=n0+Sk, indeq)^(-1)*(n0+Sk)^p;
    rat_n0_Sk := convert(MultiSeries:-series(rat_n0_Sk, Sk, ordSk), 'polynom');
    bound_rat_n0_Sk := proc(q, $)
        if type(int_idx, 'symbol') then
            bound_ratpoly_at_posints(coeff(rat_n0_Sk, Sk, q), int_idx, ordeq-p);
        elif type(int_idx, 'integer') then
            above_abs(coeff(rat_n0_Sk, Sk, q)/int_idx^(-ordeq+p));
        else
            ASSERT(false);
        end if;
    end proc:
    bounds := [seq(bound_rat_n0_Sk(rat_n0_Sk, p, q, int_idx), q=0..ordSk)];
    ASSERT(type(bounds, 'list'('And'('float', 'positive'))));
    UseHardwareFloats := false; Rounding := infinity;
    add(b, b in bounds);
end proc:

# Counterpart of reduce_order used for computing bounds that are valid for all
# logarithmic series solutions (not just power series solutions).
#
# start_indices is a set of algebraic values of n at which fundamental solutions
# may start, typically the set of all roots of a given irreducible factor of the
# indicial polynomial.
reduce_order_logs := proc(c, mu, M, start_indices, sing_shifts, $)
    local indeq, i, n, ordSk, shifts, inv_pert_indeq_bnds, p, cst_P, cst_rat,
        algeb_start_idx, step;
    indeq := add(c[i]*n^i, i=0..order-1);
    ordSk := `+`(seq(step:-mult, step in sing_shifts));
    shifts := [Record(':-shift'=n, ':-mult'=0), op(sing_shifts)];
    inv_pert_indeq_bnds := seq(
        max(seq(seq(
            bound_modified_inv_indeq(indeq, n, p, ordSk, algeb_start_idx,
                                                       step:-shift, step:-mult),
            step in shifts), algeb_start_idx in start_indices)),
        p=0..ordeq-1);
    UseHardwareFloats := false; Rounding := infinity;
    cst_P := 2^(ordeq-1)
             * add(binomial(ordeq-1, p)*inv_pert_indeq_bnds[p+1], p=0..ordeq-1);
    cst_rat := ceil(M*cst_P*above_abs(mu)); # aka K
    cst_rat, 0, cst_P; # 0 = validity, for compatibility with series case
end proc:

# Input:
# - deq (Note: 'numeric' is not explicitly used but forces cache table
#   entries to take into account 'numeric_mode'; see ModuleApply below)
# - supp (opt.): a list [poly, [rec1, rec2...]] specifying the roots
#   of the indicial equation and their multiplicities; if this is specified, the
#   returned bound will hold for the corresponding components of log series
#   solutions (otherwise, for power series solutions only)
# Output: parameters of a majorant series 'maj' and threshold 'validity' such
#   that, IF abs([z^n](sol of deq)) <= A*[z^n](maj) for n <= validity, THEN
#   sol <| A*maj
# TODO: Understand how to properly merge the two cases (logs vs pow series). In
# particular, it should be possible to compute bounds with a validity condition
# for log series, and bounds without validity condition for power series!
doit := proc(deq, yofz, numeric, supp:=NULL, $)
    option cache;
    local a, c, mu, T, M, P, K, validity, alpha, maj, trailing_zeroes, cst_P,
        msg, pol, sing_shifts, ind, i;
    userinfo(5, 'gfun', "deq" = deq);
    # module-global, used by some fns we call
    y, z := getname(yofz);
    ordeq := orddiffeq(deq, yofz);
    a, c := rewrite_diffeq(deq);
    mu, T, M, P := bound_coefficients(a);
    alpha := 1/mu;
    if supp <> NULL then
        msg := "bound_normal_diffeq (log series)";
        pol, sing_shifts := op(supp);
        ind := { seq(RootOf(pol, 'index'=i), i=1..degree(pol)) };
        K, validity, cst_P := reduce_order_logs(c, mu, M, ind, sing_shifts);
    else # normal case
        K, validity, cst_P := reduce_order(c, mu, M);
        if alpha = 0 then  # degenerate case
            msg := "bound_normal_diffeq (degenerate case)";
            ASSERT(T=0); ASSERT(type(P, 'list(0)')); # ASSERT(K=0);
            K := validity;
        else
            msg := "bound_normal_diffeq";
        end if;
    end if;
    for trailing_zeroes to nops(P) while P[-trailing_zeroes] = 0 do end do;
    P := P[1..-trailing_zeroes];
    maj := bounds:-normal_majorant(T, abs(evalf(alpha)), K, cst_P*P, z);
    userinfo(3, 'gfun', 'NoName', sprintf(msg), "majorant" = evalf[5](maj));
    [T, alpha, K, P], validity;
end proc:

# Wrapper around doit to make cache table entries depend on numeric_mode.
ModuleApply := proc(deq, yofz, $)
    # NO 'option cache' here
    doit(deq, yofz, numeric_mode);
end proc:

end module:

