with(gfun); with(NumGfun);
a := 1/3;
tests := 10;
interface(quiet=true);
deq := diffeqtohomdiffeq(holexprtodiffeq(arctan(z),y(z)),y(z));
res := [];
for NumGfun:-Settings:-binary_splitting_threshold from 1 to 1002 by 100 do
    gc();
    res := [op(res),
    [seq(2^j=time(evaldiffeq(deq,y(z),[0,a],100*2^j)),j=6..tests)]];
end do;
