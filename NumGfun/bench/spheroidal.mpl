with(gfun); with(NumGfun);
b := 1: c := 0: q := 1:
time(evaldiffeq({(1-z^2)*diff(y(z),z,z) - 2*(b+1)*z*diff(y(z),z) + (c-4*q*z^2)*y(z), y(0)=1, D(y)(0)=0},
y(z), [0,1/3],1000));
time(evalf[1000](subs(z=1/3,HeunC(0,-1/2,b,q,1/4-1/4*b-1/4*c,z^2))));
