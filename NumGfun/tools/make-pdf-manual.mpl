# Export the NumGfun help pages to pdf.
# Unfortunately, this seems not to work correclty when run from the CLI!
with(FileTools): with(eBookTools):
book := NewBook(
    "NumGfunUserManual",
    "NumGfun User Manual",
    # magic word, see Makefile
    "IloNR2XbAugIyapF"
    "Manual for NumGfun v. " || (gfun:-NumGfun:-version),
    StringTools:-FormatTime("Last updated %Y-%m-%d"));
SetAuthor(book, [table(["firstname"="Marc", "surname"="Mezzarobba"])]);
SetOutputDir("lib");
SetTempDir("lib");
dir := "NumGfun/help/";
chap := sort(ListDirectory(dir, 'returnonly'="*.mw"));
for i to nops(chap) do
  AddChapter(book, i, cat(dir, chap[i]))
end do;
settings := table([
  "paper.type" = "A4",
  "page.height" = "297mm",
  "page.width" = "210mm",
  "chapter.autolabel" = 0,
  "section.autolabel" = 0,
  "toc.section.depth" = 0,
  "generate.section.toc.level" = 0,
  "mpl.create_index" = false,
  "double.sided" = 0,
  "mpl.images.dpi" = 400,
  "title.font.family" = "sans-serif",
NULL]);
CreatePDF(book, settings);
