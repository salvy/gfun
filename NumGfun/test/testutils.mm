_Env_TestTools_Try_printf := false:
Try := TestTools:-Try:
#TestTools:-SetRecord(false):

taylorcoeffs := proc(expr, x, n)
    local s, k:
    s := convert(series(expr, x=0, n),polynom):
    return [seq(coeff(s, x, k), k=0..n-1)]:
end proc:
    
testmajlist := proc(majcoeffs, exprcoeffs)
    local delta, res:
    delta := majcoeffs - exprcoeffs:
    res := type(delta,'list'('nonnegative')):
    if not res then
        printf( "# bounded: %a\n"
                "# by:      %a\n"
                "# delta=   %a\n",
                exprcoeffs, majcoeffs, delta);
    end if;
    res:
end proc:

testmajseries := proc(maj, expr, z, {prec := 10})  # notice the order
    local majcoeffs, exprcoeffs;
    majcoeffs := evalf(taylorcoeffs(maj, z, prec)):
    exprcoeffs := map(abs, evalf(taylorcoeffs(expr, z, prec))):
    testmajlist(majcoeffs, exprcoeffs):
end proc:

testrecbound := proc(maj, rec, u, n, {prec := 30})  # notice the order
    local majcoeffs, exprcoeffs;
    majcoeffs :=  [seq(evalf(eval(maj)), n=0..prec)];
    exprcoeffs := map(abs, gfun:-rectoproc(rec, u(n), 'list')(prec)):
    testmajlist(majcoeffs, exprcoeffs):
end proc:

# y should be a fp number, otherwise this may or may not work
testabsprec := proc(x, y, prec::posint) 
    Digits := prec+10;
    verify(x, y, neighborhood(Float(1,-prec)));
end proc:

ReportTestFailures := proc(reports, $)
    local report;
    for report in reports do
        if report <> [] then
            #printf("\x1b[31m%s\x1b[0m", report[1]);
            printf("#####%s", report[1]);
        end if;
        map[2](printf, "%s", report[2..]);
        printf("\n");
    end do
end proc:
