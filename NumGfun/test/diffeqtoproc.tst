$include <testutils.mm>
with(gfun): with(NumGfun):

interface('warnlevel'=0): # :-(

for eq in [
    diffeqtohomdiffeq(holexprtodiffeq(arctan(z), y(z)), y(z)),
    holexprtodiffeq(exp(z), y(z)),
    holexprtodiffeq(erf(z), y(z)),
    holexprtodiffeq(AiryAi(z), y(z)), # lent
    ## deq(arctan) with missing/symbolic/mixed initial values
    -2*z*diff(y(z),z)+(-z^2-1)*diff(diff(y(z),z),z),
    {-2*z*diff(y(z),z)+(-z^2-1)*diff(diff(y(z),z),z)},
    { -2*z*diff(y(z),z)+(-1-z^2)*diff(y(z),z,z),
        y(0) = sqrt(Pi)+t, D(y)(0) = a },
    # random equations (thanks to A. Benoit)
    {10*(4-z)*(diff(y(z), z, z))+(1/10*(-55-7*z^2+22*z))
        *(diff(y(z), z))+(-94*z+87)*y(z), y(0) = 2, (D(y))(0) = 1},
    {10*diff(y(z), z, z)+(1/10*(97-62*z))*(diff(y(z), z))
        +(1/7*(-83-73*z^2-4*z))*y(z), y(0) = 2, (D(y))(0) = 1},
    NULL
] do
    ReportTestFailures([seq(
        Try("1",
            op(-1, [
                eq, # trick TestTools into displaying eq on failure
                diffeqtoproc(eq, y(z), prec = 20, disks = [ [[-1], 1/2],
                [0,3/4], [[1/2,1], 1/2] ])(pt, 16)
            ]),
            eval(evaldiffeq(eq, y(z), [pt], 16), ``=proc(x) x end)), # TBI
        pt in [ 0, 1/2, 1.15, -0.8234567887654, 1/3*(1+I) ])
    ]);
end do;


# catch hardcoded variable names

ReportTestFailures([

Try[testnoerror]("2",
diffeqtoproc((1+w^2)*diff(g(w),w)-1, g(w), prec=3, disks=[[0,1/4]]))

]);

# regression tests

ReportTestFailures([

Try("3",
diffeqtoproc({(x-1)*(x-20)*diff(f(x), x) = f(x), f(0) = 1}, f(x))(0.5),
1.0357739389
),

NULL ]);
