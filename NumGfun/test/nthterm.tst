$include <testutils.mm>
with(gfun):


rec1 := {u(n+1) = (n+1)*u(n), u(0) = 1}:
rec2 := {u(n+1) = u(n), u(0) = 1}:
rec3 := {u(n+3) = u(n) + u(n+1) + u(n+2), u(0) = 1, u(1) = 1, u(2) = 1}:

ReportTestFailures([

## Order 1

Try(1.1, nth_term(rec1,u(n),5), 120),

Try(1.2, nth_term(rec1,u(n),5,'series'), 34),

## Trivial cases

Try(2.1, nth_term(rec2,u(n),100), 1),

Try(2.2, nth_term(rec2,u(n),100,'series'), 100),

## Misc

Try(3.1, nth_term(rec3,u(n),100), 127071617887002752149434981),

Try(3.2, nth_term(rec3,u(n),100,'series'), 151404293106684183601223222),

NULL]);
