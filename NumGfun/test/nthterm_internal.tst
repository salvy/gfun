$include <testutils.mm>
with(gfun): with(NumGfun):

kernelopts('opaquemodules'=false):

ReportTestFailures([

####
#### makeitfloat
####

Try("makeitfloat 1",
    NumGfun:-nthterm:-makeitfloat(1/3, 2),
    0.33),

Try("makeitfloat 2",
    NumGfun:-nthterm:-makeitfloat(2/3, 2),
    0.67),

Try("makeitfloat 3",
    NumGfun:-nthterm:-makeitfloat(-2/3, 2),
    -0.67),

Try("makeitfloat 4",
    NumGfun:-nthterm:-makeitfloat(1/3-2/3*I, 2),
    0.33-0.67*I),

Try("makeitfloat 5",
    NumGfun:-nthterm:-makeitfloat(-995/1000+1005/1000*I, 2),
    -1.00+1.01*I),

NULL]);
