$include <testutils.mm>
with(gfun): with(NumGfun):

#_Env_TestTools_Try_printf := true:

interface('warnlevel'=0): # :-(

kernelopts('opaquemodules'=false):
cmp_algeb_abs := NumGfun:-recasympt:-cmp_algeb_abs:

t := RootOf(_Z^3+2,.6299605249-1.091123636*I):
t2 := RootOf(71-10*x^5+62*x^4-82*x^3+80*x^2-44*x, 4.816088382):

ReportTestFailures([

####
#### my_abs/RootOf and abs_with_RootOf
####

Try("1",
    convert(NumGfun:-utilities:-abs_with_RootOf(t), 'radical'),
    2^(1/3)),

# did we clean the remember tables of abs?
Try("2",
    abs(t),
    'abs'(t)),

# what do we do with non-RootOf expressions?
Try("3",
    NumGfun:-utilities:-abs_with_RootOf(1-sqrt(3)),
    sqrt(3)-1),

# larger example, harder to check though
Try[testnoerror]("4",
    NumGfun:-utilities:-abs_with_RootOf(
        RootOf(_Z^8-2*_Z+1,0.6110876563 + 0.8843359539*I))),

# RootOf subexpression
Try("5",
    eval(convert(NumGfun:-utilities:-abs_with_RootOf(t*sqrt(5)), 'radical')),
    2^(1/3)*5^(1/2)),

# already positive (should be catched by signum)
Try("6",
    NumGfun:-utilities:-abs_with_RootOf(t2),
    t2),

# ill-conditioned (infsolvepoly/isroot fails), gives a warning (but I don't
# how to test this)
Try("7",
    eval(convert(
        NumGfun:-utilities:-abs_with_RootOf(
                                          RootOf(-249377+11*_Z^2,-150.5677135)),
        'radical')),
    1/11*2743147^(1/2)),

# used to return unevaluated due to nested RootOf
Try("8",
    NumGfun:-utilities:-abs_with_RootOf(
                                1/RootOf(_Z^2+_Z+1,-.5000000000+.8660254038*I)),
    1),

####
#### cmp_algeb_abs
####

# Elementary cases

Try("10", cmp_algeb_abs(1, 2), -1),

Try("11", cmp_algeb_abs(2, 1), 1),

Try("12", cmp_algeb_abs(1, -2), -1),

Try("13", cmp_algeb_abs(2, -1), 1),

Try("14", cmp_algeb_abs(2, -2), 0),

# Mignotte-like polynomial with near-opposite roots

Try[testnoerror]("15.1", X^7-2*(1000*X^2-1), 'assign'='pol'),

Try("15.2",
    cmp_algeb_abs(RootOf(pol, 'index'=1), RootOf(pol, 'index'=5)),
    1),

Try("15.3",
    cmp_algeb_abs(RootOf(pol, 'index'=5), RootOf(pol, 'index'=1)),
    -1),

Try("15.4",
    cmp_algeb_abs(RootOf(pol, 'index'=4), RootOf(pol, 'index'=6)),
    0),

Try("15.5",  # algebraic and rational very close to it
    cmp_algeb_abs(
        RootOf(pol, 'index'=5),
        316227766019337933200017821973/10000000000000000000000000000000),
    -1),

NULL]);



####
#### complex intervals and related stuff
####

t := I*2^(1/2)-Pi:
iv := (evalrC(RootOf(x^7-3, 'index'=2)) + exp(sqrt(2)))/10^20:
ivref := 123207916058513/2500000000000000000000000000000000:

ReportTestFailures([

Try("1", signum(NumGfun:-utilities:-above_abs(t)-abs(t)), 1),

Try("2", signum(NumGfun:-utilities:-above_abs(iv)-ivref), 1),

Try("3", signum(NumGfun:-utilities:-below_abs(t)-abs(t)), -1),

Try("4", signum(NumGfun:-utilities:-below_abs(iv)-ivref), -1),

Try[testerror]("5", NumGfun:-utilities:-below_abs(0)),

Try("6",
signum(convert(NumGfun:-utilities:-below_abs(-1/3*I+evalrC(10^15+1/Pi)-10^15),
'rational', 'exact')-1/3),
-1),

NULL]);
