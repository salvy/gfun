# Test failures in this file are not necessarily bugs, but indicate that the
# documentation needs updating.

$include <testutils.mm>
with(gfun):
with(NumGfun):

#_Env_TestTools_Try_printf := true:

# Unfortunately, I don't know how to test that a given command prints a warning.
interface('warnlevel'=0):

alias(alpha=convert(sqrt(2), 'RootOf'));

ReportTestFailures([

### NumGfun.mw

Try("NumGfun.mw 1",
fnth_term({(3*n+3)*u(n+1)=(3*n+5)*u(n), u(0)=1}, u(n), 2000, 50),
175.89036294166519099188900014849043485353660447070009),

Try("NumGfun.mw 2.1",
holexprtodiffeq(arctan(z), y(z)),
{(z^2+1)*(diff(y(z), z))-1, y(0) = 0},
'assign'='deq'),

Try("NumGfun.mw 2.2",
evaldiffeq(deq, y(z), 1/2, 50),
0.46364760900080611621425623146121440202853705428612),

Try("NumGfun.mw 3.1",
(z^2+1)*diff(y(z),z,z) + (3*z+1)*diff(y(z),z) + z^2*y(z),
(z^2+1)*diff(y(z),z,z) + (3*z+1)*diff(y(z),z) + z^2*y(z),
'assign'='deq'),

Try("NumGfun.mw 3.2",
analytic_continuation(deq, y(z), [0, 1+I, 2], 50),
``(.72678326528197350565935299733280205125629707790244)*_C[0]
+``(.43578845882065137070719121052451044561729148136090)*_C[1]),

Try("NumGfun.mw 4.1",
{(z^2-1)*(diff(y(z), z, z))+(z^3+3)*y(z), y(0) = 1, (D(y))(0) = 0},
{(z^2-1)*(diff(y(z), z, z))+(z^3+3)*y(z), y(0) = 1, (D(y))(0) = 0},
'assign'='deq'),

Try("NumGfun.mw 4.2",
analytic_continuation(deq, y(z), [0, 1], 10, 'ord'=3),
``(4.7335543398)*(1-2*(z-1)*ln(z-1)+(-13/4+2*ln(z-1))*(z-1)^2)
+``(-8.3339545763+29.7417990787*I)*(z-1-(z-1)^2)),

Try("NumGfun.mw 5.1",
op(select(has, deq, z)),
(z^2-1)*(diff(y(z), z, z))+(z^3+3)*y(z),
'assign'='deq'),

Try[verify, Matrix]("NumGfun.mw 5.2",
transition_matrix(deq, y(z), [0, 1], 10),
Matrix(2,2,{(1, 1) = 4.7335543398, (1, 2) = 2.4577355851, (2, 1) = -8.33395457\
63+29.7417990787*I, (2, 2) = -4.1158620623+15.4424081173*I},datatype = 
anything,storage = rectangular,order = Fortran_order,shape = [])),

Try("NumGfun.mw 6",
bound_ratpoly((z^7+3*z^2+z+1)/((z-2)^3*(z^3-3)*(z-I)^2), z),
1794743353/(25000000000*(1-z)^2)
+(137935633/5000000000)*z^2
+(2309624043/50000000000)*z^3),

Try("NumGfun.mw 7.1",
holexprtodiffeq(arctan(z), y(z)),
{(z^2+1)*(diff(y(z), z))-1, y(0) = 0},
'assign'='deq'),

Try("NumGfun.mw 7.2",
bound_diffeq(deq, y(z)),
1/(2*(1-z)^2)),

### bound_diffeq.mw

Try("bound_diffeq.mw 1",
convert( # because of n
bound_diffeq({diff(y(z),z)=y(z), y(0)=1}, y(z)),
'`global`'),
Sum(1/GAMMA(n+1)*z^n,n = 0 .. infinity)),

Try("bound_diffeq.mw 2",
bound_diffeq({(1+z)*diff(y(z),z)=y(z), y(0)=1}, y(z)),
1/(-z+1)),

Try("bound_diffeq.mw 3",
bound_diffeq((1+z)*diff(y(z),z)=y(z), y(z)),
_C/(-z+1)),

Try("bound_diffeq.mw 4",
convert( # because of n
bound_diffeq({diff(y(z),z,z)=z*y(z), y(0)=1, D(y)(0)=1}, y(z)),
'`global`'),
1/5040*
Sum((n+1)*(n+2)*(n+3)*(n+4)*(n+5)*(n+6)*(n+7)*3^(-2/3*n)/GAMMA(1/3*n+1)^2*z^n,
n = 0 .. infinity)),

Try("bound_diffeq.mw 5",
bound_diffeq_tail({diff(y(z),z)=y(z), y(0)=1}, y(z), n),
piecewise(n <= abs(z)/(1-1/(abs(z)+3)), exp(abs(z)),
(n+3)*(abs(z)/(1-1/(n+3)))^n/GAMMA(n+1))),

Try("bound_diffeq.mw 6",
bound_diffeq_tail({(1+z)*diff(y(z),z)=y(z), y(0)=1}, y(z), n),
-abs(z)^n/(abs(z)-1)),

Try("bound_diffeq.mw 7",
bound_diffeq_tail(diff(y(z),z)=y(z), y(z), n),
piecewise(n <= abs(z)/(1-1/(abs(z)+3)), _C*exp(abs(z)),
_C*(n+3)*(abs(z)/(1-1/(n+3)))^n/GAMMA(n+1))),

### bound_ratpoly.mw

Try("bound_ratpoly.mw 1",
bound_ratpoly(1/(z+5),z),
1/5/(1-(1/5)*z)),

Try("bound_ratpoly.mw 2",
bound_ratpoly((5*z^6+z+2)/((z^3+1)^2*(z+I)),z),
16147093/(5000000*(1-z)^2)),

### bound_rec.mw

# here we check both that the bounds are the ones given on the help page and
# that they look correct

Try[testnoerror]("bound_rec.mw 1.1",
{I*u(n+1) = (n+1)*u(n), u(0)=3},
'assign'='rec'),

Try[testrecbound, u, n]("bound_rec.mw 1.2",
bound_rec(rec, u(n)),
rec),

Try("bound_rec.mw 1.3",
bound_rec(rec, u(n)),
(18849555927/1000000000*(n+2))*factorial(n)),

Try[testnoerror]("bound_rec.mw 2.1",
{(2*n+2)^2*u(n+1) = -(n+1)*u(n), u(0)=1},
'assign'='rec'),

Try[testrecbound, u, n]("bound_rec.mw 2.2",
bound_rec(rec, u(n)),
rec),

Try("bound_rec.mw 2.3",
bound_rec(rec, u(n)),
(n+2)*(n+1)*(1/4)^n/factorial(n)),

Try[testnoerror]("bound_rec.mw 3.1",
{15*u(n)-4*u(n+1)-13*u(n+2)+5*u(n+3), u(0) = 17/5, u(1) = 3, u(2) = 3/12, u(3) = 0, u(4) = 5, u(5) = 0},
'assign'='rec'),

Try[testrecbound, u, n]("bound_rec.mw 3.2",
bound_rec(rec, u(n)),
rec),

Try("bound_rec.mw 3.3",
subsindets(convert(bound_rec(rec, u(n)), '`global`'), 'piecewise', simplify,
'piecewise'), # normalize case order
(3650256453/1000000000)*RootOf(15*_Z^3-4*_Z^2-13*_Z+5, index = 1)^(-n)
+ PIECEWISE([1349743549/1000000000,  n = 4], [3497435481/10000000000, n = 6],
[9497435481/10000000000, n = 7], [0, 'otherwise'])),

Try("bound_rec.mw 4",
bound_rec_tail({I*(n+1)*u(n+1) = u(n), u(0)=1}, u(n)),
piecewise(n <= 2, exp(1), (n+3)*(1/(1-1/(n+3)))^n/GAMMA(n+1))),

### diffeqtoproc.mw

Try[testnoerror]("diffeqtoproc.mw 1",
gfun:-diffeqtohomdiffeq(gfun:-holexprtodiffeq(arctan(z),y(z)),y(z)),
'assign'='eq'),

Try[testnoerror]("diffeqtoproc.mw 2",
diffeqtoproc(eq, y(z)),
'assign'='p'),

Try("diffeqtoproc.mw 3",
[ p([0, 1+I, 2*I], 30), p([0, -1+I, 2*I], 30) ],
[1.570796326794896619231321691640+.549306144334054845697622618461*I,
-1.570796326794896619231321691640+.549306144334054845697622618461*I]),

Try[testnoerror]("diffeqtoproc.mw 4",
diffeqtoproc(gfun:-holexprtodiffeq(AiryAi(z), y(z)), y(z), 'prec'=12,
'disks'=[[-2.5,3],[2.5,3]]),
'assign'='p'),

# XXX: plot not tested

Try("diffeqtoproc.mw 6",
p(-3, 10),
-0.3788142937),

Try("diffeqtoproc.mw 7",
p(-3, 30), -.378814293677658074347243916500),

Try("diffeqtoproc.mw 8",
p(-42), 0.0073966067),

### dominant_root.mw

Try("dominant_root.mw 1",
dominant_root((z-1)*(z^2+1)^2, z),
[I, 2]),

Try("dominant_root.mw 2",
dominant_root((z-1)*(z^2+1)^2, z, 'rootof'),
[RootOf(_Z^2+1, index = 1), 2]),

### evaldiffeq.mw

Try("evaldiffeq.mw 1",
evaldiffeq({diff(y(z),z)-y(z), y(0)=1}, y(z), 1, 50),
2.71828182845904523536028747135266249775724709369996),

Try("evaldiffeq.mw 2",
analytic_continuation({(1+z^2)*diff(y(z),z,z)-(2*z+3)*y(z)+I*y(z), y(0)=Pi, D(y)(0)=-I}, y(z), [2]),
37.6769250446-22.6773530382*I),

Try[testnoerror]("evaldiffeq.mw 3.1",
gfun:-diffeqtohomdiffeq(gfun:-holexprtodiffeq(arctan(z),y(z)),y(z)),
'assign'='eq'),

Try[verify, Matrix]("evaldiffeq.mw 3.2",
transition_matrix(eq, y(z), [2*I, -1+I, 0, 1+I, 2*I], 20),
Matrix(2, 2, {
    ( 1, 2 ) = -9.42477796076937971539,
    ( 2, 1 ) = 0.,
    ( 1, 1 ) = 1.00000000000000000000,
    ( 2, 2 ) = 1.00000000000000000000})),

Try("evaldiffeq.mw 4",
evaldiffeq(eq, y(z), [0, I], 30, 'ord'=3),
``(-.500000000000000000000000000000*I)*(ln(z-I)+1/2*I*(z-I)-1/8*(z-I)^2)+``(.7\
85398163397448309615660845820+.346573590279972654708616060729*I)),

Try("evaldiffeq.mw 5",
convert(
evaldiffeq(eq, y(z), [0, I], 30, 'monomials'),
'`global`'),
``(-.500000000000000000000000000000*I)*(series(ln(z-I)+1*`...`,`...`))+``(.785\
398163397448309615660845820+.346573590279972654708616060729*I)*(series(1+1*
`...`,`...`))),

Try("evaldiffeq.mw 6",
evaldiffeq(eq, y(z), [0, I], 30),
-.500000000000000000000000000000*I),

Try("evaldiffeq.mw 7.1",
op(select(has, eq, z)),
-2*z*(diff(y(z), z))+(-z^2-1)*(diff(y(z), z, z)),
'assign'='eq'),

Try("evaldiffeq.mw 7.2",
evaldiffeq(eq, y(z), [0, I], 10, 'ord'=3),
``(``(0.)*_C[0]+``(-.5000000000*I)*_C[1])*(ln(z-I)+1/2*I*(z-I)-1/8*(z-I)^2)+``
(``(1.0000000000)*_C[0]+``(.7853981634+.3465735903*I)*_C[1])),

Try("evaldiffeq.mw 8",
[local_basis(eq, y(z), 0), local_basis(eq, y(z), I)],
[[1, z-(1/3)*z^3+(1/5)*z^5], [ln(z-I)+(1/2*I)*(z-I)-(1/8)*(z-I)^2-(1/24*I)*(z-I)^3+(1/64)*(z-I)^4+(1/160*I)*(z-I)^5, 1]]),

Try[verify, Matrix]("evaldiffeq.mw 8",
transition_matrix(eq, y(z), [0, I], 10),
Matrix(2, 2, {(1, 1) = 0., (1, 2) = -.5000000000*I, (2, 1) = 1.0000000000, (2,
2) = .7853981634+.3465735903*I})),

Try[testnoerror]("evaldiffeq.mw 9.1",
(z^2-2)*diff(y(z),z,z) + z*y(z) + y(z),
'assign'='eq'),

# cf. alias declaration

Try("evaldiffeq.mw 9.2",
local_basis(eq, y(z), alpha, 3),
[1-(1/4*(2+sqrt(2)))*(z-sqrt(2))*ln(z-sqrt(2))+(1/32*(3+2*sqrt(2)))*(11-10*sqrt(2)+2*ln(z-sqrt(2)))*(z-sqrt(2))^2,
z-sqrt(2)+(-(1/8)*sqrt(2)-1/4)*(z-sqrt(2))^2]),

Try[verify, Matrix]("evaldiffeq.mw 9.3",
transition_matrix(eq, y(z), [0, 1, alpha]),
Matrix(2, 2, {(1, 1) = 2.4938814696, (1, 2) = 2.4089417847, (2, 1) =
-.2035417759+6.6873857098*I, (2, 2) = .2043720671+6.4596184954*I})),

Try("evaldiffeq.mw 10.1",
holexprtodiffeq(Ei(z), y(z)),
(-z+1)*(diff(y(z), z))+z*(diff(y(z), z, z)),
'assign'='eq'),

Try("evaldiffeq.mw 10.2",
evaldiffeq(eq, y(z), [0, 1], 20),
``(1.31790215145440389486)*_C[0]+``(1.00000000000000000000)*_C[1]),

### local_basis.mw

Try[testnoerror]("local_basis.mw 1.1",
diff(y(z),z,z,z)+2*(diff(y(z), z))+y(z),
'assign'='eq'),

Try("local_basis.mw 1.2",
local_basis(eq, y(z), 0),
[1-(1/6)*z^3+(1/60)*z^5, z-(1/3)*z^3-(1/24)*z^4+(1/30)*z^5, z^2-(1/6)*z^4-(1/60)*z^5]),

Try("local_basis.mw 1.3",
local_basis(eq, y(z), I),
[1-(1/6)*(z-I)^3+(1/60)*(z-I)^5, z-I-(1/3)*(z-I)^3-(1/24)*(z-I)^4+(1/30)*(z-I)^5, (z-I)^2-(1/6)*(z-I)^4-(1/60)*(z-I)^5]),

Try[testnoerror]("local_basis.mw 2.1",
z*diff(y(z),z,z,z)+2*y(z),
'assign'='eq'),

Try("local_basis.mw 2.2",
local_basis(eq, y(z), 0),
[1-z^2*ln(z)+(-13/144+(1/12)*ln(z))*z^4, z-(1/3)*z^3+(1/90)*z^5, z^2-(1/12)*z^4]),

Try[testnoerror]("local_basis.mw 3.1",
z^4*(diff(y(z), z$4))+2*z^3*(diff(y(z), z$3))-3*z^2*(diff(y(z), z, z))+3*z*(diff(y(z), z))+(z+1)*y(z),
'assign'='eq'),

Try("local_basis.mw 3.2",
local_basis(eq, y(z), 0, 2),
[z^(1-sqrt(2))*ln(z)-(1/343*(-12+4*sqrt(2)+7*ln(z)))*z^(2-sqrt(2))*(9+4*sqrt(2)),
z^(1-sqrt(2))-(1/49)*z^(2-sqrt(2))*(9+4*sqrt(2)),
z^(1+sqrt(2))*ln(z)+(1/343)*z^(2+sqrt(2))*(-12-4*sqrt(2)+7*ln(z))*(-9+4*sqrt(2)),
z^(1+sqrt(2))+(1/49)*z^(2+sqrt(2))*(-9+4*sqrt(2))]),

Try("local_basis.mw 3.3",
local_basis(eq, y(z), RootOf(_Z^3-1, 'index'=2)),
[1+((1/48*I)*sqrt(3)-1/48)*(z-(-1)^(2/3))^4+(-11/240+(1/240*I)*sqrt(3))*(z-(-1)^(2/3))^5, z-(-1)^(2/3)-(1/8)*(z-(-1)^(2/3))^4+(-1/15-(7/120*I)*sqrt(3))*(z-(-1)^(2/3))^5, (z-(-1)^(2/3))^2+(-1/8+(1/8*I)*sqrt(3))*(z-(-1)^(2/3))^4-(1/4)*(z-(-1)^(2/3))^5, (z-(-1)^(2/3))^3+((1/4*I)*sqrt(3)+1/4)*(z-(-1)^(2/3))^4+((9/40*I)*sqrt(3)-9/40)*(z-(-1)^(2/3))^5]),

### plot_path.mw

Try[testnoerror]("plot_path.mw 1",
plot_path((1+z^2)*diff(y(z),z,z)+y(z), y(z), [0, 1+I, 2*I], 'rewrite')),

NULL]);
