$include <testutils.mm>
kernelopts('opaquemodules'=false):

ReportTestFailures([

Try("1",
    gfun:-NumGfun:-diffeq_for_derivative({(1+z^2)*diff(y(z),z)-1, y(0) = 0}, y(z)),
    {2*z*y(z)+(1+z^2)*diff(y(z),z), y(0) = 1}),

Try("2",
    gfun:-NumGfun:-diffeq_for_derivative(
        gfun:-NumGfun:-diffeq_for_derivative(
            {2*z*diff(y(z),z)+diff(y(z),`$`(z,2)), y(0) = 0,
                D(y)(0) = 2/Pi^(1/2)},
            y(z)),
        y(z)),
    {2*diff(y(z),z)*z+4*y(z)+diff(y(z),`$`(z,2)),
        y(0) = 0, D(y)(0) = -4/Pi^(1/2)}),

Try[verify, 'interval']("4",
gfun:-NumGfun:-bound_normal_diffeq:-bound_ratpoly_at_posints( -625000000000000000000000000000000000000*(56*x^2+(28*I)*2^(1/2)*x-88*2^(1/2)*x+213-44*I)^2*(94*x-87)/((56*x^5+62*x^3-97*x^2+73*x+4)*(-1+x)*(-500000000000000000001+100000000000000000000*x)^2), x, 3),
2.6635e39..3e39),

Try("4",
gfun:-NumGfun:-bound_normal_diffeq:-bound_ratpoly_at_posints(42+1/x, x, 0),
43.),

Try("5",
gfun:-NumGfun:-bound_normal_diffeq:-bound_ratpoly_at_posints(42+1/x, x, 3),
43.),

Try("5",
gfun:-NumGfun:-bound_normal_diffeq:-bound_ratpoly_at_posints((y-10)^2/(y+10)^2,y,0),
1.),

Try("5",
gfun:-NumGfun:-bound_normal_diffeq:-bound_ratpoly_at_posints(x/(x+2)^2,x,0),
0.125),

Try("6",
gfun:-NumGfun:-utilities:-diffeq_infsing({(x-1)*(x-20)*diff(f(x), x) = f(x),
f(0) = 1}, f(x), 'numeric'=true),
1.0
),

Try("7",
gfun:-NumGfun:-utilities:-diffeq_infsing({(x-1)*(x-20)*diff(f(x), x) = f(x),
f(0) = 1}, f(x)),
1
),

NULL]);
