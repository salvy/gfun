# Regular singular points

$include <testutils.mm>
with(gfun): with(NumGfun):

#_Env_TestTools_Try_printf := true:

interface('warnlevel'=0): # TBI


deq := diffeqtohomdiffeq(holexprtodiffeq(arctan(z), y(z)), y(z)):
deq2 := holexprtodiffeq(arctan(w), g(w)):

# local basis = [1, arctan]
# expand(op(1,series(arctan(z), z=I, 1))) assuming z-I>0:
# 1/2*I*ln(2)-1/2*I*ln(z-I)+1/4*Pi
dirI := Matrix([[0, -I/2], [1, Pi/4 + I/2*ln(2)]]):
invI := dirI^(-1):
id := Matrix([[1,0],[0,1]]):


Whittaker_f :=
-sin((1+2*sqrt(3))*Pi)*GAMMA(1-2*sqrt(3))*GAMMA(-3/2+sqrt(3))/Pi
* WhittakerW(2, sqrt(3), z)
+ GAMMA(1-2*sqrt(3))*GAMMA(-3/2+sqrt(3))/(GAMMA(1+2*sqrt(3))*GAMMA(-3/2-sqrt(3)))
*WhittakerM(2, sqrt(3), z):

Whittaker_g := WhittakerM(2, sqrt(3), z):

Whittaker_mat := Matrix([[Whittaker_f,Whittaker_g],
[diff(Whittaker_f,z),diff(Whittaker_g,z)]]):

# Test various forms of singular analytic continuation paths

ReportTestFailures([

seq(
    Try[verify, 'Matrix(neighborhood(10^(-10)))'](1,
        transition_matrix(deq, y(z), op(1, data), 10),
        evalf[20](op(2, data))),
data in [
    [ [0, I],       dirI ],
    [ [I, 0],       invI ],
    [ [I],          id   ],
    [ [I, I],       id   ],
    [ [I, I, I],    id   ],
    [ [0, 1, I],    dirI ],
    [ [0, I, 0],    id   ],
    [ [I, 1, I],    id   ],
    [ [I, -I, 0],   invI ],
    NULL ]
),

# Incorrect paths

seq(
    Try[testerror](2, transition_matrix(deq, y(z), path)),
path in [
    I,
    2*I,
    [0, 2*I],
    [I, 0, 2*I]
]),

# Irregular singular point

Try[testerror](3,
    transition_matrix(holexprtodiffeq(exp(1/(1-z)), y(z)), y(z), [0,1], 10)),

# Bug reported by Christoph Koutschan: we used not to detect additional
# singular points on a straight-line path whose endpoints were themselves
# regular singular points
Try[testerror](4,
transition_matrix((-8*z^3+4*z^4+5*z^2-z)*diff(y(z),z)+10*z^2-4*z-8*z^3+1 , y(z),
[0,1], 10)),

# catch harcoded variable names

Try[testnoerror](5,
    transition_matrix(deq2, g(w), [0,I])),

Try[testnoerror](6,
    analytic_continuation(deq2, g(w), [0,I], ord=3)),

### Constant factor in the asymptotic expansion of Apéry numbers

Try[testnoerror]("7.1",
z^2*(z^2-34*z+1)*(diff(diff(diff(y(z), z), z), z))+3*z*(2*z^2-51*z+1)*(diff(diff(y(z), z), z))+(z-5)*y(z)+(7*z^2-112*z+1)*(diff(y(z), z)),
'assign'='eq'),

# only one of the basic solutions is analytic at the origin...
Try("7.2",
local_basis(eq, y(z), 0, 3),
[(12*ln(z)+(5/2)*ln(z)^2)*z+(1/2)*ln(z)^2+(72+210*ln(z)+(73/2)*ln(z)^2)*z^2, ln(z)+(12+5*ln(z))*z+(210+73*ln(z))*z^2, 1+5*z+73*z^2]),

Try("7.3",
NumGfun:-utilities:-diffeq_infsing(eq, y(z), 'nonzero'),
RootOf(_Z^2-34*_Z+1, index = 1),
'assign'='alpha'),

# ...all basic sols but one are analytic at α...
Try("7.4",
local_basis(eq, y(z), alpha, 3),
[1+(-239/12*2^(1/2)-169/6)*(z-17+12*2^(1/2))^2, (z-17+12*2^(1/2))^(1/2)+(-203/32*2^(1/2)-9)*(z-17+12*2^(1/2))^(3/2)+(24031/160*2^(1/2)+1087523/5120)*(z-17+12*2^(1/2))^(5/2), z-17+12*2^(1/2)+(-55/6*2^(1/2)-13)*(z-17+12*2^(1/2))^2]),

# ...so we are interested in mat[2,3]
Try[testabsprec,50]("7.5",
transition_matrix(eq, y(z), [0, alpha], 50)[2,3],
#-((1/4)*I)*(1+2^(1/2))^2*2^(3/4)/(Pi*(2*2^(1/2)-3))
# (see, e.g., M. D. Hirschhorn, Estimating the Apéry numbers, Fibonacci
# Quart. 50 (2012), 129--131)
4.54637624752284460023959302491516155330304971072320109032758*I
),

### Bessel functions

Try("8.1",
holexprtodiffeq(BesselJ(0,z), y(z))[1],
(diff(y(z), z, z))*z+y(z)*z+diff(y(z), z),
'assign'='deq'),

Try("8.2",
local_basis(deq, y(z),0,5),
[ln(z)+(1/4-(1/4)*ln(z))*z^2+(-3/128+(1/64)*ln(z))*z^4,
1-(1/4)*z^2+(1/64)*z^4]),

Try[verify, 'neighborhood'(1e-5)]("8.3",
RootOf(_Z^3 + 20, index=1),
1.357208808+2.350754612*I,
'assign'='alg'),

Try[testnoerror]("8.4",
[(ln(2)-gamma-Pi*I/2)*BesselJ(0,z)-BesselK(0,I*z), BesselJ(0,z)],
'assign'=row),

Try[verify, 'Matrix(neighborhood(1e-30))']("8.4",
transition_matrix(deq, y(z), [0, alg], 30),
evalf[40](subs(z=alg, Matrix([row, diff(row, z)])))),

### Sine and Cosine Integrals, both at Gaussian rational points and at RootOfs

Try[testnoerror]("9.1",
analytic_continuation(
    (diff(y(z), z))*z+2*(diff(y(z), z, z))+z*(diff(y(z), z, z, z)),
    y(z), [0, 456/123*I+1], 10),
'assign'='res'),

Try[testabsprec, 10]("9.2",
op(coeff(res, _C[0])),
evalf[12](Ci(456/123*I+1)-gamma)),

Try[testabsprec, 10]("9.3",
op(coeff(res, _C[2])),
evalf[12](Si(456/123*I+1))),

Try[testnoerror]("9.4",
analytic_continuation(
    (diff(y(z), z))*z+2*(diff(y(z), z, z))+z*(diff(y(z), z, z, z)),
    y(z), [0, convert(sqrt(2), 'RootOf')], 10),
'assign'='res'),

Try[testabsprec, 10]("9.5",
op(coeff(res, _C[0])),
evalf[12](Ci(sqrt(2))-gamma)),

Try[testabsprec, 10]("9.6",
op(coeff(res, _C[2])),
evalf[12](Si(sqrt(2)))),

Try[verify, 'Matrix(neighborhood(1e-10))']("9.7",
transition_matrix(
    (diff(y(z), z))*z+2*(diff(y(z), z, z))+z*(diff(y(z), z, z, z)),
    y(z), [-1, 0, I, -1], 10),
LinearAlgebra:-IdentityMatrix(3)),

### dilog

Try[verify, 'Matrix(neighborhood(1e-10))']("10.1",
transition_matrix(
    diff(y(z), z)+(-1+3*z)*(diff(y(z), z, z))+(z^2-z)*(diff(y(z), z, z, z)),
    y(z), [1,0], 10),
evalf[15](
# NOT independently checked--but let's at least test that we are consistent with
# ourselves
Matrix(3, 3, {(1, 1) = I*Pi, (1, 2) = 1, (1, 3) = -(1/6)*Pi^2, (2, 1) = 0, (2,
2) = 0, (2, 3) = -1, (3, 1) = -1, (3, 2) = 0, (3, 3) = 1})
)),

### Irrational exponents—Whittaker functions

Try("11.1",
local_basis((-z^2+8*z-11)*y(z)+4*(diff(y(z), z, z))*z^2, y(z), 0, 10),
[
# Whittaker_f, but MultiSeries gets it slightly wrong
z^(1/2-sqrt(3))+
(2/11*(1+
2*sqrt(3)))*z^(3/2-sqrt(3))+
(1/352*(101+
37*sqrt(3)))*z^(5/2-sqrt(3))+
(1/528*(135+
83*sqrt(3)))*z^(7/2-sqrt(3))-(1/33792*(7365+
4291*sqrt(3)))*z^(9/2-sqrt(3))+
(1/16896*(1107+
641*sqrt(3)))*z^(11/2-sqrt(3))-(13/1622016*(1524+
881*sqrt(3)))*z^(13/2-sqrt(3))+
(1/811008*(1338+
773*sqrt(3)))*z^(15/2-sqrt(3))-(1/1349517312*(236343+
136504*sqrt(3)))*z^(17/2-sqrt(3))+
(1/674758656*(10331+
5966*sqrt(3)))*z^(19/2-sqrt(3)),
convert(map(radnormal, MultiSeries:-series(Whittaker_g, z, 10)),
'polynom')]),

Try[verify, 'Matrix(neighborhood(1e-90))']("11.2",
transition_matrix((-z^2+8*z-11)*y(z)+4*(diff(y(z),z,z))*z^2, y(z), [0,10], 90),
evalf[100](subs(z=10, Whittaker_mat))),

Try[verify, 'Matrix(neighborhood(1e-10))']("11.3",
transition_matrix((-z^2+8*z-11)*y(z)+4*(diff(y(z),z,z))*z^2, y(z),
[0,RootOf(_Z^6+86*_Z^5+71*_Z^4-80*_Z^3+2*_Z^2+7*_Z+24, index = 1)], 10),
evalf[15](subs(z=RootOf(_Z^6+86*_Z^5+71*_Z^4-80*_Z^3+2*_Z^2+7*_Z+24, index=1),
Whittaker_mat))),

Try[verify, 'Matrix(neighborhood(1e-10))']("11.4",
transition_matrix((-z^2+8*z-11)*y(z)+4*(diff(y(z),z,z))*z^2, y(z),
[0,RootOf(95*_Z^5+11*_Z^4-49*_Z^3-47*_Z^2+40*_Z-81, index = 3)], 10),
evalf[15](subs(z=RootOf(95*_Z^5+11*_Z^4-49*_Z^3-47*_Z^2+40*_Z-81, index = 3),
Whittaker_mat))),

### A simple diffeq with algebraic solutions

Try("12.1",
op(select(has, algeqtodiffeq(x*f^3+1*f+1=0, f(x)), x)),
6*f(x)+(54*x+6)*(diff(f(x), x))+(27*x^2+4*x)*(diff(f(x), x, x)),
'assign'=deq_alg),

Try("12.2",
local_basis(deq_alg, f(x), 0, 2),
[1/sqrt(x)+(3/8)*sqrt(x), 1-x]),

Try[verify, 'Matrix(neighborhood(1e-30))']("12.3",
transition_matrix(deq_alg, f(x), [0,1/2], 30),
Matrix(2, 2, {
(1, 1) = 1.563884510526955938205456995014, # I/2*(sol[2]+sol[3]) in the output
                                           # of fsolve with Maple 17
(1, 2) = .770916997059248100825146369307,
(2, 1) = -1.368421376494809487796840208800,
(2, 2) = -.242227537868895947195424081255})),

NULL]);
