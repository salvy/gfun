with(gfun:-NumGfun):
$include <testutils.mm>


ReportTestFailures([

Try("1", dominant_root(z+1, z), [-1, 1]),

Try("2", dominant_root(z^2+2*z-2*I*z-2*I, z), [I-1, 2]),

Try("3", dominant_root((-2+z)*(2+z)^2, z), [-2, 2]),

# roots of close absolute value belonging to different irreducible factors
Try("4", dominant_root(expand((z+1)^4*(z-1+1/10^20)), z),
[1-1/10^20, 1]),

# several dominant roots belonging to different irreducible factors
Try[member]("5", dominant_root(z^2-1, z), [ [1, 1], [-1,1] ]),

# Mignotte polynomial (irreducible polynomial with close roots)
Try[verify, 'neighborhood(1e-20)']("6",
evalf[30](dominant_root(X^7-2*(1000*X-1)^2, X)),
evalf[30]([RootOf(_Z^7-2000000*_Z^2+4000*_Z-2,.99999999997763932023e-3), 1])),

# Mignotte-like irreducible polynomial with near-opposite roots
Try[verify, 'neighborhood(1e-20)']("7",
evalf[30](op(1, dominant_root(X^7-2*(1000*X^2-1), X))),
evalf[30](RootOf(_Z^7-2000*_Z^2+2,-.31622776601433793320e-1))),

# similar example with non-real roots
Try[verify, 'neighborhood(1e-20)']("8",
evalf[30](dominant_root(X^7-2*(1000*X^2-I+1), X)),
evalf[30]([RootOf(_Z^7-2000*_Z^2-2+2*I,-.14391204993750742880e-1
        -.34743442275511562818e-1*I), 1])),

Try("9", dominant_root((z^8-1)*(z-I), z), [I, 2]),

Try[testnoerror]("10", dominant_root(z^2+z+1, z)),

Try[testnoerror]("11",
dominant_root((-6311369*z^2-9420000+131250*I)*(2*z^2+1)^2
        *(-6311369*z^2-3140000+43750*I)^2,z)),

# this used to loop forever beacause of an abs() missing in irreducible/check
Try[testnoerror]("12", dominant_root(z^3-2*z+5, z)),

# variable
Try("13", dominant_root(x^2+1, x), [I, 1]),

# algebraic coefficients etc.

Try("14", dominant_root(x^2-sqrt(2), x), [2^(1/4), 1]),

Try(15,
dominant_root(
36578304*x^20+47806848*x^19-15577488*x^18+85944338*x^17+142744699*x^16
+19714501*x^15+61107535*x^14+60269006*x^13+44741174*x^12-21707431*x^11
- 88807013*x^10-66913466*x^9-117725375*x^8-90020302*x^7-108486403*x^6
-75705229*x^5-60011354*x^4-29734889*x^3-13625882*x^2-3414460*x-707281, x),
[RootOf(72*_Z^5+37*_Z^4-23*_Z^3+87*_Z^2+44*_Z+29,index = 2), 3]),

Try[verify, 'neighborhood(1e-10)'](16,
evalf(abs(dominant_root(
x^3+(RootOf(7*_Z^5-22*_Z^4+55*_Z^3+94*_Z^2-87*_Z+56,index=2)
+2^(1/2))*x^2+RootOf(7*_Z^5-22*_Z^4+55*_Z^3+94*_Z^2-87*_Z+56,index=2)^2
+RootOf(7*_Z^5-22*_Z^4+55*_Z^3+94*_Z^2-87*_Z+56,index = 2)+1, x)[1])),
1.63684506108002),

# à la Mignotte with algebraic coefficients
Try(17, dominant_root(X^7-2*(1000*(-3)^(1/3)*X^2-1), X),
[RootOf(_Z^21+6*_Z^14+12*_Z^7+24000000000*_Z^6+8,index = 20), 1]),

Try(18,
dominant_root(
1545*_Z^7+12*_Z*RootOf(7*_Z^5-22*_Z^4+55*_Z^3+94*_Z^2-87*_Z+56, index = 1)
-2*RootOf(7*_Z^5-22*_Z^4+55*_Z^3+94*_Z^2-87*_Z+56, index = 1)^2+5, _Z),
[RootOf(143786212528021875*_Z^35+3509899223985000*_Z^29+3413035122024375*_Z^28
+68153382990000*_Z^23-48503966076000*_Z^22+58345884983250*_Z^21
-904701571200*_Z^17+577354510800*_Z^16-744484453200*_Z^15+299248593450*_Z^14
-6503535360*_Z^11-5937546240*_Z^10+1637971920*_Z^9-3679967520*_Z^8
+463150315*_Z^7-32514048*_Z^5-4015872*_Z^4-16836480*_Z^3-457296*_Z^2
-5829272*_Z-14449, index = 13), 1]
),

NULL

]):

