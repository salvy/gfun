$include <testutils.mm>
with(gfun): with(NumGfun):


################################################################################

# bound_ratpoly

for rat in [
    (z^7+3*z^2+z+1)/((z-2)^3*(z^3-3)*(z-I)^2),
    (1+2*z)/((z^2-1)*(z-1/2)),
    1/(1-z)^2*(1-2*z)^2,
    1/((1-z)*(1-2*z)^5),
    (2*z^2-9*z+8)/((2-z)*(2+z)^2),
    (1+z^2)/(1-2*z^2+z^4),
    0,
    -56-7*z^5+22*z^4-55*z^3-94*z^2+87*z,
    NULL]
do
    ReportTestFailures([
        Try[testmajseries, z]("1.1",
            bound_ratpoly(rat,z),
            rat),
        Try[testmajseries, z]("1.2",
            bound_diffeq(y(z)=rat,y(z)),
            rat),
        NULL]);
end do;

ReportTestFailures([Try("2", bound_ratpoly(1/z,z), 1/z)]);

################################################################################

# bound_diffeq etc. I: explicit expressions

for expr in [
    log(1-z)/(z^3-2)^3,
    ## 1 is irregular singular
    exp(1/(1-z))/exp(1),
    cos(1/(1-z)),
    1/exp(1)*exp(1/(1-z))+1/(1+z/3),
    ## noninteger Gevrey order
    AiryAi(z),
    ## regular singular point at the origin
    BesselJ(1,z),
    NULL]
do
    ReportTestFailures([
        Try[testnoerror]("2.1", holexprtodiffeq(expr,y(z)), 'assign'='deq'),
        Try[testmajseries, z]("2.2",
            bound_diffeq(deq, y(z)),
            expr),
        # TODO: simple way to test the result?
        Try[testnoerror]("2.3",
            bound_diffeq_tail(deq, y(z), n)),
        NULL])
end do;

ReportTestFailures([

## used to fail due to a bad rounding although the non-homogeneous version
## worked
Try[testmajseries, z]("3",
    bound_diffeq( {-2*z*diff(y(z),z)+(-1-z^2)*diff(y(z),`$`(z,2)), y(0) = 0,
    D(y)(0) = 1}, y(z)),
    arctan(z)),

Try[testerror]("4",
    bound_diffeq(1+(-z+z^2)*y(z), y(z))),  # No power series sol

NULL]);

################################################################################

interface('warnlevel'=0):

# bound_diffeq etc. II

for deq in [
    ## apparent singularity, no initial values
    { -z*(z-1)^2*diff(y(z),z,z) +(z-1)*(z^2-2*z-1)*diff(y(z),z) + (z^2-1)*y(z) = 0 },
    ## apparent dominant singularity at z=-0.618033...
    ## => the current version of bound_diffeq does *not* return a tight bound
    ## (but we can desingularize the equation)
    { diffeqtohomdiffeq(holexprtodiffeq(exp(z/(1-z))+log(z), y(z)), y(z)) }, 
    ## no finite singularity
    { diff(y(z),z) = y(z), y(0)=1 } ,
    ## here the root that determines the irregularity is not the dominant root
    diff(y(z),z,z,z)=1/(1-z)^2*diff(y(z),z,z)+1/(z^2+1)^3*y(z) ,
    ## non-positive dominant sing
    (1-z^3)*diff(y(z),z) = y(z),
    ## Used to loop up to Settings:-max_indicial_eq_tail_index
    -z^2*(z-5)*diff(diff(diff(y(z),z),z),z) -
        z*(2*z-15)*diff(diff(y(z),z),z) + (-z^3+
        5*z^2+25*z-120)*diff(y(z),z)-(z-5)^2*y(z),
    NULL]
do
    ReportTestFailures([
        Try[testnoerror]("5.1",
            bound_diffeq(deq, y(z))),
        Try[testnoerror]("5.2",
            bound_diffeq_tail(deq, y(z), n)),
        NULL]);
end do;

# This one should return a divergent series after printing a warning.  I don't
# know how to test for this behaviour.
#bound_diffeq(rectodiffeq({u(n+1)=(n+1)*u(n), u(0)=1}, u(n), y(z)), y(z)):

################################################################################

# bound_rec

ReportTestFailures([seq(
    Try[testrecbound, u, n]("6", bound_rec(rec, u(n)), rec),
    rec in [
    { u(n+1) = u(n), u(0) = 0 },
    { u(n+1) = n*u(n), u(0) = 1 }, # used to fail (too clever diffeq<->rec)
    { u(n+1) = u(n), u(0) = 18 },
    { u(n+1) = (n+1) * u(n), u(0) = 0 },
    { u(n+1) = (n+1) * u(n), u(0) = 1 },
    { (n+2)*u(n+2) = u(n), u(0)=1, u(1)=1 },
    { u(n+2)=u(n+1)+(n+1)*u(n), u(0)=1, u(1)=3 },
    { (n + 1)*u(n + 1) = (2*n + 3)*u(n), u(0) = 1 },
    { (2*n + 1)*u(n + 1) = (n + 3)*u(n), u(0) = 1 },
    { u(n+1)=u(n)+1, u(0)=-1 },
    { u(n+1)=u(n)+1, u(0)=1 },  # polynomial bound
    { u(n+1)=u(n)+1, u(0)=-2 },
    ## Example 2 from Mezzarobba & Salvy (2010)
    { 2*u(n+3) = (n+2)*u(n+1) + u(n),
        u(0)=1/5, u(1)=1/5, u(2)=1/5 }, # (a) integrals
    { u(n+2) = (n+1)*u(n) + u(n+1),
        u(0)=1, u(1)=1 }, # (b) involutions
    ##
    { (n+2)*(n+3)*u(n) = (7*n^2+7*n-2)*u(n-1) + 8*(n-1)*(n-2)*u(n-2),
        u(0)=1, u(1)=1 }, # Baxter
    ## this used to trigger a bug related to multiplicities in dominant_root
    { 25*u(n)+(-30*n-36-6*n^2)*u(n+2)+(128+14*n^3+72*n^2+160*n+n^4)*u(n+4),
        u(0) = 1, u(1) = 0, u(2) = 3/4, u(3) = 0},
    NULL])
]);

## Example 2(c) from Mezzarobba & Salvy (2010): Chudnovsky & Chudnovsky's
## formula for computing π
A := 13591409:
B := 545140134:
C := 640320^3:
rec := { (A+B*n) * (3*n+3)* (3*n+2)*(3*n+1) * (n+1)^3 * C * u(n+1)
         + (6*n+6)*(6*n+5)*(6*n+4)*(6*n+3)*(6*n+2)*(6*n+1) * (A+B*(n+1)) * u(n),
         u(0) = A }:
ReportTestFailures([
    Try[testnoerror]("7", bound_rec_tail(rec, u(n)))
]);

################################################################################

# degenerate cases: deqs of order zero, one-term recs

testgeqwithindets := proc(computed, expected)
    global _C, z;
    local inds;
    inds := indets(expected);
    if indets(computed) = {} and inds = {} then
        evalb(computed >= expected)
    elif member(_C, inds) then
        type(computed, 'linear'(_C));
            #and evalb(degree(computed, z) >= degree(expected, z));
    elif member(z, inds) then
        testmajseries(computed, expected, z);
    else
        false
    end if;
end proc:

for data in [
    # order zero
    [y(z),          0,  0],
    [z^2*y(z),      0,  0],
    [(z^2+1)*y(z),  0,  0],
    # deqs corresponding to one-term recs
    [diff(y(z), z), _C, _C],
    [{diff(y(z), z, z), y(0)=1}, _C*(1+z), _C],
    [{diff(y(z), z, z), y(0)=1, D(y)(0)=42}, z+42, 43],
        # purerectodiffeq((n^2-9)*u(n+6), u(n), y(z));
    [27*y(z)-11*diff(y(z),z)*z+diff(diff(y(z),z),z)*z^2, _C, _C],
    NULL ]
do
    deq, expected_bound, expected_sum_bound := op(data):
    ReportTestFailures([
        Try[testgeqwithindets]("8.1",
            bound_diffeq(deq, y(z)),
            expected_bound),
        Try[testgeqwithindets]("8.2",
            eval(bound_diffeq_tail(deq, y(z), 0), z=1),
            expected_sum_bound),
        Try("8.3", bound_diffeq_tail(deq, y(z), 20), 0),
    NULL]);
end do:

deq := -2*w*diff(g(w),w)+(-w^2-1)*diff(diff(g(w),w),w):
rec := d(i+2)=i^2*d(i):

ReportTestFailures([

Try("9",
has(bound_diffeq_tail(
        27*y(z)-11*diff(y(z),z)*z+diff(diff(y(z),z),z)*z^2, y(z), 6), _C),
true),

# catch hardcoded variable names

Try("10", indets(bound_ratpoly(1/(u^2+3*u+1)^2, u)), {u}),

Try("11", indets(bound_diffeq(deq, g(w))), {_C, w}),

Try("12", indets(bound_diffeq_tail(deq, g(w), k), 'name'), {_C, w, k}),

Try("13", indets(bound_rec(rec, d(i)), 'name'), {_C, i}),

Try("14", bound_rec_tail(rec, d(i), j), infinity),

# used to yield unreadable bounds with old versions of NumGfun
# (thanks to Sylvain Chevillard for this example and the next one)

Try("15",
type(
    bound_rec(
        {4/15*(n+1)^2*v(n)+(16/15*(n+1)^2+22/15*n+28/15)*v(n+1)
        +(n+2)*(n+3)*v(n+2),
        v(0) = -9743/40969, v(1) = 23489/129848},
    v(n))
    /((n+1)*(n+2)*(2/3)^n),
    'rational'),
    true),

# a surprising but correct behaviour of bound_rec on a related sequence

Try("16",
bound_rec(
{ (4/15)*n^2*u(n)+((16/15)*n^2+(22/15)*n+2/5)*u(n+1)+(n+1)*(n+2)*u(n+2),
u(1) = -9743/40969, u(2) = 23489/129848},
u(n)),
_C*(n+1)*(2/5)^n),

NULL]);
