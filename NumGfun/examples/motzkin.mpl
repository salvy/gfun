with(gfun);
rec := { (n+3) * M(n+2) = 3*n*M(n) + (2*n+3)*M(n+1), M(0)=0, M(1)=1, M(2)=1 };
m := rectoproc(rec, M(n));
[seq(m(n),n=0..30)];
r := m(1000000):
save r, "m1000000.txt";
length(r);
