with(gfun:-NumGfun);
kernelopts(opaquemodules=false);


kernelopts(printbytes=false);
deq := { (z+1)*(3*z^2-z+2)*diff(y(z),z$3) + (5*z^3+4*z^2+2*z+4)*diff(y(z),z$2)
    + (z+1)*diff(y(z),z) + (4*z^3+2*z^2+5)*y(z), y(0)=0, D(y)(0)=I, D(D(y))(0)=0 };
sing := [fsolve((z+1)*(3*z^2-z+2), z, 'complex')];
f := diffeqtoproc(deq, y(z));
f(1/2,100);
f([0,-2/5+3/5*I, -2/5+I, -1/5+7/5*I],10);
f([0,-2/5+3/5*I],200);
#f([0,-2/5+3/5*I, -2/5+I, -1/5+7/5*I],400);
transition_matrix(deq, y(z), [0,-2/5+3/5*I, -2/5+4/5*I, -2/5+I, -1/5+6/5*I], 20);

