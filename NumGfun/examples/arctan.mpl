with(gfun);
with(NumGfun);
a := 3/5*(1+I);
deq := diffeqtohomdiffeq(holexprtodiffeq(arctan(z),y(z)),y(z));
evaldiffeq(deq,y(z),a,1000);
