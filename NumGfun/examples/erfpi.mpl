with(gfun);
with(NumGfun);
prec := 10000;
deq := diffeqtohomdiffeq(holexprtodiffeq(erf(z),y(z)),y(z)):
mypi := convert(evalf[prec](Pi),rational,exact):
time(evaldiffeq(deq, y(z), [0, mypi], prec));
time(evaldiffeq(deq, y(z), [0, mypi], prec/10, 'usebitburst'=false));
time(evalf[prec](erf(mypi)));
