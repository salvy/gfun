NumGfun: Maple package for numerical and "analytic" computations with
D-finite (aka holonomic) functions.

Copyright 2008, 2009, 2010, 2011, 2012 Inria.
Copyright 2013, 2014 CNRS.
Written by Marc Mezzarobba <marc@mezzarobba.net>.

LICENSE, INSTALLATION
---------------------

NumGfun is normally distributed as a subpackage of gfun, itself part of
the Algolib distribution available from
  http://algo.inria.fr/libraries/
under the GNU Lesser General Public License (version 2.1).

More frequent binary-only releases of gfun are available from
  http://algo.inria.fr/libraries/papers/gfun.html

Please refer to the documentation of Algolib (resp. gfun) for
installation instructions.

SOURCE RELEASES OF NUMGFUN
--------------------------

Separate source releases of some versions of NumGfun are prepared for
the sole purpose of making the source code easily available.  Please
note that using them to install NumGfun is not recommended.  (The
frequent binary releases of gfun mentioned above, which usually contain
the last stable version of NumGfun, should be prefered.)  To do it
nontheless, download the most recent source code release of gfun and
replace the NumGfun/ subdirectory by the contents of the archive.

RELEASE HISTORY
---------------

* 1.0 [2014-09]
  Support for evaluation “at” regular singular points with rigorous error
  estimates. Some support for algebraic points. Some cleanup and bug fixes.
  Tested with Maple 17. Should mostly work with Maple 13 and later.

* 0.6 [2012-06]
  Roughly corresponds to the state of NumGfun described in the version
  dated 2012-01-24 of the author's thesis "Autour de l'évaluation
  numérique des fonctions D-finies". Tested with Maple 13 and Maple 15,
  should also work with Maple 14 and Maple 16.

* 0.4--0.5 [2009--2010]
  Several versions released only as part of gfun/Algolib.
  NumGfun 0.5 is the version included in Algolib 14.0 [2010-12]/ gfun
  3.54 (although the version string reads '0.5devel').

* 0.1--0.3
  Early versions released separately before the inclusion of NumGfun
  into gfun.
