# Dominant roots of polynomials
# Author: Marc Mezzarobba

# In [MS2010], we call dominant roots of a polynomial those of maximal
# multiplicity among the nonzero roots of smallest modulus.  However, this code
# disregards this nonzero condition and leaves it to its caller to filter out
# roots at the origin if needed.

# Originally adapted from gfun:-infsolvepoly.

# TODO: optionally return the absolute value of the dominant root instead of
# its exact value?

dominant_root := module()

description "compute a root of maximal multiplicity among those of minimal"
    "modulus of a univariate polynomial with algebraic (RootOf) coefficients";

export doit, ModuleApply;

export longgcd, infroot_resultant, irreducible_solve, same_abs, closest_to_zero,
    refine_factors, as_primitive_RootOf_or_gaussian_rational;

# gcd of more than two polynomials
longgcd := proc(polys, $)
    #option inline;
    foldl(gcd, op(polys));
end proc;

# Compute a polynomial whose roots include the squared absolute values of those
# of Poly \in ℚ[z], "with multiplicities".
#
# (Note to self: if u and v are roots of poly both close to 1, then u², v² and
# u·v will all be roots of poly2, and all close to 1.)
infroot_resultant := proc(Poly, z, $)
    option cache;
    local poly, conjpoly, y, j;
    poly := expand(Poly);
    conjpoly := add(conjugate(coeff(poly, z, j))*z^j, j=0..degree(poly, z));
    resultant(
        subs(z=y, poly),
        numer(subs(z=z/y, conjpoly)),
        y);
end proc:

# alg must be a rational expression in I and RootOfs (radicals etc. are not
# allowed)
as_primitive_RootOf_or_gaussian_rational := proc(alg, $)
    local primfield, primelt, newalg, pol, modulus, deg, d, pows, i, mat, nsp,
        coef, newpol;
    if type(alg, 'complex'('rational')) then return alg end if;
    # First express alg as a polynomial with rational coefficients in some
    # primitive RootOf. In many cases, Primfield will chose alg itself as a
    # primitive element, so pol will just be _Z, but not if alg is something
    # like RootOf(...)+1 or even RootOf(_Z-RootOf(...)-1).
    newalg := convert(alg, 'RootOf'); # mainly because of I
    primfield := evala(Primfield(newalg));
    if primfield[1] = [] then return newalg end if; # alg is rational
    primelt := lhs(op(primfield[1]));
    # needed when alg contains both I and its RootOf representation
    newalg := subs(primfield[2], newalg);
    newalg := subs(RootOf(_Z^2+1,index=1)=I, newalg);
    if type(newalg, 'complex'('rational')) then return newalg end if;
    pol := subs(primelt=_Z, newalg);
    ASSERT(type(pol, 'polynom'('rational',_Z)));
    # Now find a linear relation between the powers of alg, and deduce a monic
    # polynomial of minimal degree annihilating alg.
    modulus := op(1, primelt);
    deg := degree(modulus, _Z);
    ASSERT(deg > 0);
    pows := [seq(rem(pol^i, modulus, _Z), i=0..deg)];
    mat := Matrix(deg, deg+1, (i,j) -> coeff(pows[j], _Z, i-1));
    # I am not sure if it can actually happen that degree(alg) < deg
    nsp := {};
    for d from 0 to deg while nsp = {} do
        nsp := LinearAlgebra:-NullSpace(mat[..,1..1+d]);
    end do;
    coef := op(1, nsp);
    newpol := add(coef[i+1]/coef[d]*_Z^i, i=0..d-1);
    RootOf(newpol);
end proc:

# Input: fac = [pol, mult, ...] where pol is a monic irreducible polynomial with
#   rational or algebraic (RootOf) coefficients, and mult is just passed around
#   for convenience (additional elements are allowed on input but discarded)
#   (cf. the output format of evala(Factors()))
# Output: list of record(root, mult, interval enclosure, ...) corresponding to
#   the roots of pol
# Note:
#   We need irreducible polynomials (squarefree is not enough), because we rely
#   on evala/... stuff that does not work with reducible RootOfs.
irreducible_solve := proc(fac, z, $)
    local pol, mult, alg, primpol, roots_of_prim, candidates, ivpol,
        may_be_root, deg, i;
    fail_if_Digits_too_large(convert(procname, 'string'));
    pol, mult := fac[1], fac[2];
    # Get rid of nested RootOf's: compute a set of RootOf(...,index=...) with
    # rational coefficients containing all roots of pol.
    alg := as_primitive_RootOf_or_gaussian_rational(RootOf(pol, z));
    if type(alg, 'complex'('rational')) then
        return [Record(':-val'=alg, ':-pol'=pol, ':-primpol'=_Z-alg,
                       ':-mult'=mult, ':-iv'=evalrC(alg))];
    end if;
    ASSERT(type(alg, 'RootOf'));
    primpol := op(1, alg);
    roots_of_prim := [seq(RootOf(primpol, 'index'=i), i=1..degree(primpol))];
    # Filter out the roots of primpol that cannot be roots of pol.
    candidates := map(
        x -> Record(':-val'=x, ':-pol'=pol, ':-primpol'=primpol, ':-mult'=mult,
                    ':-iv'=evalrC(x)),
        roots_of_prim);
    ivpol := subsindets(pol, {RootOf, 'complex(float)'}, evalrC);
    may_be_root := cand -> iv_contains_zero(evalrC(subs(z=cand:-iv, ivpol)));
    candidates := select(may_be_root, candidates);
    # RootOf=0 to avoid pbs with degree() when z=_Z; ok since pol is monic
    deg := degree(eval(pol, RootOf=0));
    ASSERT(nops(candidates) >= deg);
    if nops(candidates) > deg then
        # Refine. (We could avoid redoing the symbolic part.)
        Digits := 2*Digits;
        return irreducible_solve(_passed);
    end if:
    candidates;
end proc:

# Input: list of records describing roots as in refine_factors
# Output: whether all elements of the list have the same absolute value
same_abs := proc(candidates, $)
    local cand, above_all_abs, above_all_sqrabs, below_all_abs,
        below_all_sqrabs, ann_init, ann_sqrabs, check;
    # Compute an interval containing the absolute values of all candidates.
    # Note to self: a non-rigorous enclosure or an enclosure of a single
    # candidate is not enough!
    above_all_abs := max(seq(cand:-above_abs, cand in candidates));
    above_all_sqrabs := convert(above_all_abs, 'rational', 'exact')^2;
    below_all_abs := min(seq(cand:-below_abs, cand in candidates));
    below_all_sqrabs := convert(below_all_abs, 'rational', 'exact')^2;
    # Compute a bunch of polynomials whose roots include the squares of the
    # absolute values of all remaining candidates.
    ann_init := { seq(cand:-primpol, cand in candidates) };
    ann_sqrabs := map(infroot_resultant, ann_init, _Z);
    # Check that each of these polynomials has only one real root whose
    # value is compatible with the enclosures of the candidates. Then check
    # that they have a common root that is compatible with the estimates.
    check := proc(pol, $)
        local ivs, compat;
        ivs := RootFinding:-Isolate(pol, _Z, 'output'='interval');
        compat := select(
            iv -> evalb(iv[1]<=above_all_sqrabs and iv[2]>=below_all_sqrabs),
            map(rhs, ivs));
        evalb(nops(compat) = 1);
    end proc:
    evalb(all(check, ann_sqrabs) and check(longgcd(ann_sqrabs)));
end proc:

# Returns the point closest to zero of a real or complex interval
closest_to_zero := proc(iv, $)
    local lx, ux, nonzero_x;
    if nops(iv) = 2 then
        lx, ux := op(iv);
        nonzero_x := evalb(sign(lx) = sign(ux) and lx <> 0.);
        if not nonzero_x then   0;
        elif lx > 0 then        lx;
        else                    ux;
        end if;
    elif nops(iv) = 4 then
        Complex(
            closest_to_zero([op(1, iv), op(3, iv)]),
            closest_to_zero([op(2, iv), op(4, iv)]));
    end if;
end proc:

# Input:  list of irreducible polynomials with multiplicities
# Output:
# · a root of maximal multiplicity among those of minimal modulus
#   (or, in numeric mode, an approximation of such a root, with coefficients in
#   Q[i], and closer to zero than any root),
# · its multiplicity,
# · a list of records giving all the roots of minimal modulus (irrespective of
#   multiplicity) and a bunch of information about them.
# (Note to self: if I remember correctly, the multiplicity is used in
# bound_ratpoly and friends but not in bound_normal_diffeq. This may indicate
# doubtful design decisions :->)
refine_factors := proc(facpoly, z, {banzai := false}, $)
    local candidates, cand, above_min_abs, dom, dom2;
    candidates := map(op @ irreducible_solve, facpoly, z);
    candidates := map(
        cand -> Record[cand](
            ':-below_abs'=below_abs(cand:-iv, 'test_zero'),
            ':-above_abs'=above_abs(cand:-iv)),
        candidates);
    # Find all the roots that may have minimal absolute value
    above_min_abs := min(seq(cand:-above_abs, cand in candidates));
    candidates := select(cand -> cand:-below_abs <= above_min_abs, candidates);
    userinfo(6, 'gfun', "nbmini" = nops(candidates));
    if numeric_mode then
        userinfo(6, 'gfun', "computing a lower bound only");
    elif banzai then
        userinfo(4, 'gfun', "skipping checks");
    elif nops(candidates) > 1 then
        # Check that all remaining candidates have the same absolute value.
        # Otherwise, refine.
        if not same_abs(candidates) then
            Digits := 2*Digits;
            fail_if_Digits_too_large("dominant_root");
            # TODO: It would be nice to refine only the candidates we need to,
            # and share the algebraic part of irreducible_solve, but the
            # current structure of the code does not make this convenient. In
            # general, the current version of the code (with support for
            # algebraic coefficients) is actually more naive than the old
            # version inspired by infsolvepoly (which only handled coefficients
            # in Q[i]).
            return refine_factors(_passed);
        end if;
    end if;
    # Now pick one candidate of maximum multiplicity.
    dom := mymin(candidates, (a, b) -> a:-mult > b:-mult);
    if numeric_mode then
        # Keep multiplicity, but use the value of the candidate whose enclosure
        # is closest to zero (and the point closest to zero of the corresponding
        # interval).
        dom2 := mymin(candidates, (a, b) -> a:-below_abs > b:-below_abs);
        dom:-val := convert(closest_to_zero(dom2:-iv), 'rational', 'exact');
    end if;
    [dom:-val, dom:-mult, candidates];
end proc:

# Programmer interface, for internal use. Note that (at the moment) this version
# only handles polynomials with coefficients in ℚ[RootOfs]--radicals and Is are
# not allowed.
doit := proc(poly, z, { return_internal_data := false }, $)
    local fac, res;
    if degree(collect(poly, z), z) = 0 then
        res := [infinity, 1, []];
    else
        fac := evala(Factors(poly, z)); # here because of recursive calls
        res := refine_factors(op(2, fac), z);
    end if;
    if return_internal_data then res else res[1..2] end if;
end proc:

# User interface.
ModuleApply := proc(Poly, z, {rootof:=false}, $)
    local poly, res;
    poly := convert(Poly, RootOf); # get rid of I and radicals
    res := simplify(doit(poly, z), 'RootOf');
    if rootof then return res end if;
    simplify(convert(res, 'radical'), 'radical')
end proc:

end module:
