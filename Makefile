SHELL=/bin/bash
ifeq (X$(MAPLEAPP), X)
export MAPLEAPP=maple
endif

# May be overriden when this Makefile is called recursively from that of
# algolib/ddmf/whatever
MLA = lib/gfun.mla
# There is a new format for help databases starting with Maple18, 
# but I did not find tools to produce the .help correctly from the .mw or .mws in Maple18.
# So I do it in Maple17 and then convert manually using HelpTools:-Database:-ConvertAll() in Maple18.
HDB = lib/gfun.help


# Delete the default suffixes, then set up our own
.SUFFIXES: 
.SUFFIXES: .mpl .date .tst .out .ok

.PHONY: all clean test gtest ngtest mtest testing algolib_compile \
    algolib_help algolib_test


all: $(MLA) $(HDB)

distrib: $(MLA) $(HDB)
	- rm -rf gfun/*
	- rm gfun.zip
	- mkdir gfun
	cp -p lib/gfun.help lib/gfun.mla gfun/
	zip -r gfun gfun/gfun.help gfun/gfun.mla

# Note that the following rule deletes lib/gfun.mla.  We may eventually
# split this rule to suit versions of the top-level Makefile of algolib
# that create a symlink gfun/lib/gfun.mla -> somewhere/algolib.mla.
clean: cleantest
	rm -rf lib/NumGfunUserManual lib/gfun.hdb lib/gfun.help
	rm -rf *~ */*~ */*/*~ tst/*.ok \
	    NumGfun/test/*.ok ContFrac/test/*.ok \
	    minimization/test/*.ok \
	    lib/* *.date */*.date

cleantest: 
	rm -rf tst/*.out NumGfun/test/*.out ContFrac/test/*.out minimization/test/*.out

### MLA

SOURCEDIRS = -I . -I NumGfun
SRC = gfun.mpl ratinterp.mi Storjohann.mi *.mm NumGfun/*.mm ContFrac/*.mm ported/*.mm

# %.date records the last time the contents of %.mpl were added to
# $(MLA). Note that we cannot write a generic rule %.date: %.mpl (unless
# we add a target like
# gfun.mpl: $(SRC)
# 	touch gfun.mpl
# which is probably not a good idea)
gfun.date: $(SRC)
	sed -e 's/^#savelib/savelib/' gfun.mpl | \
	  ${MAPLEAPP} -s -q -e2 ${SOURCEDIRS} -b $(MLA) -B && touch $@

$(MLA): $(SRC)
	mkdir -p lib
	rm -rf $(MLA) gfun.date
	echo "march('create',\"${MLA}\",500);" | ${MAPLEAPP} -s -q
	$(MAKE) gfun.date
## echo "IF CREATING gfun.mla FOR DISTRIBUTION, USE MAPLEAPP=maple17 FOR COMPATIBILITY FROM maple14 to maple2015."
## does not work anymore since maple2018 changed their ToInert(_Inert_FORFROM).
### doc

HELPSRC = help/makehelp.mpl help/*.mw NumGfun/help/*.mw ContFrac/help/*.mw 

# same mechanism as above
lib/.hdbdate: $(HELPSRC)
	$(MAPLEAPP) -c 'THE_HDB := \"'$(HDB)'\" \;' help/makehelp.mpl \
	    > /dev/null  &&  touch $@

$(HDB): $(HELPSRC)
	mkdir -p lib
	rm -rf $@ lib/.hdbdate
	$(MAKE) lib/.hdbdate

lib/NumGfunUserManual/NumGfunUserManual.fo:
	# really needs to be done by hand using the graphical interface :-(
	$(MAPLEAPP) -t NumGfun/tools/make-pdf-manual.mpl
	rm -f lib/NumGfunUserManual.pdf

lib/NumGfunUserManual.pdf: lib/NumGfunUserManual/NumGfunUserManual.fo
	sed -i -e 's/Copyright.*IloNR2XbAugIyapF//' lib/NumGfunUserManual/NumGfunUserManual.fo # should not be here, but see comment about maple -x above
	fop -fo $< -pdf $@ # slow

### Tests

GFUN_TESTS = $(wildcard tst/*.tst)
NUMGFUN_TESTS = $(wildcard NumGfun/test/*.tst)
CONTFRAC_TESTS = $(wildcard ContFrac/test/*.tst)
MIN_TESTS = $(wildcard minimization/test/*.tst)
TESTS = $(GFUN_TESTS) $(NUMGFUN_TESTS) $(CONTFRAC_TESTS) $(MIN_TESTS)

#%.out: %.tst gfun.date NumGfun/test/testutils.mm
%.out: %.tst NumGfun/test/testutils.mm $(MLA)
	touch failed_tests.txt && \
	${MAPLEAPP} -s -t -q -e2 -A2 -b "lib/" -B -I NumGfun/test < $< > $@
	@grep -q -e failed.running.the.following.code -e error $*.out && echo "failed: " $* >> failed_tests.txt || true

.out.ok:
	grep -q -e failed.running.the.following.code -e error $*.out || touch $*.ok

test: $(TESTS:.tst=.out)
	- cat failed_tests.txt
	- rm failed_tests.txt

gtest: $(GFUN_TESTS:.tst=.out) cftest mtest
	- cat failed_tests.txt
	- rm failed_tests.txt

ngtest: $(NUMGFUN_TESTS:.tst=.out)
	@find NumGfun/test/ -name '*.out' -not -empty | xargs tail -n +1

cftest: $(CONTFRAC_TESTS:.tst=.out)
	- cat failed_tests.txt
	- rm failed_tests.txt

mtest: $(MIN_TESTS:.tst=.out)
	- cat failed_tests.txt
	- rm failed_tests.txt

testing:
	$(MAKE) -j 4 test


### algolib API

algolib_compile: gfun.date

algolib_help: lib/.hdbdate

algolib_test: testing $(TESTS:.tst=.ok)
	for n in $(TESTS:.tst=.ok) ; \
	    do \
	    	[ -e $$n ] || echo gfun/$$n ; \
	    done >> failed_tests.txt
	cat failed_tests.txt

