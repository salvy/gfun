## Unfortunately, convert/horner is spending a lot of time in type/ratpoly
## even in the case when the expression is a polynomial in the variable we're
## converting to. The following, taken directly from convert/horner:-nestit,
## circumvents the problem.
myhorner:=proc(x,v)
local k, s, t;
    try t := series(x, v, infinity) catch: return x end try;
    if 2 < nargs then t := map(horner, t, args[3 .. nargs]) end if;
    if t = 0 then return 0 end if;
    s := 0;
    for k from nops(t) by -2 to 4 do s := (op(k - 1, t) + s)*v^(op(k, t) - op(k - 2, t)) end do;
    return (op(1, t) + s)*v^op(2, t)
end:
