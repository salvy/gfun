
#diffeqtorec
# Input:  eqn: differential equation (for example output of algeqtodiffeq)
#    y(z): its unknown function
#    u(n): the name of the sequence of Taylor coefficients at the origin
#    ini: (optional) boolean indicating whether initial conditions should be computed
#    contentrec: (optional) will be assigned the content that was removed from
#      the recurrence. This is useful when the diff. eqn. is only the homogenous part
#      of the actual one.
# Output: the linear recurrence satisfied by u(n)
#
   diffeqtorec:=proc (eqn,yofz,uofk, {ini::boolean:=FAIL,returncontent::boolean:=false,contentrec::name:=_Content})
   option `Copyright (c) 1992-2010 by Algorithms Project, INRIA France. All rights reserved.`;
   local iniconds, f, y, z, u, k, Y, Z, rec, inirec, cont;
      getname(uofk,u,k); # This also checks that >=3 args are passed
      if type(eqn,'set') and ini<>false then
          f:=formatdiffeq([eqn,yofz],'y','z','iniconds')
      else
          f:=formatdiffeq([eqn,yofz],'y','z');
          if ini=true or f[1]<>0 then iniconds:={} else iniconds:=NULL fi
#         if f[1]<>0 then error "inhomogenous equation" end if
      end if;
      # Avoid problems when u=y or u=z,...
      f:=subs([y=Y,z=Z],f);
      if iniconds<>NULL then iniconds:=subs(y=Y,iniconds) fi;
      rec,inirec,cont:=`diffeqtorec/doit`(f,Y,Z,u,k,iniconds);
      if returncontent then contentrec:=cont fi;
      if iniconds<>NULL then inirec:=`goodinitvalues/rec`(rec,u,k,inirec,true) end if;
      makerec(rec,u,k,inirec)
   end proc: # diffeqtorec
