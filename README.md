Gfun is a Maple package that provides tools for

. guessing a sequence or a series from its first terms;

. manipulating rigorously solutions of linear differential or recurrence equations, using the equation as a data-structure.

See its [web page](https://perso.ens-lyon.fr/bruno.salvy/software/the-gfun-package/).

