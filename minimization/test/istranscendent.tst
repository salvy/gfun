with(TestTools):

testfcn:=proc(f,z) local y;
  gfun:-istranscendental(gfun:-holexprtodiffeq(f,y(z)),y(z))[1] end:

Try(1,testfcn(exp(z),z),true);
Try(2,testfcn(ln(1-z),z),true);
Try(3,testfcn((1-z)*log((1-z))+1/(1-z),z),true);

# Apéry
Try(4,gfun:-istranscendental({(z - 5)*y(z) + (7*z^2 - 112*z + 1)*diff(y(z), z) + (6*z^3 - 153*z^2 + 3*z)*diff(y(z), z, z) + (z^4 - 34*z^3 + z^2)*diff(y(z), z, z, z), y(0) = 1, D(y)(0) = 5, (D@@2)(y)(0) = 146},y(z))[1],true);

# Tricky hgm
Try(5,testfcn(hypergeom([1/6,5/6],[7/6],z),z),FAIL[1]);

# Stanley
Try(6,gfun:-istranscendental({(32*z^2 + 24*z + 12)*y(z) + (256*z^3 + 216*z^2 - 12*z)*diff(y(z), z) + (304*z^4 + 264*z^3 - 34*z^2)*diff(y(z), z, z) + (96*z^5 + 84*z^4 - 12*z^3)*diff(y(z), z, z, z) + (8*z^6 + 7*z^5 - z^4)*diff(y(z), z, z, z, z), y(0) = 0, D(y)(0) = 1/3, (D@@2)(y)(0) = 2, (D@@3)(y)(0) = 108/5},y(z))[1],true);




