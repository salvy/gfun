istranscendental:=proc(diffeq::set,yofz::function(name),isminimal::boolean:=false)
local deq,dop,i,Dz::nothing,z,sing,indeq,order,expos,basis,listcoeffs,sings,diffexpos,j,oldorder;
	z:=op(yofz);
	deq:=gfun:-diffeqtohomdiffeq(diffeq,yofz);
	if not isminimal then 
		oldorder:=degree(DEtools['de2diffop'](op(select(has,deq,z)),yofz,[Dz,z]),Dz);
		deq:=gfun:-minimizediffeq(deq,yofz,'homogeneous'=true)
	fi;
	deq:=op(select(has,deq,z));
	dop:=DEtools['de2diffop'](deq,yofz,[Dz,z]);
	order:=degree(dop,Dz);
	if not isminimal and order<oldorder then
		userinfo(3,'istranscendental',
			sprintf("input diff eqn of order %d, minimal one of order %d",oldorder,order));
		userinfo(4,'istranscendental',"minimal diff op:",dop)
	fi;
	listcoeffs:=[seq(coeff(dop,Dz,i),i=0..order)];
	sings:=[op(map(RootOf,map2(op,1,factors(listcoeffs[-1])[2]),z)),infinity];
	# Try 0 first
	if member(0,sings,'i') then sings:=[0,op(subsop(i=NULL,sings))] fi;
	for sing in sings do
		indeq:=gfun:-indicialpolynomial(listcoeffs,z,sing);
		if degree(indeq,z)<>order then
			return true, sprintf("irregular singularity at %a",sing)
		fi;
		expos:=roots(indeq,z);
		if {op(map2(op,2,expos))}<>{1} then # multiplicities
			return true, sprintf("multiple root of the indicial equation ==> ln at %a",sing) fi;
		expos:=map2(op,1,expos);
		if nops(expos)<>order then 
			return true, sprintf("irrational exponent at %a",sing) fi;
		#  check for integer difference of exponents
		diffexpos:={seq(seq(expos[i]-expos[j],j=i+1..order),i=1..order)};
		if hastype(diffexpos,integer) then 
			# check for logarithms
			diffexpos:=max(map(abs,select(type,diffexpos,integer)));
			basis:=DEtools['formal_sol'](dop,[Dz,z],z=sing,:-order=diffexpos+1);
			if has(basis,ln) then
				return true, sprintf("logarithmic singularity at %a",sing) fi;
		fi;
	od;
	FAIL
end:

