## This file contains :

# minfact: given a linear differential equation
#	whose coefficients belong to Q(z),
#	and initial conditions in Q,
#	finds a minimal right factor of the corresponding
# 	operator having the solution with the given initial conditions.

# mininhom: input is the output of minfact
# 	Returns a minimal order inhomogeneous linear differential
#	equation satisfied by the solution.

################################################################
# Next steps
#	add an optional argument: fuchsian right factor
#	add an optional argument: E-function (irreg at infty)
#	extend to coefficients with parameters
################################################################


use
	diffop2de=DEtools['diffop2de'],
	de2diffop=DEtools['de2diffop'],
	gen_exp=DEtools['gen_exp'],
	#rectoproc=gfun:-rectoproc,
	#diffeqtorec=gfun:-diffeqtorec,
	#listtoseries=gfun:-listtoseries,
	#findequationgivenorder=gfun:-findequationgivenorder,
	#findequationgivenordermodp=gfun:-findequationgivenordermodp,
	# maximum distance for a result of integer optimization to be assumed integer
	IntegerTolerance = 1e-2

in

minimizediffeq:=module()
	option package;
	export isapparent;
	local ModuleApply, bound_deg_coeffs, bound_valuations, bound_apparent,
		cleangenexps,fixedallvalues, generalized_degree, minfact,
		mininhom, newton_polygon, guessdiffopmodp, genexpfromindpol,
		leadtermandexponentqadicexpansion, multdenomatsing, ratsols,
		isapparentwithindpol, isapparentwithexps,pb_bound_deg_coeffs, adjoint;

ModuleApply:=proc(diffeq::set,yofz::function(name),
		{homogeneous::boolean:=true,use_ILP::{0,1,2}:=1,ILPtimelimit::integer:=5}
)
local deq;
	deq:=minfact(diffeq,yofz,use_ILP,ILPtimelimit);
	if homogeneous then deq
	else mininhom(deq,yofz)
	fi
end:

# Minimization algorithm
# Input:
# 	diffeq: a set containing differential equation and initial conditions
#	yofz of the form y(z): the unknown function and its variable
#	use_ILP: 0 means do not try to solve an integer linear programming problem
#			 1 means try it after a candidate minimal factor has been found, 
#				with a time limit
#			 2 try it at all stages, with a time limit
#		could be implemented later
#			 3 like 1 with no time limit
#			 4 like 2 with no time limit
#	ILPtimelimit: time limit for Integer Linear Programming
# 			 when use_ILP=2 this is the value that is used
#			 when use_ILP=1, the code uses 10% of the time used in exact guessing
# Output: 
#   a right factor of the linear operator associated to diffeq,
#		cancelling the given solution, and of minimal order.
# It is assumed that the initial conditions are given up to 
# the largest integer root of the indicial polynomial at 0.
#
# A subtlety is that even if a factorization exists, there may not 
# be a right factor of order, say, the order of the equation minus 1.
minfact:=proc(diffeq,yofz,use_ILP,ILPtimelimit)
local y,z,ini,deq,dop,Dz::nothing,ordeq,rec,getcoeffsmodp,getcoeffs,useilp,
	u::nothing,n::nothing,sigma,N,Lmodp,L,approx,i,alpha::nothing,found,ordfact,
	bdc,p,A,base,boundnbterms,gcrd,T::nothing,NP,allsings,genexp,pt,
	sing,bounds_needed,degapprox,FFE,maxbound,genexps,checkmin,p2,ilptimelim,
	apparent,shifteddop,optimpb,m::nothing,binvars,V,timeguess,indpol,st:=time(),st2;
	y:=op(0,yofz); z:=op(yofz);
	deq,ini:=selectremove(has,diffeq,z); deq:=op(deq);
	dop:=de2diffop(deq,y(z),[Dz,z]);
	dop:=collect(numer(dop),Dz); # in case the deq has rational coeffs
	if not type(dop,polynom('rational',[Dz,z])) then 
		error "only rational coefficients handled in this version" fi;
	ordeq:=degree(dop,Dz);

	userinfo(3,'minimizediffeq',sprintf("entering with order %d and degree %d",ordeq,degree(dop,z)));

	# maximum integer root of the indicial equation at 0:
	sigma:=max(ordeq-1,op(map2(op,2,map(op,[isolve(indicialpolynomial(dop,[Dz,z],0))]))));
	# two procedures computing the coefficients of the
	# solution under study, one mod p and one over the rationals
	rec:=diffeqtorec(diffeq,y(z),u(n));
	p:=modp1(Prime(1)); # big prime number
	p2:=modp1(Prime(p)); # another one
	getcoeffsmodp:=rectoproc(rec,u(n),remember,'evalfun'=modp,'postargs'=[p]);
	getcoeffs:=rectoproc(rec,u(n),remember);

	N:=sigma+3; # sigma could be 0
	base:=3.; # should be > 1. 
	found:=dop;
	checkmin:=false;
	A:=OreTools['SetOreRing'](z,'differential');

	# local analysis:
	# In this version, we factor the leading polynomial.
	# A factorization-free variant is certainly possible.
	allsings:=[op(select(has,map2(op,1,factors(coeff(dop,Dz,ordeq))[2]),z)),infinity];
	apparent:=[];
	bounds_needed:=true;

	# iterate with more and more coefficients in the series until
	# a differential equation is found 
	# or the number of coefficients exceeds what the bounds
	# on the degrees of the coefficients require.
	ordfact:=ordeq-1; # order of the factor
	do
		if ordfact=0 then # the last factor found is minimal
			return {diffop2de(numer(found),[Dz,z],y(z))} union ini fi;
		if bounds_needed then # perform the local analysis at each singularity
			for sing in allsings do
				# check whether the singularity is apparent
				# and remove it from allsings if it is
				indpol:=indicialpolynomial(found,[Dz,z],`if`(sing=infinity,infinity,RootOf(sing,z)));
				if sing<>infinity and isapparentwithindpol(found,indpol,Dz,z,sing) then
					userinfo(3,'minimizediffeq',sprintf("apparent singularity: %.180a",sing));
					apparent:=[op(apparent),sing];
					allsings:=remove(type,allsings,identical(sing));
					next
				fi;
				if sing=infinity then pt:=infinity else pt:=RootOf(sing,z) fi;
				if degree(indpol,z)=ordeq then
					genexps:=genexpfromindpol(indpol,z,pt,T)
				else
					genexps:=gen_exp(found,[Dz,z],T,z=pt,`if`(type(pt,RootOf),'groundfield'=[pt],NULL));
					genexps:=cleangenexps(genexps,`if`(degree(sing,z)>1,{RootOf(sing,z)},NULL))
				fi;
				genexp[sing]:=genexps;
				shifteddop:=DEtools['translate']([seq(coeff(dop,Dz,i),i=0..degree(dop,Dz))],z,
						`if`(sing=infinity,infinity,RootOf(sing,z)));
				shifteddop:=add(shifteddop[i]*Dz^(i-1),i=1..nops(shifteddop));
				NP[sing]:=newton_polygon(shifteddop,Dz,z)
			od;
			optimpb,binvars,V:=pb_bound_deg_coeffs(allsings,genexp,m,T,z);
			bounds_needed:=false
		fi;

		# bound the degrees of the coefficients of a right factor of order ordfact
		if not assigned(bdc[ordfact]) then
			userinfo(1,'minimizediffeq',sprintf("looking for a factor of order %d",ordfact));
			if assigned(Lmodp) then
				maxbound:=floor(nops(Lmodp)/(ordfact+1))-1
			else maxbound:=-infinity fi;
			useilp:=use_ILP=2 or checkmin and use_ILP=1;
			if useilp then if use_ILP=2 then ilptimelim:=ILPtimelimit
			else ilptimelim:=ceil(timeguess/10)
			fi fi;
			bdc[ordfact]:=bound_deg_coeffs(allsings,NP,ordfact,z,T,subs(m=ordfact,optimpb),binvars,V,maxbound,useilp,ilptimelim);
			# the returned bound is FAIL when no such factor exists
			if bdc[ordfact]=FAIL then
				userinfo(2,'minimizediffeq',sprintf("proved no factor of order %d",ordfact));
				ordfact:=ordfact-1;
				next
			fi;
			if max(bdc[ordfact])>maxbound then
				userinfo(2,'minimizediffeq',sprintf("bound on the degrees of the coefficients at order %d: %a",ordfact,bdc[ordfact]))
			else
				userinfo(2,'minimizediffeq',sprintf("bound on the degrees of the coefficients at order %d not larger",ordfact));
				# would have been found earlier:
				ordfact:=ordfact-1;
				next
			fi;
			boundnbterms:=convert(bdc[ordfact],`+`)+nops(bdc[ordfact])+ordfact+4; # +ordfact because of diff
			# In cases when there is no factor, it is better to avoid guessing
			# on too large lists at the end.
			# This N is smaller than base*current N
			# and such that there exists k, N*base^k=boundnbterms
			if checkmin then N:=boundnbterms else
				N:=ceil(boundnbterms/base^floor(log(boundnbterms/N)/log(base)))
			fi;
			if N+1<=nops(Lmodp) then # would have been found earlier
				ordfact:=ordfact-1;
				next
			fi
		fi;
		st2:=time();
		Lmodp:=[seq(getcoeffsmodp(i),i=0..N)];
		userinfo(3,'minimizediffeq',sprintf("computed %d modular coefficients in %g sec.",N+1,time()-st2));
		if indets(Lmodp,name)<>{} then error "invalid initial conditions" fi;
		st2:=time();
		approx:=guessdiffopmodp(Lmodp,z,Dz,p,`if`(checkmin,ordfact,1),ordfact);
		userinfo(3,'minimizediffeq',sprintf("tried guessing mod p in %g sec.",time()-st2));

		if approx<>FAIL then
			# When an equation is found for the truncation mod p,
			# it has to be certified
			userinfo(3,'minimizediffeq',sprintf("found modular operator of order %d and degree %d",degree(approx,Dz),degree(approx,z)));
			# DEtools[GCRD] is very slow.
			# It is therefore better to try and guess the differential operator directly
			# since then the call to DEtools[GCRD] will only perform one Euclidean division
			# For that, we first compute the gcrd mod p and deduce bounds on degree & order
			# that give us a number of terms for Hermite-Pade.
			st2:=time();
			FFE:=OreTools['Modular']['FractionFreeRightEuclidean']('OrePoly'(seq(coeff(dop,Dz,i),i=0..degree(dop,Dz))),
				'OrePoly'(seq(coeff(approx,Dz,i),i=0..degree(approx,Dz))),p,A);
			# FFE[1] possibly wrong (bug in FractionFreeRightEuclidean in Maple2022)
			for i to FFE[1] while type(FFE[2][i],'OrePoly') do od;
			gcrd:=OreTools['Modular']['Primitive'](FFE[2][i-1],p,A);
			#gcrd:=OreTools['Modular']['GCRD'](OrePoly(seq(coeff(dop,Dz,i),i=0..degree(dop,Dz))),
			#	OrePoly(seq(coeff(approx,Dz,i),i=0..degree(approx,Dz))),p,A);
			gcrd:=expand(numer(normal(add(op(i,gcrd)*Dz^(i-1),i=1..nops(gcrd)))));
			if degree(gcrd,Dz)=0 then 
				userinfo(3,'minimizediffeq',"not a true factor, increasing number of terms");
				approx:=FAIL 
			else
				userinfo(3,'minimizediffeq',sprintf("modular gcrd of order %d and degree %d found in %g sec.",degree(gcrd,Dz),degree(gcrd,z),time()-st2));
			fi
		fi;

		if approx<>FAIL then
			# find an equation over the rationals
			# + order because of diff
			N:=((1+degree(gcrd,Dz))*(1+degree(gcrd,z))+degree(gcrd,Dz)+5); 
			st2:=time();
			L:=[seq(getcoeffs(i),i=0..N)];
			userinfo(3,'minimizediffeq',sprintf("computed %d rational coefficients in %g sec.",N+1,time()-st2));
			timeguess:=time();
		    approx:=findequationgivenorder(L,degree(gcrd,Dz),z,"differential",[0$(degree(gcrd,Dz)+1)],false);
		    timeguess:=time()-timeguess;
   			userinfo(3,'minimizediffeq',sprintf(
   				cat("guessing ",`if`(approx=FAIL,"failed","succeeded"),
   					" in %g sec."),timeguess))
		fi;

		if approx<>FAIL then
			degapprox:=nops(approx)-1;
			approx:=add(approx[i]*Dz^(i-1),i=1..nops(approx));
			userinfo(3,'minimizediffeq',sprintf("checking equation of order %d",degapprox));
			# Test exact division (modulo another prime)
			if OreTools['Modular']['RightPseudoRemainder']('OrePoly'(seq(coeff(dop,Dz,i),i=0..degree(dop,Dz))),
				'OrePoly'(seq(coeff(approx,Dz,i),i=0..degapprox)),p2,A)<>'OrePoly'(0) then
				approx:=FAIL # we could try a gcd
			else
				checkmin:=true; # from now on, try to prove it is minimal
				userinfo(4,'minimizediffeq',sprintf("it is a right factor"))
			fi;
		fi;

		if approx<>FAIL then
			userinfo(1,'minimizediffeq',sprintf("found a right factor of order %d in %g sec.",degapprox,time()-st));
			found:=approx;
			# It may not have the minimal order, which will be checked recursively
			ordfact:=degapprox-1;
			bounds_needed:=true;
			next
		fi;
		
		if N>=boundnbterms then # with that many coefficients, a factor would have been found
			ordfact:=ordfact-1
		else # increase number of terms
			N:=min(ceil(base*N),boundnbterms)
		fi
	od;
	error "should not happen"
end:

# Input: 
# 	deq: a set containing a minimial homogeneous differential equation and initial conditions
#	yofz of the form y(z): the unknown function and its variable
# Output: 
#   a minimal inhomogeneous linear differential equation satisfied
#	by the solution.
mininhom:=proc(deq::set,yofz::function(name))
local y:=op(0,yofz),z:=op(yofz),Dz::nothing,dop,ord,Q,deq2,rhs,
	u::nothing,n::nothing,sigma,alpha::nothing,S,j,integratingfactor,ordsing,adj,i;
	dop:=de2diffop(op(select(has,deq,z)),yofz,[Dz,z]);
	adj:=adjoint(dop,Dz,z);
	integratingfactor:=ratsols(adj,Dz,z);
	if integratingfactor=[] then 
		userinfo(2,'minimizediffeq',"no rational integrating factor");
		deq
	else
		userinfo(2,'minimizediffeq',"found a rational integrating factor");
		# See Adamczewski-Rivoal 2018 p. 703
		ord:=degree(dop,Dz)-1;
		integratingfactor:=op(integratingfactor); # unique if deq is minimal
		Q[ord]:=integratingfactor*coeff(dop,Dz,ord+1);
		for j from ord-1 by -1 to 0 do # coefficients of the "bilinear concomitant" [Poole 1960, p.38]
			Q[j]:=normal(integratingfactor*coeff(dop,Dz,j+1)-diff(Q[j+1],z))
		od;
		deq2:=diffop2de(add(Q[j]*Dz^j,j=0..ord),[Dz,z],y(z));
		# minimum integer root of the indicial equation at 0:
		ordsing:=select(type,[isolve(indicialpolynomial([seq(coeff(dop,Dz,i),i=0..ord+1)],z,0))],integer);
		sigma:=min(0,op(map2(op,2,map(op,ordsing))));
		S:=listtoseries(rectoproc(diffeqtorec(deq,y(z),u(n)),u(n),list)(-sigma+ord+1),z);
		rhs:=coeff(series(eval(deq2,y(z)=S),z,-sigma+ord+1),z,0);
		userinfo(5,'minimizediffeq',sprintf("minimal inhomogeneous diffeq: %a",deq2-rhs));
		{deq2-rhs} union remove(has,deq,z)
	fi
end:


# Input:
#	allsings:	singularities (including infinity)
#	NP:			Newton polygons at the singularities
#	ordfact:	order of the right factor
#	z:			variable in the operator
#	T:			variable in the generalized exponents
#	lowerbound: no need to find a bound smaller than that
#	use_ILP:	boolean
#	ilptimelim: integer
# Output:
#	an upper bound on the degree of the coefficients 
#		of any right factor M of order ordfact
#	or FAIL if no such factor can exist.
bound_deg_coeffs:=proc(allsings,NP,ordfact,z,T,constraint,binvars,V,lowerbound,use_ILP,ilptimelim)
local sing,apparent,bounddegnum,bounddegden,vals,i,bound;

	# I. Valuations at the singularities
	vals:=bound_valuations(allsings,NP,ordfact,z);
	if vals=FAIL then return FAIL fi; # FAIL means no possible factorization at order ordfact
	userinfo(5,'minimizediffeq',sprintf("bounds on the valuations of the coefficients at the singularities %a",[seq(["sing",i,"vals",vals[i]],i=allsings)])); 
	# At this stage, for each coefficient of a monic factor,
	# we have a lower bound on its valuation at each singularity
	# and therefore an upper bound on the degree of its denominator.
	#
	# Bound the degree of the denominators
	# of each coefficient of a monic right factor
	# apart from the terms coming from the apparent singularities. 
	# This means that from our valuations with a last coefficient
	# equal to 1, we have to reverse to the leading coefficient = 1.
	## i --> coeff of Dz^(ordfact-i)
	for i to ordfact do
		bounddegnum[i]:=vals[infinity][i+1];
		bounddegden[i]:=add(max(0,vals[sing][i+1]),sing=subs(infinity=NULL,allsings))
	od;

	# II. Bound the degree of the factor induced by apparent singularities
	# Bound on the number of apparent singularities that would
	# be needed to require computing more terms of the series
	# expansion.
	bound:=ceil((lowerbound-bounddegden[ordfact])/ordfact);
	apparent:=bound_apparent(constraint,binvars,V,bound,use_ILP,ilptimelim);
	if apparent=FAIL then if lowerbound=-infinity then return FAIL else return lowerbound fi fi;
	userinfo(3,'minimizediffeq',sprintf("# apparent singularities <= %d",apparent));
	# Apparent singularities are regular
	for i to ordfact do	bounddegden[i]:=bounddegden[i]+i*apparent od;

	# III. Bound the degree of the numerators/denominators
	# using the valuation at infinity.
	## i --> coeff of Dz^(ordfact-i)
	[bounddegden[ordfact],seq(max(0,bounddegden[ordfact]-bounddegnum[i]),i=1..ordfact)]
end:

# Input: same as bound_deg_coeffs
#
# Output:
#	allsings:	singularities (including infinity)
#	NPS:		Newton polygons at the singularities
#	ordfact:	order of the right factor
#	z:			variable in the operator
#
# vals[sing]=[0,a1,a2,a3,...] for sing<>infinity means that the valuation 
# of the coefficient of Dz^i at sing is at least ai (possibly <0)
# When sing=infinity, it means that the degree of 
# the coefficient of Dz^i minus that of Dz^0 is at most ai.
bound_valuations:=proc(allsings,NPS,ordfact,z)
local c::nothing,sing,opt,ind,val,i,co,slope,num,den,j,deg,vals,NP;
	#	For each singularity alpha, the valuations at alpha of 
	#	the coefficients of a factorization in monic form are bounded using
	#	the additivity of Newton polygons.
	for sing in allsings do
		NP:=NPS[sing];
		# NP is a list of vectors giving the slopes, with integer coordinates,
		# sorted by increasing slope.
		#
		# The valuations of the coefficients of the factor are bounded
		# by the solution of a linear optimization problem
		# with the constraints that
		#	. only slopes in NP are used
		#	. a rational slope in NP can only be used for a width
		#		equal to a multiple of its denominator
		#	. a slope is used at most for its width in NP
		try
			opt:=Optimization['LPSolve'](
				# c[i] is the (integer) number of times the slope NP[i] is used
				# in the factor.
				# Maximize the contributions
				add(c[i]*numer(NP[i][1]/NP[i][2]),i=1..nops(NP)),
				# use integer multiples of the rational slopes
				{seq(c[i]<=NP[i][2],i=1..nops(NP)),
				# the total width is the target order
					add(denom(NP[i][1]/NP[i][2])*c[i],i=1..nops(NP))=ordfact},
				assume={nonnegative,integer},'maximize');
		catch "no feasible":
			return FAIL # there is no factor of this order
		end try;
		# translate the slopes of the Newton polygon 
		# into valuation bounds for coefficients in Dz
		ind:=0;val[0]:=0;
		for i to nops(NP) do
			co:=subs(opt[2],c[i]);
			if co>0 then
				slope:=NP[i][1]/NP[i][2];
				num:=numer(slope);
				den:=denom(slope);
				for j to den*co do
					if sing<>infinity then
						val[ind+j]:=val[ind]+ind+j+iquo(j,den)*num
					else
						val[ind+j]:=val[ind]+ind+j-iquo(j,den)*num
					fi
				od;
				ind:=ind+den*co
			fi
		od;
		if sing=infinity then deg:=1 else deg:=degree(sing,z) fi;
		vals[sing]:=[seq(deg*val[i],i=0..ordfact)]
	od;
	vals
end:

# Input: 
#	allsings:	singularities (including infinity)
#	genexp:		generalized exponents at the singularities
#	m:			variable standing for the order of the right factor
#	T:			variable in the generalized exponents
#	z:			variable in the singularities
# Output:
#	a linear programming problem parameterized by the order for
#	the number of apparent singularities
#
#	genexp is an equivalence class of generalized exponents returned by DEtools[gen_exp]
#	of the form [pol_1(1/T), pol_2(1/T),..., T^k=()], k giving the ramification
#	Each of the generalized exponents contributes the exponent
#		e_i=coeff(pol_i,T,0), k times
#	RootOfs may occur in pol_i(T), in which case the sum of the exponents
#	    should sum over the values of the RootOf
#		and the degree of the extension is tracked in the multiplicity
#   This follows van Hoeij's approach, exploiting the bound
#   by Bertrand & Beukers in the irregular case.
#   Bertrand (1999) has omega_i pol in 1/z, not 1/T.
#
#	If the exponential parts at pt are [P_1,...,P_k], each with 
#	n_i exponents e_{ij}, this returns the list of possible
#			sum(e_{ij}) - ordfact(ordfact-1)/2 + sum_{p,q} m_pm_q\deg(P_p-P_q),
#   where m_ell is the number of e_{ell,j} in the first sum.
#	This is also 
#			sum(e_{ij}) - ordfact(ordfact-1)/2 + sum_{e,f} \deg(P_e-P_f),
#	if one associates one P_e to each exponent e_{ij}.
#	See pp. 60--62 of 
#		Bertrand (1999), On Andre’s proof of the Siegel-Shidlovsky theorem,
#   	Publications de l’Universite Keio 27 (1999), 51–63. 
#
pb_bound_deg_coeffs:=proc(allsings,genexp,m,T,z)
local sing,i,j,u,constraint,V::nothing,exps,deg,x::nothing,y::nothing,ram,exp,pols,S,irr,i1,j1,i2,j2,rof,bound,vars,indsrofs,pol,sys,cofs,sol,indsy,constry,indy,new,v,w,inds2,inds,thisrof,eqsol;
	indy:=proc(pt,IJ1,IJ2,eqsol) # unique label for each edge of the graph
	local ij1:=subs(eqsol,IJ1), ij2:=subs(eqsol,IJ2);
		if ij1[1]<ij2[1] then pt,op(ij1),op(ij2)
		elif ij2[1]<ij2[1] then pt,op(ij2),op(ij1)
		elif ij1[2]<=ij2[2] then pt,op(ij1),op(ij2)
		else pt,op(ij2),op(ij1) fi end;
	# constraints: use m exponents at each singularity
	# and conjugate algebraic exponents should either not appear, or all appear
	for sing in allsings do
		exps:=genexp[sing];
		ram:=[seq(degree(op([-1,1],exp),T),exp=exps)]; # ramification
		pols:=[seq(subs(T=1/T^(1/ram[i]),subsop(-1=NULL,exps[i])),i=1..nops(exps))];
		# At this stage we have a list of the generalized exponents
		# (see ?DEtools[gen_exp]).
		# We have to select ordfact exponents at each singularity.
		# use one 0/1 variable x for each exponent
		S[sing]:=subs(T=1/T,add(add(ram[i]*x[sing,i,j]*(exps[i][j]),j=1..nops(exps[i])-1)-m*(m-1)/2,i=1..nops(exps)));
		constraint[sing]:=seq(add(ram[i]*x[sing,i,j],j=1..nops(exps[i])-1)=m,i=1..nops(exps));
		# then group the variables that must be equal
		indsrofs:=indets(S[sing],'specified_rootof');
		sol:=NULL;
		sys:={};
		for pol in map2(op,1,indsrofs) do
			thisrof:=select(proc(u) op(1,u)=pol end,indsrofs);
			deg:=degree(pol,_Z);
			for i to deg do
				cofs:=map2(coeff,S[sing],thisrof,i);
				sys:=sys union {seq(op(j,cofs)-op(1,cofs),j=2..nops(cofs))}
			end do
		od;
		if has(sys,T) then sys:=map(coeffs,map(collect,sys,T),T) fi;
		S[sing]:=subs(T=0,S[sing]);
		sol:=solve(sys);
		constraint[sing]:=op(subs(sol,[constraint[sing]]));			
		S[sing]:=subs(sol,S[sing]);
		if has(S[sing],RootOf) then	S[sing]:=evala(S[sing])	fi;
		# Irregular singular point:
		# For this optimization problem,
		# the idea is to add one variable per pair (e_i,e_j), i<j
		# and the sum of these variables for fixed i should be ordfact-1.
		# Idea taken from Deghan, Assari, Shah 2015.
		eqsol:=select(type,sol,`=`(name,name));
		eqsol:=[seq([op([1,2..3],v)]=[op([2,2..3],v)],v=eqsol)];
		irr[sing]:=2*add(add(add(add(
				ram[i1]*ram[i2]*y[indy(sing,[i1,j1],[i2,j2],eqsol)]*max(generalized_degree(pols[i2][j2],T),
					generalized_degree(pols[i1][j1],T)),
				j2=1..nops(pols[i2])),
				i2=i1+1..nops(pols)),
				j1=1..nops(pols[i1])),
				i1=1..nops(pols))
			+2*add(add(add(ram[i1]^2*y[indy(sing,[i1,j1],[i1,j2],eqsol)]*generalized_degree(pols[i1][j2]-pols[i1][j1],T),
				j2=j1+1..nops(pols[i1])),
				j1=1..nops(pols[i1])),
				i1=1..nops(pols));
		if degree(sing,z)>1 then
			rof:=RootOf(sing,z);
			if has(S[sing],rof) then S[sing]:=sum(subs(rof=V,S[sing]),V=rof) fi;
			if has(irr[sing],rof) then irr[sing]:=sum(subs(rof=V,irr[sing]),V=rof) fi;
			S[sing]:=degree(sing,z)*S[sing] # all choices occur at all roots of sing
		fi;
		if has(pols,T) then # irregular singular point
			constry:=seq(seq(add(add(ram[i1]*ram[i2]*y[indy(sing,[i1,j1],[i2,j2],eqsol)], 
						j2=1..nops(pols[i2])),
						i2=i1+1..nops(pols))
					+add(add(ram[i1]*ram[i2]*y[indy(sing,[i2,j2],[i1,j1],eqsol)], 
						j2=1..nops(pols[i2])),
						i2=1..i1-1)
					+add(ram[i1]^2*y[indy(sing,[i1,j1],[i1,j2],eqsol)],
						j2=j1+1..nops(pols[i1]))
					+add(ram[i1]^2*y[indy(sing,[i1,j2],[i1,j1],eqsol)],
						j2=1..j1-1)
				=subs(sol,x[sing,i1,j1])*(m-1),
					j1=1..nops(pols[i1])),
					i1=1..nops(pols));
			constraint[sing]:=constraint[sing],constry
		fi
	od;
	# the sum to be minimized with binary choices of x[sing,k,i,j] and y[sing,k,i,j]
	bound:=add(S[sing]-1/2*irr[sing],sing=allsings)+m*(m-1);
	# the constraints
	constraint:={V=bound,seq(constraint[sing],sing=allsings)};
	vars:=[op(indets(constraint,indexed))];
	constraint,vars,V
end:

# Input: 
#	constraint:	linear programming problem constructed by pb_bound_deg_coeffs
#	binvars:	names of the binary variables in that problem
#	V:			variable to be optimized
#	lowerbound: finding a bound lower than that is unnecessary
#	use_ILP:	control use of integer linear programming
#	ilptimelim: upper bound on the time for integer linear programming if use_ILP=true
# Output:
#	optimal value, or an upper bound on it.
#
# Do not try to solve the ILP, except by trying
# a small amount of time at the end, controled by maximaldepth and ILPtimelimit
# The problem is that timelimit cannot stop the ILP 
# which is external and sometimes this is where all the time will
# be spent before we can decide to use the result of the relaxed
# LP problem.
bound_apparent:=proc(constraint,binvars,V,lowerbound,use_ILP,ilptimelim)
local bdV,opt,i,bad,nbit,st,opt2,dlim;
	bdV:=V=-infinity..min(0,-lowerbound);
	# V has to be minimized
	userinfo(4,'minimizediffeq',"solving LP problem");
	userinfo(5,'minimizediffeq',sprintf("minimizing %a, which should be an integer, with binary x & y under the constraints %a,%a.",
		V,constraint,bdV));
	# Checks whether relaxed problem is feasible first
	try
		nbit:=max(50,5*nops(binvars)+5*nops(constraint)); # copied from Optimization:-LP:-SolveInteger
		do
			st:=time();
			# seems better to test bdV afterwards
			opt:=Optimization['LPSolve'](V,constraint,# bdV,
				seq(i=0..1,i=binvars),'iterationlimit'=nbit);
			if opt[1]>op([2,2],bdV) then error "no feasible" fi;
			userinfo(4,'minimizediffeq',sprintf("Relaxed LP completed in %g sec.",time()-st));
			if abs(opt[1]-round(opt[1]))<IntegerTolerance then
				opt:=subs(opt[1]=round(opt[1]),opt)
			else opt:=subs(opt[1]=ceil(opt[1]),opt) fi;
			userinfo(5,'minimizediffeq',sprintf("solution: %a",opt));
			# Check result because LPSolve sometimes stops with an 
			# uncatchable "Warning, limiting number of iterations reached"
			bad:=remove(proc(u) type(op(1,u),indexed) and member(round(op(2,u)),{0,1}) end,op(2,opt));
			if nops(bad)=1 and
				map(proc(u) round(op(1,u)-op(2,u)) end,subs(opt[2],remove(has,constraint,V)))={0} then break fi;
			userinfo(4,'minimizediffeq',"pbm with this solution, increasing precision");
			nbit:=2*nbit; Digits:=2*Digits
		od
	catch "no feasible":
		userinfo(4,'minimizediffeq',sprintf("Relaxed LP completed in %g sec.",time()-st));	
		userinfo(4,'minimizediffeq',"no feasible solution");
		return FAIL
	end try;
	if not use_ILP or opt[1]>0 then return -opt[1] fi; # this is the default
	try
		nbit:=max(50,5*nops(binvars)+5*nops(constraint)); # copied from Optimization:-LP:-SolveInteger
		st:=time();
		for dlim from 5 do
			try
				opt2:=Optimization['LPSolve'](V,constraint,bdV,
						'binaryvariables'=binvars,'assume'=integer,'iterationlimit'=nbit,
						'depthlimit'=dlim, 
						'initialpoint'=opt[2]);
				userinfo(4,'minimizediffeq',sprintf("ILP improved bound from %d to %d in %g sec.",
					-floor(opt[1]),-floor(opt2[1]),time()-st));
				userinfo(5,'minimizediffeq',sprintf("solution: %a",opt2));
				return -floor(opt2[1])
			catch "maximal depth": 
				if time()-st>ilptimelim then error "time expired" fi;
			end try
		od
	catch "no feasible": 
		userinfo(4,'minimizediffeq',sprintf("ILP completed in %g sec.",time()-st));	
		userinfo(4,'minimizediffeq',"no feasible integer solution");
		return FAIL
	catch:
		userinfo(4,'minimizediffeq',sprintf("ILP failed in %g sec., using upper approx",time()-st));	
		-floor(opt[1])
	end try;
end:


##############################	UTILITIES #############################

# for a polynomial with non-integer exponents, return the maximal exponent
generalized_degree:=proc(pol,v)
	if type(pol,name) then if pol=v then 1 else 0 fi
	elif type(pol,{numeric,RootOf}) then 0
	elif type(pol,`+`) then max(map(procname,[op(pol)],v))
	elif type(pol,`*`) then convert(map(procname,[op(pol)],v),`+`)
	elif type(pol,`^`) then op(2,pol)*procname(op(1,pol),v)
	else error "unexpected input",pol
	fi
end:

# Input:
#	dop:	linear differential operator as a polynomial in Dz & z
#	Dz,z: variables
# Output:
#	list of vectors [x,y] with integer coordinates,
#	where x is the ordinates (degree in z), 
#	y is the difference in the abscissas (degree in Dz),
#	sorted by increasing order of slopes
#	If a_i is the coefficient of Dz^i, this is the convex hull
#	of the set (i,val(a_i)-i)+(\R_{-}\times\R_+).
newton_polygon:=proc(dop,Dz,z)
local deg,ldeg,clean,pts,slopes:=[],ini,lastpt,slope,j,c,sl,pt;
	clean:=collect(dop,[Dz,z]); # so that coeff works
	deg:=degree(clean,Dz);
	pts:=[seq([ldegree(coeff(clean,Dz,j),z)-j,j],j=0..deg)];
	ldeg:=min(seq(pt[1],pt=pts));
	# this is the effect of adding the quadrant to the
	# lowest point
	if ldeg<pts[1][1] then pts:=subsop(1=[ldeg,0],pts) fi;
	# first point in the convex hull:
	ini:=pts[1];
	while ini[2]<deg do
		lastpt:=ini;
		slope:=infinity;
		for j from lastpt[2]+1 to deg do
			c:=pts[j+1][1];
			sl:=(c-ini[1])/(j-ini[2]);
			if sl<slope then slope:=sl fi;
			# keeping intermediate points could also prove useful
			if sl<=slope then lastpt:=[c,j] fi 
		end do;
		slopes:=[op(slopes),lastpt-ini];
		ini:=lastpt
	od;
	slopes
end:

# This should be more accessible from gfun
guessdiffopmodp:=proc(L,z,dz,p,minord,maxord)
local lord,ord,res;
	lord:=[maxord];
	while lord[1]/2>minord do lord:=[iquo(lord[1],2),op(lord)] od;
	for ord in lord do 
		userinfo(1,'gfun',"looking for equation of order",maxord);
	    res:=findequationgivenordermodp(L,ord,z,dz,p,"differential",[0$(ord+1)],true);
	    if res<>FAIL then return res fi
	od;
	FAIL
end:

# returns the exponent and the leading coefficient
# in the q-adic expansion of pol
leadtermandexponentqadicexpansion:=proc(pol,q,x)
local ord,P:=pol;
   if pol=0 then [infinity,0]
   else
	   for ord from 0 while divide(P,q,'P') do od:
	   [ord,rem(P,q,x)]
   fi
end:

# compute the indicial equation using a q-adic expansion
# (see van der Put Singer 2003 p. 102)
# and select its minimal negative integer root
# ldop is given as a list of coefficients
multdenomatsing:=proc(ldop,z,q)
local LL,i,j,deg,mini,indmini,pol,CC,g,T::nothing;
	LL:=map(leadtermandexponentqadicexpansion,ldop,q,z);
	mini:=0;
	indmini:=1;
	for i to nops(LL) do
		if LL[i][1]-(i-1)<mini then mini:=LL[i][1]-(i-1); indmini:=i fi
	od;
	pol:=0:
	for i from indmini to nops(LL) do
		if LL[i][1]-(i-1)=mini then 
			pol:=pol+mul(T-j+1,j=indmini..i-1)*rem(diff(q,z)^(i-indmini)*LL[i][2],q,z)
		fi 
	od;
	CC:=[coeffs(collect(pol,z),z)];
	g:=0: for i in CC do g:=gcd(g,i) od;
	deg:=min(op(select(type,map2(op,1,roots(g)),negint)));
	if deg=infinity then 1 else q^(-deg) fi
end:

# Faster than DEtools[ratsols] in our examples
# By 
#	1) computing indicial equations without using algebraic extensions
# 	2) testing the existence of a rational solution mod p first
# There is still room for improvement if needed by
#	. computing the expansion of the inverse of the multiple of the
#		denominator by Newton iteration rather than quadratically
#	. using the techniques of Bostan, Cluzeau, Salvy (2005) on the
#		mod p part
#	. using modp1 in some parts of the computation
ratsols:=proc(dop,Dz,z)
local ldop,sing,denom,i,sol,c::nothing,ssol,S,sys,deginf,inds,indinf,p,ssolp,vars,mat,rnk,Sp,ratsol,j,lco,a,cc,deg,denomp,ord,prevord,pt;
	ldop:=[seq(coeff(dop,Dz,i),i=0..degree(dop,Dz))];
	# 1. Compute a multiple of the denominator
	indinf:=indicialpolynomial(ldop,z,infinity);
	deginf:=-min(op(select(type,map2(op,1,roots(indinf)),integer)));
	if deginf=-infinity then return [] fi;
	sing:=select(has,map2(op,1,factors(ldop[-1])[2]),z);
	denom:=mul(multdenomatsing(ldop,z,i),i=sing);
	deginf:=deginf+degree(denom,z);
	if deginf<0 then return [] fi;
	# 2. find a nonsingular point for series expansion
	for pt do 
		if eval(ldop[-1],z=pt)=0 then next fi;
		lco:=eval(denom,z=pt);
		if lco<>0 then break fi
	od;
	# 3. find a prime where the denominator is not 0 at pt
	p:=modp1(Prime(1)); # big prime number
	while lco mod p=0 do p:=prevprime(p) od;
	# 4. compute mod p
	denomp:=Expand(subs(z=z+pt,denom)) mod p;
	a[0]:=1/coeff(denomp,z,0) mod p;cc:=-a[0];
	deg:=degree(denomp,z);
	prevord:=0;
	ord:=deginf+5;
	do
		if denom=1 then ssolp:=a[0]
		else
			# 1/denom at sufficiently high precision
			for i from prevord+1 to ord do
				a[i]:=cc*add(a[i-j]*coeff(denomp,z,j),j=1..min(i,deg)) mod p
			od;
			ssolp:=add(a[i]*z^i,i=0..ord)+O(z^(ord+1))
		fi;
		# multiply by numerator
		ssolp:=series(add(c[i]*z^i,i=0..deginf)*ssolp,z,ord+1) mod p;
		# inject in the differential equation
		S:=series(add(subs(z=z+pt,ldop[i])*diff(ssolp,[z$(i-1)]),i=1..nops(ldop)),z,ord+1) mod p;
		sys:=select(has,{op(S)},c);
		if denom=1 or nops(sys)>deginf+2 then break fi;
		prevord:=ord;
		ord:=ord+deginf+5-nops(sys)
	od;
	# 5. solve mod p
	vars:=[seq(c[i],i=0..deginf)]:
	if sys<>{} then # otherwise, we have a solution
		mat:=LinearAlgebra['GenerateMatrix'](sys,vars)[1];
		rnk:=LinearAlgebra['Modular']['Rank'](p,mat);
		if rnk=nops(vars) then return [] fi
	fi;
	# 6. now work over Q
	ratsol:=add(c[i]*z^i,i=0..deginf)/denom;
	Order:=ord;
	ssol:=series(ratsol,z=pt);
	do
		S:=series(add(ldop[i]*diff(ssol,[z$(i-1)]),i=1..nops(ldop)),z=pt);
		sys:=select(has,{op(S)},c);
		ssol:=solve(sys,{op(vars)});
		sol:=collect(subs(ssol,ratsol),[seq(c[i],i=0..deginf)],normal);
		if sol=0 then return [] fi;
		# need to check the result
		Order:=nops(ldop);
		# find another nonsingular point
		for pt from pt+1 while eval(ldop[-1],z=pt)=0 do od;
		ssol:=series(sol,z=pt);
		Sp:=ssol mod p;
		Sp:=series(add(ldop[i]*diff(Sp,[z$(i-1)]),i=1..nops(ldop)),z=pt) mod p;
		if Sp=0 or op(1,Sp)=O(1) then # looks like 0 at z=pt
			inds:=select(has,indets(sol,indexed),c);
			return [seq(coeff(sol,i),i=inds)]
		fi;
	od;
end:

isapparent:=proc(dop,dzz,sing)
local Dz,z,indeq;
	Dz,z:=op(dzz);
	indeq:=indicialpolynomial(dop,dzz,RootOf(sing,z));
#	userinfo(3,'minimizediffeq',sprintf("indicial equation found for factor of degree %d",degree(sing,z)));
	isapparentwithindpol(dop,indeq,Dz,z,sing)
end:

isapparentwithindpol:=proc(dop,indeq,Dz,z,sing)
local expos,ord,i;
	expos:=roots(indeq,z);
	ord:=degree(dop,Dz);
	if {op(map2(op,2,expos))}<>{1} then return false fi;
	expos:=map2(op,1,expos);
	if not type(expos,list(nonnegint)) or
		nops(expos)<>ord then return false fi;
	isapparentwithexps([seq(coeff(dop,Dz,i),i=0..ord)],z,sing,sort(expos))
end:

# exps is the sorted list of local exponents at sing (a polynomial)
isapparentwithexps:=proc(dop,z,sing,exps)
local bigo,missing,sol,i,j,zero,sys,inds,vals,shift,c::nothing,a::nothing,var; # a is a generic point
	bigo:=op(-1,exps)+1;
	missing:=sort([op({$0..bigo-1} minus {op(exps)})]);
	vals:=map2(op,1,map(leadtermandexponentqadicexpansion,dop,sing,z));
	shift:=min(seq(vals[i]-i+1,i=1..nops(vals)));
	for i from nops(exps) by -1 to 1 do 
		sol:=(z-a)^op(i,exps)+add(`if`(j>op(i,exps),c[j]*(z-a)^j,0),j=missing)+
			add(c[j]*(z-a)^j,j=bigo..bigo-shift-1)+O((z-a)^(bigo-shift));
		zero:=series(add(dop[j]*diff(sol,[z$(j-1)]),j=1..nops(dop)),z=a,bigo);
		sys:=[seq(rem(op(2*j-1,zero),subs(z=a,sing),a),j=1..iquo(nops(zero),2)-1)];
		sys:=remove(type,sys,identical(0));
		# the triangular part of the system always has a solution
		while sys<>[] and indets(sys[-1],specindex(c)) minus indets(sys[1..-2],specindex(c))<>{} do
			sys:=subsop(-1=NULL,sys)
		end do;
		inds:=indets(sys,specindex(c));
		# frequent special case
		if nops(sys)=2 and nops(inds)=1 then
			var:=op(inds);
			if rem(coeff(sys[1],var,1)*coeff(sys[2],var,0)
				-coeff(sys[1],var,0)*coeff(sys[2],var,1),subs(var=a,sing),a)<>0 then return false 
			fi 
		elif solve(subs(a=RootOf(sing,z),sys),inds)=NULL then return false
		fi
	od;
	true
end:

# The exponents at regular singular points are algebraic numbers in general.
# Here they are split into RootOf's of irreducible polynomials.
genexpfromindpol:=proc(indpol,z,pt,T)
local facts,point,i,j,lin,high;
	facts:=select(has,factors(indpol,`if`(type(pt,RootOf),pt,NULL))[2],z);
	if pt=infinity then point:=T=1/z else point:=T=z-pt fi;
	lin,high:=selectremove(proc(u) degree(u[1],z)=1 end,facts);
	[[seq(solve(i[1],z)$i[2],i=lin),
		seq(seq(RootOf(i[1],z,index=j)$i[2],j=1..degree(i[1],z)),i=high),point]]
end:

# Following code provided by John May.
# Same behaviour as allvalues(expr,rootoftoprotect,implicit)
# except that allvalues returns nested RootOfs occasionally.
fixedallvalues := proc(expr,rootoftoprotect)
local newexpr, rfs, rf, i, r, out, protected::nothing;
	if nargs>1 then
		newexpr:=subs(rootoftoprotect=protected,expr)
	else newexpr:=expr fi;
   rfs := remove(has, # remove needed in maple2021
   		indets(newexpr, 'specfunc(polynom(anything,_Z), RootOf)'),index);
   if nops(rfs) = 0 then
   		if nargs=1 then return newexpr
        else return subs(protected=rootoftoprotect,newexpr) fi
   end if;
   rf := rfs[1];
   rfs := seq(op(0, rf)(op(rf), index = i), i = 1 .. degree(op(rf), _Z));
   out := [seq(subs(rf = r, expr), r in rfs)];
   return map(thisproc, out)[];
end proc;

cleangenexps:=proc(listlistexpos,pt)
local lchvars,chvar,i,j,k,n,listexpos,ll,protect,L,rf,rfs,deg,res;
	n:=nops(listlistexpos);
	L:=listlistexpos;
	if nargs=2 then	L:=subs(pt=protect,L) fi;
	lchvars:=map2(op,-1,L);
	for chvar in {op(lchvars)} do 
		ll:=NULL;
		for i to n do 
			listexpos:=L[i];
			if op(-1,listexpos)=chvar then 
				listexpos:=listexpos[1..-2];
				rfs:=indets(listexpos,specfunc(anything,RootOf));
				if rfs={} then ll:=ll,op(listexpos)
				elif nops(rfs)>1 then error "unexpected RootOfs",rfs
				elif not irreduc(op(1,rf)) then error "case of reducible exponent not handled",rfs
				else
					rf:=op(rfs);
					deg:=degree(op(1,rf),_Z);
					for j in listexpos do # list of pols in T
						if not has(j,rf) then ll:=ll,j 
						else ll:=ll,seq(subs(rf=RootOf(op(1,rf),index=k),j),k=1..deg)
						fi 
					od
				fi 
			fi
		od;
		res[chvar]:=[ll,chvar]
	od;
	res:=[seq(res[chvar],chvar={op(lchvars)})];
	if nargs=2 then res:=subs(protect=pt,res) fi;
	res
end:

adjoint:=proc(dop,Dx,x)
local res, i, r;
    r:=degree(dop,Dx);
    res:=0;
    for i from r by -1 to 0 do res:=collect(coeff(dop,Dx,i)-res*Dx-diff(res,x),Dx) od;
    collect(res,Dx,normal)
end:


end module:


##########################################################
end use:

#march('create',"minimizediffeq.mla",20);
#savelib(minimizediffeq,"minimizediffeq.mla");
