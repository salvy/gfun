# Compute a small order recurrence for a P-recursive sequence

#### Compute a small order recurrence for a P-recursive sequence.
#
# Input:
# - rec: a recurrence defining uniquely u(n),
# - u,n: names
# - nmax (optional): maximal number of terms such that u(0),...,u(nmax-1) will be guessed with
# - procu (optional): a procedure computing u, should have remember option.
# - nmin (optional, keyword): minimal number of terms s.t.
#      u(0),...,u(nmin-1) will be guessed with
#
# Requirement: u(0),...,u(nmin-1), and rec should define u(n) uniquely.
#
# Output:
# - a valid recurrence, defining u(n).
#
# More and more terms are computed, in order to provide a new operator.
# This operator (or its GCRD with the original one) is a right divisor,
# which is then checked against the initial conditions.

reducerecorder:=proc(rec, u::name, n::name,
                     nmax::{posint,identical(infinity)}:=infinity,
                     procu::procedure:=NULL,
                     {nmin::posint:=8}, $)
local optionalarg, maxordereqn_back, restore_params, NMIN, exp_growth,
   recA, ordA, U, N, guess, recG,
   Ring, A, G, R, Rrat, denRrat, ordR,
   iniA, iniR, PROCU, procv, u_equals_v, needed,
   i, j, k;

   try
      recA:=formatrec([rec,u(n)],'U','N','iniA');
   catch :
      error "invalid input recurrence";
   end try;

   # Procedure computing u(n)
   if procu=NULL then
      # if type(recA,list(polynom(rational,N))) then
      optionalarg:=NULL;
      # else optionalarg:='evalfun=normal' # todo: retester sans, et essayer le guessing quand les entrées ne sont pas normalisées
      # fi;
      PROCU:=rectoproc(rec,u(n),remember,optionalarg)
      else PROCU:=procu
   end if;

   # Maximum order for guessed recurrences,
   # (and backup of the current parameter value)
   if recA[1]<>0 then
      error "non homogeneous recurrence"
   end if;
   ordA:=nops(recA)-2;

   maxordereqn_back:=Parameters('maxordereqn'=ordA-1);
   restore_params:=proc() Parameters('maxordereqn'=maxordereqn_back); end;

   # Initial number of terms to guess with
   exp_growth:=proc(NMIN,nmax) option inline; min(2*NMIN,max(NMIN+1,nmax)); end;

   # Guess an equivalent recurrence, and check it.
   # When checking fails, guess again with more initial values.
   NMIN := nmin;
   while nmax >= NMIN do

      # Guessing
      guess,NMIN:=op(proctorec(PROCU,u,n,NMIN,nmax));

      if guess=FAIL then
         restore_params();
         return rec;

      else

         # Right factor R with polynomial coefficients, via GCRD.
         recG:=formatrec([guess,u(n)]);
         if normal(op(1,recG))<>0 then
            error "inhomogeneous guess" # todo:rectohomrec
         end if;
         Ring:=OreTools['SetOreRing'](n,'shift');
         A:='OrePoly'(op(2..-1,recA));
         G:='OrePoly'(op(2..-1,recG));
         Rrat:=OreTools['GCD']['right'](
            A,G,'method'=':-euclidean',Ring);
         denRrat:=den(Rrat);

         if G='OrePoly'(1) then
            NMIN:=exp_growth(NMIN,nmax);
            next
         end if;

         R:=map( t->normal(t*denRrat), Rrat );
         ordR:=nops(R)-1:

         # Enough initial conditions for R (and maybe more).
         iniA := map2(op,[1,1], `goodinitvalues/rec`(recA,u,n,iniA,true)):
         iniR := iniA union select(t->t>=0, map(i->i-ordR, myisolve(op(-1,R),n)));

         # Checking u = v with v defined by R and these initial conditions.
         procv:=rectoproc(
            { OreTools['Apply'](R,v(n),Ring), seq( v(i)=PROCU(i), i in iniR) },
            v(n), 'remember' );
         u_equals_v := true;
         for i in select( proc(i) i>=NMIN and not(i in iniR) end,
                          {seq(seq(j+k,k=1..ordA-ordR),j in iniR)} ) do
            if normal( procv(i) - PROCU(i) ) <> 0 then
               userinfo( 2, 'reducerecorder',
                         sprintf("Inconsistent initial condition at index %a.",i));
               u_equals_v:=false;
               break;
            end if;
         end do;

         if not u_equals_v then
            NMIN:=exp_growth(NMIN,nmax);
            next
         end if;

         # Eliminating redundant initial conditions
         needed := proc(j)
            if j<ordR then return true
            elif normal( subs(n=j-ordR,op(-1,R)) )=0 then
               return true
            elif normal( add( subs(n=j-ordR,op(i+1,R))*
                              PROCU(j-ordR+i), i=0..ordR ) ) <> 0 then
               return true
            else return false; end if;
         end proc;
         iniR := select(needed, iniR);

         return makerec( [0,op(R)], u, n, {seq(u(i)=PROCU(i), i in iniR)} );

      end if;
   end do;

   restore_params();
   return rec;

end proc;#reducerecorder


#### Guessing a recurrence, using more and more initial values.
#
# Input:
# - procu, u, n: procedure for computing u(n), and the associated names,
# - nmin:
#      minimal number of initial values used for guessing,
# - (optional) nmax:
#      maximal number of initial values used for guessing,
#      it may be infinite. (which is default)
#
# Output: a list
# - [rec,nused]: if some recurrence rec was found, using nused terms for guessing,
# - [FAIL,nused]: if no recurrence was found, using nused terms for guessing.
#
# Specification:
# if nmax is infinite, nmin, 2*nmin, 4*nmin,... terms are tried successively for guessing.
# otherwise, nmin, nmax/2^.., ..., nmax/4, nmax/2, nmax terms are tried successively for guessing.

proctorec:=proc(procu::procedure, u::name, n::name,
                nmin::posint:=8, nmax::{posint,identical(infinity)}:=infinity, $)
description "Guessing a recurrence, using more and more initial values.";
local rec_aux,rec,nused,aux,i;

   if nmax=infinity then
      userinfo(2,'proctorec',"Guessing with",nmin,"terms.");
      userinfo(5,'proctorec',"Sequence computation");
      aux:=[seq(procu(i),i=0..nmin-1)];
      userinfo(5,'proctorec',"Guessing");
      rec:=listtorec(aux,u(n),['ogf']);

      if rec=FAIL then
         return proctorec(procu,u,n,2*nmin,infinity);
      else
         return [rec[1],nmin];
      end if;
   end if;

   if nmin>nmax then return [FAIL,nmax] end if;

   # using max(nmax/2,nmin), and nmin once only.
   rec_aux,nused:=op(proctorec(procu,u,n,nmin,max(iquo(nmax,2),min(nmax-1,nmin))));
   if rec_aux<>FAIL then
      return [rec_aux,nused];
   else
      userinfo(2,'proctorec',"Guessing with",nmax,"terms.");
      userinfo(5,'proctorec',"Sequence computation");
      aux:=[seq(procu(i),i=0..nmax-1)];
      userinfo(4,'proctorec',"Guessing");
      rec:=listtorec(aux,u(n),['ogf']);
      if rec=FAIL then return [FAIL,nmax];
      else
         return [rec[1],nmax];
      end if;
   end if

end proc;#proctorec

## Denominator of an Ore polynomial
den:=proc(A) option inline; global i;
   lcm(seq(denom(op(i,A)),i=1..nops(A)));
end;
