# C-fractions format definition for gfun:-ContFrac

# miscelaneous parameters
local ORD, default_series_margin;
ORD:=6;
default_series_margin:=10;

# A type is defined here for C-fractions (see Cuyt et alii 2008),
# they are particular shapes of continued fractions,
# which correspond to formal power series in one variable x:
#
# cf = a0 + a_1 x^alpha_1 / (1+ a_2x^alpha_2 / (...))
#
# - a0, a_i     are complex numbers,
# - alpha_i     are positive integers.
#
# a_i x^alpha_i is called the i-th partial numerator.
#
# Assumption: alpha_i is assumed to be constant from a certain i on.
#
# - truncation at order n means stopping with (a_n x^alpha_n)
#
# - finite representation is given (as in numtheory[cfrac]) by:
#   [a0, [a_1 x^alpha_1, 1], ..., [a_n x^alpha_n, 1]]
#
# - infinite representation is a record made of
#     + x, a, alpha, n: the names
#     + a0:             value at x=0
#     + init:           list of first partial numerators a_i * x^alpha_i
#                       (its length is denoted 'k' in the following)
#     + reca:           a P-recurrence defining (a_n) for any n>k.
#     + exprsa:
#          possibly rational formulas for tail periodic fractions.
#          + either FAIL if no expression found or computed
#          + or     a list of rational expressions (call its length 'l'),
#                   providing valid rational expressions for a_n for any n>k,
#                   -> for a_k+1, use the first formula, for a_k+2 the second,
#                      etc in a cyclic way.
#     + asympt_alpha:   value of alpha_i for any i>k (assumed constant here)

`type/CFrac` := proc( expr )
   type( op(expr),
         record(
            'x',
            'a',
            'alpha',
            'n',
            'a0',
            'init',
            'reca',
            'exprsa',
            'asympt_alpha')
       ) and
   expr:-x :: name and
   expr:-a :: name and
   expr:-alpha :: name and
   expr:-n :: name and
   expr:-a0 :: complex and
   expr:-init :: list and
   expr:-asympt_alpha :: posint
   ;
end proc;

# Printing a C-fraction.
`print/CFrac` := proc( t, $)
local cf, x, a, n,
   aux,
   k, l,
   generic_formula,
   symb_cf, asympt_a;

   cf := Record( op(t) );
   x, a, n := cf:-x, cf:-a, cf:-n;

   k := nops(cf:-init);

   # symbolic C-fraction
   aux := [ op(cf:-init),
             a[k]*x^cf:-asympt_alpha,
             `...`,
             a[n]*x^cf:-asympt_alpha, `...` ];
   symb_cf := `print/CFRAC`([cf:-a0, op(map(t->[t,1], aux))]);

   # genericity condition
   # startingfrom := k;

   # generic formula (or recurrence)
   cf:-asympt_alpha;

   if cf:-exprsa <> FAIL then
      l := nops(cf:-exprsa);
      if l=1 then
         asympt_a := a[n] = factor(cf:-exprsa[1]);
      elif l=2 then
         asympt_a := a[n] = `print/piecewise`(
            n::even, factor(cf:-exprsa[1]), n::odd, factor(cf:-exprsa[2]));
      else
         return FAIL;
      fi;
      generic_formula := asympt_a;
   else
      generic_formula := cf:-reca;
   fi;

   return symb_cf, generic_formula;
end;

## Compute the first coeffs of a C-fraction.
#
# Input:
# - a procedure: N,x -> S(x) mod (x^N)
# - a variable name x
#
# Output: a procedure
#   N -> list [a1,a2,...] of length N, s.t.
#   S = a1/(1+a2/(1+a3/... .(1+an))),
#      where a1, .., an are nonconstant monomials.
#
# (assuming all exponents are strictly smaller than the parameter alpha_max)

finite_cfrac:=proc(procS, x::name, $)

   if contfrac_type=':-simregular' then

      proc(N::nonnegint, $)
      local S, cfaux, ord, ord1, ord2, cford1, cford2, bigcford, margin;
      option remember;
         bigcford := (2+alpha_max)*N;
         margin := default_series_margin;#4

         # estimating the number of terms needed to compute the expansion
         ord1, ord2 := 3,6;
         cford1,cford2 := op( map(
            proc(ord) -2 + nops(numtheory[':-cfrac']( procS(ord,x), x=0, bigcford,
                                                      contfrac_type, 'quotients' )) end,
            [ord1,ord2]) );
         ord := ord1 + (ord2-ord1) * floor( (N-cford1)/(cford2-cford1) ) + margin;

         # expanding
         S:=procS(ord, x):
         cfaux := numtheory[':-cfrac'](
            S, x=0, bigcford, contfrac_type, 'quotients' );
         if nops(cfaux) > N+1 then
            return map2(op,1,cfaux[2..N+1])
         fi;
         error "Not enough terms."
      end proc;
   else
userinfo(1,'gfuncontfrac',"unsupported continued fraction format");
      return FAIL
   fi;

end proc;

# computation of the first partial numerators of a C-fraction solution
# of a Riccati equation,
# by an indeterminate coefficients method.
#
# Input:
# - riccati: a Riccati equation in y(z), with rational coefficients
# - y, z: function and variable name
# - N : order of the expansion wanted.
#
# Output: a procedure
#   N -> a list [a1,a2,...,aN] of length N, s.t.
#   y(z) = a1/(1+a2/(1+a3/... .(1+an))) is a solution to the Riccati equation,
#   and a1, .., an are nonconstant monomials in z.

mycfrac := proc(riccati, y::name, z::name)
   # todo: type checking
   # initial value

   return
   proc(N::nonnegint)
   option remember;
   local e,emax,tmp,aux,val,c,deq,j,S,k;
      emax := alpha_max;
      deq[0]:=riccati;
      for j to N do
         for e[j] to emax do
            tmp := numer( eval( deq[j-1], y(z)=c[j]*z^e[j]) );
            aux := collect( tmp, z );
            # taking the trailing coeff
            # (manually, MultiSeries:-LeadingTerm being slower)
            S:=convert(series(aux,z,alpha_max*(j+ORD)),polynom):
            if S=0 then
               next fi;
            k:=0: while normal(coeff(S,z,k))=0 do k:=k+1 od;
            val := {solve( coeff(S,z,k), c[j] )};
            if nops(val)=0 and e[j]=emax then
               error "no solution."
            elif nops(val)=0 then
               next
            elif nops(val)>2 then
               error "branching point."
            elif normal(op(1,val))=0 then
               next
            else
               c[j]:=val[1];
               deq[j]:=collect( numer(
                  eval(deq[j-1], y(z)=c[j]*z^e[j]/(1+y(z)) ) ),
                  [diff,y(z)], factor );
               # this 'factor' is important (ex: hypergeom ratio with params).
               break
            fi;
         od;
         if c[j]::name then error "unassigned coeff." fi;
      od;
   return [seq(c[j]*z^e[j],j=1..N)];
   end proc;
end proc;#mycfrac
