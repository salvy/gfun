## Guess and prove a continued fraction expansion,
# related to a given holonomic y(x) at x=0
# (the related functino will be denoted: y1(x))
#
# WARNING: **power series** only, as input (no Laurent series).
#
# Input:
# - diffeq   : a linear differential equation in y,
#              with polynomial coefficients (see gfun)
# - y        : the function name
# - x        : the variable name
# - (optional) names : list of other names used
#   + y1          : function related to y which may have a nice continued fraction expansion
#   + n, a, alpha : index, and C-fraction elements names (see cfrac.mm)
#   + P, Q        : numerator and denominator sequences
#   + H           : poly in (x,P,Q,P',Q',..,a(n),a(n+1),....) st. H_n -> 0 iff
#                   formal convergence holds.
#
# - (optional, keyword) proofelts : boolean indicating if proof elements should be returned
#
# - (optional, keyword) HH  : name for auxiliary sequences concerning H
#     -> HH(k) = H(2k)
#     these objects may or may not appear in the proof depending on the options given.
# - (optional, keyword) procS           : a procedure that maps (N,x)
#    to the Taylor polynomial of degree N for the function, in x.
#
#
# Output: either FAIL, or the expansion,
#    or a list made of the expansion and proof elements (if proofelts is given)
#
# - expansion: a list [val_y1, cf]:
#   + val_y1 is an expression for y1(x) depending on y(x)
#   + cf is the proved infinite formal C-fraction expansion of y1(x)
#        of type `type/CFrac` (see cfrac.mm)
#
# - proof elements : a list [diffeq, defH, recH, ...]
#   + diffeq is a (non linear) Riccati differential equation for y1(x)
#   + defH defines a sequence (H_n) s.t. (H_n)->0 iff cf = initial series
#     (depending on P_n, Q_n, some shifts and derivatives of them,
#      x, a_n, a_n+1, ...)
#   + recH is an equation verified by (H_n), with coeffs poly in (x, a_n, a_n+1, ..)
#   + ... are other proof elements,
#     as provided in `valuationincrease/rational`.
#     Nota: their nb and meaning vary depending on the formulas for (a_n).
#
# Remark:
#   the related function is called y1 according to the parameter given,
#   and has the following shape:
# y1(x) = x^N * ( y(x) - rat(x) ) where
#      rat is of shape P(t)/t^k with P poly of deg < k.
#
# limitations:
# - asymptotically constant exponents ("alpha") in the C-fraction only

series_to_cfrac := proc(diffeq, y::name, x::name,
                     names::list(name):=NULL,
                     {proofelts:=false,
                      HH::name:=':-HH',
                      procS::procedure:=NULL}, $)
local y1, n, a, alpha, P, Q, H,
   aux, riccati, val_y1, Sf, Sg,
   tmp;

   if names<>NULL then
      y1, n, a, alpha, P, Q, H := op(names);
   end if;

   # series computation
   if procS<>NULL then
      Sf:=procS;
   else
userinfo(5,'gfuncontfrac','NoName',"Computing y(x) from ODE,\
(this might add variables instead of initial conditions).");
      Sf:=diffeq_to_taylor(diffeq,y(x));
   fi;

   # transformation into a Riccati equation (at zero).
   aux := function_change( diffeq, y(x), y1, ':-procS'=Sf );
   if aux = FAIL then return FAIL; fi;
   val_y1, riccati := aux;

userinfo(4,'gfuncontfrac','NoName',"Expanding ",y1(x)=val_y1,"as an infinite continued fraction.");
   Sg:=proc(N::nonnegint,x::name) series(subs(y(x)=Sf(N+1,x),val_y1),x,N+1) end;

   # (finite) continued fraction computation and formula guessing
   tmp := riccati_to_cfrac(riccati, y1(x), Sg, ### proofelts, not up to date with proof elements here.
                           'names'=[n, a, alpha, P, Q, H],
                           'othernames'=[HH]);
   if tmp=FAIL then return FAIL; fi;

   res := tmp;
   if proofelts then
      res :=
         [ [val_y1, res[1]], #formula
           [riccati, op(res[2])] # proof elements
         ];
   fi;
   return res;

end proc;#series_to_cfrac
