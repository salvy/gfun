# List of equations from the external sources:
# - Cuyt et alii, 2008

### (soon) Comprehensive list of explicit C-fraction expansions in Cuyt et alii, 2008.
# - Cuyt_Cfrac : table of all functions expanded (todo: doc when stabilized)
# - utility :
#   + labels(chapter) gives the list of all formulas in a chapter
# - notations:
#   + incomplete_gamma

incomplete_gamma := (a,z) -> z^a/a*hypergeom( [a], [a+1], -z );

Ierfc := (k,z) -> 2/factorial(k)/sqrt(Pi) * int( (t-z)^k*exp(-t^2), t=z..infinity);

myHankelH1 := proc(nu, z)
   combine( 2/sqrt(Pi)*exp(-I*(Pi*(nu+1/2)-z))*(2*z)^nu*
            KummerU(nu+1/2,2*nu+1,-2*I*z), power) end;
myHankelH2 := proc(nu, z)
   combine( 2/sqrt(Pi)*exp(I*(Pi*(nu+1/2)-z))*(2*z)^nu*
            KummerU(nu+1/2,2*nu+1,2*I*z), power) end;

# cf. Cuyt (18.2.19) and (18.2.20)
Q01 := proc(z) 1/2*erfc(z/sqrt(2)) end;
R := proc(x) sqrt(2*Pi)*exp(x^2/2)*Q01(x) end;

hankel := (nu,k) -> GAMMA(nu+k+1/2)/factorial(k)/GAMMA(nu-k+1/2);
hP := (nu,invz) -> sum( (-1)^k * hankel(nu,2*k) * (2*invz)^(2*k), k=0/infinity);
hQ := (nu,invz) -> sum( (-1)^k * hankel(nu,2*k+1)*  (2*invz)^(2*k+1), k=0/infinity);

# Cuyt (18.3.2)
II := proc(k, x)
   local j;
   if k=0 then 1/sqrt(2*Pi)*int(exp(-t^2/2),t=x..infinity)
   else
      for j to k do
         II(j,x):=int(subs(x=t,II(j-1,x)),t=x..infinity)
      od:
      II(k,x)
   fi;
end;

# Cuyt (18.4.5)
PP:=proc(x,alpha,theta) 1-GAMMA(alpha,x/theta)/GAMMA(alpha) end;

# list of formulas labels in a chapter
labels := proc(chapter::posint, $)
   if not member(chapter, map(op,[indices(Cuyt_Cfrac)])) then
      return []
   fi;
   return sort( map(op, [ indices(Cuyt_Cfrac[chapter]) ]) )
end:

# Data structure itself
Cuyt_Cfrac := table(
   [ 11 = table(
      [ 1.2 = solve( exp(z)=1+2*z/(2-z+z^2/6/(1+aux(z))), aux(z) ),
        1.3 = exp(z),
        2.2 = solve( ln(1+z) = z/(1+y(z)), y(z) ),
        2.4 = solve( ln((1+z)/(1-z)) = 2*z/(1+y(z)), y(z) ),
        3.7 = solve( tan(z) = z/(1+y(z)), y(z) ),
        # 4.4 variable change needed
        4.5 = solve( arcsin(z)=z*sqrt(1-z^2)/(1+y(z)), y(z) ),
        # 4.6 variable change needed
        # 4.7 variable change needed
        4.8 = solve( arctan(z)=z/(1+y(z)), y(z) ),
        # 4.9 variable change needed
        5.5 = solve( tanh(z)=z/(1+y(z)), y(z) ),
        6.4 = solve( arcsinh(z)=z*sqrt(1+z^2)/(1+y(z)), y(z) ),
        # 6.5-8 variable changes needed
        6.9 = arctanh(z),
        7.1 = solve( (1+z)^alpha = 1+alpha*z/(1+y(z)), y(z) ),
        7.2 = solve( (1+z)^alpha = 1/(1-alpha*z/(1+y(z))), y(z) ),
        7.4 = subs( z=1/z,
                    solve( ((z+1)/(z-1))^alpha = 1+2*alpha/z/(1-alpha/z+y(z)),
                          y(z) ) )
      ]),
     12 = table(
        [  # 12.1.12 non riccati
           # 4.1 non riccati
           # 4.2 non riccati
           # 4.3 non riccati
           # 5.1 non riccati
           # 5.2 non riccati, variable change needed
           # 5.3 non riccati, variable change needed
           # 6.15 equivalent to 6.17 (formally)
           6.17 = subs( z=1/z,
                        GAMMA(a,z)/z^a/exp(-z) ),
           # 6.21 and
           # 6.22 for example values of l>=0 only, see below.
           6.21 = subs( z=1/z,
                        solve(
                           GAMMA(a,z)/z^a/exp(-z) =
                           - sum(z^k/pochhammer(a,k+1),k=0..l-1) + z^l/pochhammer(a,l)*y(z),
                           y(z) ) ),
           6.22 = subs( z=1/z,
                        solve(
                           GAMMA(a,z)/z^a/exp(-z) =
                           - sum(pochhammer(1-a,k-1)/(-z)^k, k=1..l) +
                           pochhammer(1-a,l)/(-z)^l * y(z),
                           y(z) ) ),
           6.23 = z * incomplete_gamma( a, z ) / z^a / exp(-z)
           # 6.24 == 6.23 up to the series definition.
           # 6.25 and
           # 6.26 are similar (todo ?)
        ]),
     13 = table(
        [ 1.11 = sqrt(Pi)*z*exp(z^2)*erf(z),
          2.20 = subs( z=1/z, sqrt(Pi) * erfc(z)*exp(z^2)/z ),
          3.5 = subs(z=1/z, Ierfc(k,z) / Ierfc(k-1,z)),
          4.9 = ( FresnelC(z)+I*FresnelS(z) ) * exp(-I*Pi*z^2/2) / z
#          z^2 * hypergeom( [1], [3/2], -z^2 ) # variable change here.
        ]),
     14 = table(
        [ 1.16 = subs(z=1/z, exp(z)*Ei(nu,z)),
          # 1.18 todo
          # 1.19 equivalent to 1.16
          1.20 = solve( Ei(nu,z) = z^(nu-1)*GAMMA(1-nu)-exp(-z)/z*y(z), y(z) ),
          # 2.21 todo
          2.24 = subs(z=1/z, solve( Ei(-z) = 1/z/exp(z)/(1+y(z)), y(z) ) ) # z>0
        ]),
     15 = table(
        [ 3.3 = hypergeom([a,b],[c],z)/hypergeom([a,b+1],[c+1],z),
          3.4 = z*hypergeom( [a,1],[c+1],z)
        ]),
     16 = table(
        [ 1.13 = hypergeom([a],[b],z)/hypergeom([a+1],[b+1],z),
          1.14 = z*hypergeom([1],[b+1],z),
          2.4 = hypergeom([a,b],[],z)/hypergeom([a,b+1],[],z),
          3.4 = hypergeom([],[b],z)/hypergeom([],[b+1],z),
          5.7 = exp(z) # todo
        ]),
     17 = table(
        [ 1.37 = BesselJ(nu+1,z)/BesselJ(nu,z),
          1.39 = solve(BesselI(n+1,z)/BesselI(n,z) = z/(2*nu+2)/(1+y(z)), y(z)),
          # for technical reasons, a slight modification is performed,
          # on the next two formulas of Cuyt et alii.
          1.44 = subs(z=1/z, 1/z*HankelH1(nu+1,z)/HankelH1(nu,z)),
          1.46 = subs(z=1/z, 1/z*HankelH2(nu+1,z)/HankelH2(nu,z)),
          2.32 = solve( BesselI(nu+1,z)/BesselI(nu,z)=z/(2*nu+2)/(1+y(z)), y(z) ),
          # 2.33 equivalent to 2.32
          2.35 = subs( z=1/z,
                    solve( BesselK(nu+1,z)/BesselK(nu,z) =
                           1/(1-(1+2*nu)/(2*z)/(1+y(z))), y(z) ) )
        ]),
     18 = table(
        [ 2.16 = subs(x=1/z, Q01(x) / ( x*exp(-x^2/2)/sqrt(2*Pi) ) ),
          2.17 = subs(x=z, solve( Q01(x)=1/2-exp(-x^2/2)/x/sqrt(2*Pi)*(x^2/(1+y(x))), y(x))),
          # 2.21 todo ?
          2.22 = subs(x=z, solve(sqrt(Pi/2)*exp(x^2/2)-R(x)=x/(1+y(x)), y(x))),
          3.4 = subs( x=1/z, II(4,x)/II(3,x) ), # todo: 3 -> l
          4.20 = subs(x=z, PP(x,alpha,theta) * GAMMA(alpha) /
                      (x/theta)^(alpha-1)  / exp(-x/theta) )
          # 4.21 is equivalent to 4.20
          # 5.17 is trivially hypergeometric

        ])
   ]
);
