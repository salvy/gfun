# Parameters for gfun:-ContFrac

# type of continued fraction looked for, see numtheory[cfrac] for a description
# for the moment, simregular fractions only (i.e. C-fractions, see cfrac.mm)

contfrac_type := ':-simregular';

# bound on the exponents of the partial numerators.

alpha_max := 2;

# solve the recurrences on the coefficients for rational expressions

do_solve := true;

params_list := {
   ':-contfrac_type',
   ':-alpha_max',
   ':-do_solve',
   ':-shiftmax',
   ':-rational_coeffs'
};

## see the help page
Parameters := proc(x)
local old, param_name, p, q, ii;
   if nargs > 1 then
      return op(map(procname,[args]));
   end if;
   if x::'`=`' then
      param_name := CheckName(lhs(x));
      old := thismodule[param_name];
      if member(param_name, params_list) then
         if param_name=':-contfrac_type' then
            if rhs(x) :: identical(':-simregular') then
               p:=[exports(thismodule)];
               q:=[exports(thismodule,'instance')];
               member(param_name,p,'ii');
               q[ii] := rhs(x);
               old;
            else
               error "invalid value for %1: %2", param_name, rhs(x);
            end if;
         elif param_name=':-alpha_max' then
            if rhs(x) :: posint then
               p:=[exports(thismodule)];
               q:=[exports(thismodule,'instance')];
               member(param_name,p,'ii');
               q[ii] := rhs(x);
               old;
            else
               error "invalid value for %1: %2", param_name, rhs(x);
            end if;
         end if;
      else
         error "Not a parameter: %1", param_name;
      end if;
   else
      thismodule[CheckName(x)];
   end if;
end proc:

## check the name of a parameter
CheckName := proc(x)
local param_name;
   param_name := convert(x, '`global`');
   if not member(param_name, params_list) then
      error "invalid option: %1", param_name;
   else
      param_name
   end if;
end proc:
