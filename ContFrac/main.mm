###    This program is free software: you can redistribute it and/or modify
###    it under the terms of the GNU General Public License as published by
###    the Free Software Foundation, either version 3 of the License, or
###    (at your option) any later version.
###
###    This program is distributed in the hope that it will be useful,
###    but WITHOUT ANY WARRANTY; without even the implied warranty of
###    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
###    GNU General Public License for more details.
###
###    You should have received a copy of the GNU General Public License
###    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#    Title:   ContFrac module
#    Created: Fri Jul   4  2014
#    Authors: Sébastien Maulat <sebastien.maulat@ens-lyon.fr>
#
# Description: Automatic continued fractions expansions,
#              using D-finite series and sequences.
#
# Major Modifications:
# - 7/2014: Birth (S. Maulat)
# - 7/2015: better pretty printing (S. Maulat)

ContFrac := module()

description
   "Continued Fractions for Special Functions"
   "Corresponding continued fractions with rational coefficients, solutions of Riccati equations with rational coefficients.";

option
   package, load = setup;

global

   # parameters in the database,
   # (examples in the compendium by Cuyt et alii).

   k, l, z,
   alpha,
   a, b, c, n, nu,

   # types

   cfrac,
   `type/CFrac`,
   `print/CFrac`,
   pretty_print_cfrac;

export

   # C-fractions
   # - guessing
   guess_cfrac,

   # - proving
   riccati_to_cfrac, # C-fraction from a Riccati equation
   holexpr_to_cfrac, # function change and C-fraction expansion

   # parameters (should be private?)
   Parameters,
   contfrac_type,
   alpha_max,
   do_solve,

   # demo utilities
   expr_to_cfrac,

   # data base
   Cuyt_Cfrac,
   labels,

   # Cuyt definitions of some special functions
   incomplete_gamma,
   Ierfc,
   myHankelH1, myHankelH2,
   Q01, R,
   II, PP,
   hankel, hP, hQ
;

local

   setup,

   finite_cfrac,
   mycfrac,
   `diffeq_to_riccati/atzero`,

   # parameters and defaults
   params_list,
   CheckName,
   default_HH,
   default_k,
   default_maxnbused,
   default_maxnbused_guess,
   default_minnbused,
   default_minnbused_guess,
   default_pnumer,
   default_x1,

   # utilities
   is_riccati,
   recP,

   # heuristics, related to ddmf
   variable_change,
   `related_riccati/atzero`,
   related_riccati,

   # misc
   periodic_ratexprs,
   to_riccati_2ndorder,
   diffeq_to_taylor,
   diffeq_to_defH,
   defH_to_recHsymb,
   get_listP,
   get_listQ,
   get_listH,
   readvaluationincrease,
   combinerecs,
   combinerecs2,
   `valuationincrease/rational`,

   # demo utilities
   guessriccati,
   `guessriccati/simple`,
   guessdifference,
   mytaylor,

   # old ideas
   series_to_cfrac

;

# parameters
$include <ContFrac/parameters.mm>

# demo utilities
$include <ContFrac/utils.mm>

# data base
$include <ContFrac/db.mm>

# heuristics: especially for converting into Riccati equations
$include <ContFrac/heuristics.mm>

setup := proc()
   description "setup procedure for ContFrac";
   global `type/CFrac`, `print/CFrac`;

# C-fractions
$include <ContFrac/cfrac.mm>

end proc;


#################### Guessing tools ##########################################


## Default values
default_minnbused_guess := 8;
default_maxnbused_guess := 16;
default_minnbused := 8;
default_maxnbused := 16;
default_pnumer := ':-p';
default_x1 := ':-x1';
default_HH := ':-HH';
default_k := ':-k';


## Guess the general shape of a C-fraction.
#
# Input:
# - v0: an initial value (at zero)
# - proccf: a procedure (should have remember option)
#           Input: N>=0
#           Output: [a1, ...,aN] representing
#                   y(x) = a1/(1+a2/(1+a3/... .(1+aN)))
#                   (a1, .., an are nonconstant monomials in x).
# - x:  name of the main variable
# - a, alpha: names for the numerators coefficients and exponents
# - n: name for the recurrence index
# - <minnbused> (optional): minimum number of (consecutive) terms to be used
# - <maxnbused> (optional): maximum ...
# - <pnumer> (optional, keyword): name ("partial numerator")
#
# Output: (nb_used, cf) where
# - nb_used: number of partial numerators used for guessing
# - cf:      a C-fraction as defined in `type/CFrac`

guess_cfrac := proc(
   v0, proccf,
   x::name, a::name, alpha::name, n::name,
   {minnbused::nonnegint := default_minnbused,
    maxnbused::posint := default_maxnbused,
    pnumer::name:= default_pnumer}, $)

   local
   proc_pnumer, rec_pnumer, nbused, expr_pnumer,
   frec, ord, nbextraini, goodini,
   maxn, rec_pnumer2, U, N,
   extraini, exprsa, asympt_alpha, guessed_cf,
   aux, eq, i, ini, j;

   proc_pnumer:=proc(n,$)
      proccf( min( 2^(ilog2(max(2,n))+1), maxnbused) )[n+1];
   end;

   # guessing a recurrence on the partial numerators
   rec_pnumer, nbused := op( gfun:-proctorec(
      proc_pnumer, pnumer, n, minnbused, maxnbused ) );
   if rec_pnumer = FAIL then return FAIL fi;

   # formatting recurrence, computing offsets and initial values
   frec := formatrec([rec_pnumer,pnumer(n)],'U','N','ini');
   ord := nops(frec)-2;
   goodini:=`goodinitvalues/rec`(frec,pnumer,n,ini,false);
   # (todo) assumption: goodini of the shape a(0)=.., a(1)=.., ...
   nbextraini:=nops(goodini)-ord;
   rec_pnumer2 := select(t -> not has(t,{seq(pnumer(i),i=0..nbextraini-1)}), rec_pnumer);
userinfo(3,'gfuncontfrac','NoName',"recurrence found for partial numerators: ",rec_pnumer2);

   # solving, when parameter do_solve is true
   if do_solve then

      expr_pnumer := rsolve(rec_pnumer2, pnumer(n)) assuming n >= nbextraini;

      # todo: heuristic if it failed: shifting the recurrence
      # (in particular for gamma)

      # increasing the shift to remove special cases in the definition
      if expr_pnumer::piecewise then
         # max index n st u(n) appears in special cases
         maxn := seq( subs( eq, n ),
                     eq in [seq(op(2*i+1, expr_pnumer), i=0..(nops(expr_pnumer)-3)/2)]);
         nbextraini := max( nbextraini, maxn+1 );
      fi;

      # obtaining a single expression for pnumer(n)
      # the piecewise part will be stored into extra initial conditions
      expr_pnumer := simplify(expr_pnumer) assuming n >= nbextraini;
      exprsa := periodic_ratexprs( n, subs(x=1, expr_pnumer) );
   else
      exprsa := FAIL;
   fi;

   # initial conditions
   aux := rectoproc(rec_pnumer,pnumer(n));
   extraini := [ seq( simplify(aux(j)), j=0..nbextraini-1) ];

   # extracting asymptotic degree in the first asymptotic value
   asympt_alpha := degree( proccf(nbextraini+2)[-1], x );

   guessed_cf := CFrac(Record(
      ':-x'=x, ':-a'=a, ':-alpha'=alpha, ':-n'=n,
      ':-a0'=v0,
      ':-init' = extraini,
      ':-reca' = subs(pnumer=a, x=1, rec_pnumer2),
      ':-exprsa' = exprsa,
      ':-asympt_alpha' = asympt_alpha));

   return nbused, guessed_cf;

end proc;#guess_cfrac


##################### Main exported functions ##################################

## Guess and prove a continued fraction expansion,
# for a series solution y(x) of a Riccati equation.
# This is where the CORE procedure of this package.
#
# Input:
#
# - riccati : a generalized Riccati equation:
#              y' = poly(x,y(x))
# - y(x)    : the function name
# - S       : a procedure (n,x) -> y(x) mod (x^n)
#              (in Taylor form)
# - names (optional): list of names used in the proof
#     + n, a, alpha: index, and C-fraction elements names (see cfrac.mm)
#     + P, Q       : numerator and denominator sequences
#     + H          : poly in (x,P,Q,P',Q',..,a(n),a(n+1),....) st. H_n -> 0 iff
#                     formal convergence holds.
#     + HH, k (optional): intermediate sequence, with meaning HH(k)=H(2n).
#
# - prooftable (optional): table used for storing proof elements along the computation
#
# - minnbused, maxnbused (optional, keyword):
#      minimal and maximal number of terms in C-fraction to be used
#
# - guessonly (optional, keyword, undocumented outside): boolean, defaults to false:
#      indicates wheather the proof step should be bypassed.
#
# Output: either FAIL, or a C-fraction expansion of the solution y(x).
#   This expansion is PROVED --- except if 'guessonly' is given.
#   It is of type `type/CFrac` (see cfrac.mm),
#   and proof elements are stored into 'prooftable' during computations.
#
# Side effects: if given, 'prooftable' is added the following entries:
#   + prooftable[rem_def]: equation H(n) = poly defining the remainder 'H(n)' in function of
#              the continued fraction elements and of the canonical
#              numerators/denominators of the convergents:
#                 a(n), x, P(n,x), Q(n,x), their derivatives and shifts.
#         we have the property: (H_n)->0 iff cf = initial series
#   + prooftable[rem_rec]: recurrence defining H(n), with coefficients polynomial in
#       the variable x and the partial numerators a(n), a(n+1), ... of the continued fraction.
#   + and other fields, see `valuationincrease/rational`.
#
# Limitations:
# - asymptotically constant exponents ("alpha") in the C-fraction only,

riccati_to_cfrac := proc(
   riccati, yofx, S,
   names::list(name):=[n, a, alpha, P, Q, H, default_HH,default_k],
   prooftable::table:=NULL,
   {minnbused::nonnegint := default_minnbused_guess,
    maxnbused::nonnegint := default_maxnbused_guess,
    guess_only::truefalse:=false}, $)

local riccati_expr,
   y, x, n, a, alpha, P, Q, H, HH, k,
   cf_ini, tmp, nbused, CF, cf,
   v0, defH, recHproved,
   proca_aux, proca, procalpha,
   listP, listQ, listH;

   # Arguments reading
   getname(yofx, y, x);
   if type(riccati,`=`) then riccati_expr := lhs(riccati)-rhs(riccati)
   else riccati_expr := riccati end if;
   if not is_riccati(riccati_expr,y(x)) then
      error "wrong argument to riccati_to_cfrac" end if;

   n, a, alpha, P, Q, H := op(names[1..6]);
   if nops(names)>6 then HH:=names[7] else HH:=default_HH end if;
   if nops(names)>7 then k:=names[8] else k:=default_k end if;

   # Guessing, on more and more terms
   v0 := simplify( coeff(S(1,x),x,0) );
   riccati_expr := collect( eval(riccati_expr,y(x)=v0+y(x)), [diff,y], normal );#todo
   cf_ini :=  # use mycfrac instead to compute from the Riccati equation.
      gfun:-ContFrac:-finite_cfrac( S, x );

userinfo(1,'gfuncontfrac','NoName',"Guessing a formula");
   tmp := guess_cfrac( v0, cf_ini, x, a, alpha, n,
                     ':-minnbused'=minnbused, ':-maxnbused'=maxnbused );
   if tmp=FAIL then
userinfo(0,'gfuncontfrac','NoName',"No formula found.");
      return FAIL;
   fi;
   nbused, CF := tmp;

   # Returning if guess_only option provided
   if guess_only then return CF; fi;

userinfo(1,'gfuncontfrac','NoName',"conjecture formula (on",nbused,"coefficients)", print(CF) );

   # Recursive definition of a sequence of "remainders": (H_n),
   # H_n being the numerator of the differential equation applied to the rational approximants
   # --> H_n = numer( diffeq(v0+P_n/Q_n) )
   # we have the following
   # Lemma: val(H_2n) -> infinity     iff    the guessed formula is true.
   defH := diffeq_to_defH( riccati_expr, y, x, [n, P, Q] );
   if defH=FAIL then return FAIL; fi;

   if prooftable <> NULL then prooftable['rem_def'] := H(n) = defH; end if;

userinfo(1,'gfuncontfrac','NoName',
            sprintf("defining a sequence %a, which relates to convergence.",H(n,x)));
userinfo(2,'gfuncontfrac','NoName',print(H(n,x)=defH));
userinfo(1,'gfuncontfrac','NoName',
            sprintf("lemma: the conjecture holds iff there exists an unbounded %a s.t., %a tends to 0.",':-i'(n),H(':-i'(n),x)));
userinfo(1,'gfuncontfrac','NoName',"  (i.e., their valuations tend to infinity)");
userinfo(3,'gfuncontfrac','NoName',sprintf(
      "  (%a and %a are the canonical numerator and denominator of truncation)",P,Q));

userinfo(1,'gfuncontfrac','NoName',
            sprintf( "proving %a:",':-Limit'('val'(H(n,x)),n='infinity')='infinity'));
userinfo(1,'gfuncontfrac','NoName',
            sprintf("- computing a recurrence for %a.",H(n,x)));

   recHproved:=subs(HH=H,
                    eval(defH_to_recHsymb(CF, defH, H, P, Q),
                         H=proc(n) HH(n) end));

   if prooftable <> NULL then prooftable['rem_rec'] := recHproved; end if;

userinfo(1,'gfuncontfrac','NoName',"  (which does not conclude)",
            print(collect(recHproved,[H,x],proc(t) `...`*x^ldegree(t,x) end)=0));
userinfo(4,'gfuncontfrac','NoName',print(collect(
            sort(isolate(recHproved, H(n+4)),[H,n]),
            [H,x],proc(t) factor(t) end)));

   # Computing the values (H_n), using the coefficients recurrences.
   # (and elementary properties of continued fractions, see [Cuyt 2008]).
   cf := op(1,CF);
   proca_aux := rectoproc(cf:-reca,a(n),remember);
   proca := proc(n::nonnegint)
      if n < nops(cf:-init) then subs(x=1,cf:-init[n+1])
      else proca_aux (n) fi;
   end proc;
   procalpha := proc(n::nonnegint)
      if n < nops(cf:-init) then degree(cf:-init[n+1],x)
      else cf:-asympt_alpha fi;
   end proc;
   listP:=proc(N, $) get_listP(x, [proca, procalpha], N); end;
   listQ:=proc(N, $) get_listQ(x, [proca, procalpha], N); end;
   listH:=proc(N, $) get_listH(defH,P,Q,n,x,listP,listQ,N) end;

   # Proof of H_n -> 0 (i.e. valuation increase).
   tmp:=`valuationincrease/rational`( CF, listH, recHproved, H, [HH,k], prooftable );
   if tmp=false then return FAIL; fi;

   return CF;

end proc;#riccati_to_cfrac


## Compute and prove a continued fraction expansion,
# involving a given holonomic function at a given point.
# A heuristic function change is performed in order to get a formula.
#
# Symbolically, f = y(x), and an expansion is given for a related y[1] ( x[1] ),
#   which is abbreviated into y1(x1)
# - x1 is a fractional power of a shift (or inverse) of x
# - y1(x1) consists of:
#   + y(x) itself (expressed in x1), or its logarithmic derivative in x1,
#   + shifted by its fractional part in x1
#
# Input:
# - f, a holonomic expression,
# - y, a symbolic name for f,
# - x=v, a name and a value for the main variable (x),
# - y1, a name for the new function,
# - x1, a name for the new variable (distinct from x if x_eqn is not x=0),
# - names (optional): n, a, alpha, P, Q, H, and optionally HH, k. See riccati_to_cfrac,
# - prooftable (optional): table to store elements proving the Cfraction expansion,
#     see riccati_to_cfrac
# - expr_x (optional): name, if given it is assigned the value of x depending on x1.
# - expr_y1 (optional): name, if given it is assigned the value of y1(x1) depending on y and x1.
#
# Output: either FAIL or a (proved) continued fraction expansion for y1(x1).
#
# Side effects:
# - expr_x: if given, it is assigned the value of x depending on x1.
# - expr_y1: if given, it is assigned the value of y1(x1) depending on y and x1.
# - prooftable: used as in riccati_to_cfrac.
#
# fixme: use better series computation procedures.
holexpr_to_cfrac:=proc(
   f, y::name, x_eqn::`=`(name,{rational,identical(infinity)}),
   y1::name, x1::name:=default_x1,
   names::list(name):=[n, a, alpha, P, Q, H, default_HH, default_k],
   prooftable::table := NULL,
   expr_x::name:=NULL, expr_y1::name:=NULL, $)

   local x, v, x_of_x1, y1_eqn, deq1, S1, cf;

   x := op(1,x_eqn);

   # Function change: from y(x) to y1(x1) at x1=0,
   # which may satisfy a C-fraction expansion formula.
   x_of_x1, y1_eqn, deq1 := op( related_riccati( f, y, x_eqn, y1, x1 ) );

   S1 := proc(N,z)
      mytaylor( eval( subs(x=x_of_x1, x1=z,
                           eval(rhs(y1_eqn),y=proc(b) subs(x=b,f) end)) ),
                z, N) end;

   cf := riccati_to_cfrac( deq1, y1(x1), S1, names, prooftable );

   if cf = FAIL then return FAIL; fi;
   if expr_x <> NULL then expr_x := x_of_x1 end if;
   if expr_y1 <> NULL then expr_y1 := y1_eqn end if;
   return cf;

end proc;#holexpr_to_cfrac



# todo
is_riccati := proc(expr, yofz)
   local y,z;
   y,z := 'y','z';
   getname(yofz,'y','z');
   if not has(expr, [y,z]) then
      return false;
   end if;
   return true;
end;


## Numerator of a differential equation for y(x), when y(x) = P(n,x)/Q(n,x)
#
# Input:
# - diffeq: a differential equation in y(x)
#     that is, a gfun differential equation, or any
#     or any differential equation polynomial in x, y(x), y'(x), ...
# - y, x: names
# - (optional) names : list of other names used
#   + n           : index
#   + P, Q        : numerator and denominator sequences
#
# Output: expression in terms of P,Q and their derivatives
# rq: P and Q are given as global names here.
#
# the convergence of this term to infinity (with n) caracterizes the convergence of the continued fraction P(n)/Q(n) to the initial function

diffeq_to_defH:=proc(diffeq, y::name, x::name,
                  names::list(name):=[n, P, Q], $)
local n, P, Q,
   ratdiffeq,recop,defH;

   n, P, Q := op(names);

   if diffeq::set then
      recop:=op(select(has,diffeq,diff));
   else recop:=diffeq; fi;
   ratdiffeq:=normal(expand(subs(y(x)=P(n,x)/Q(n,x),
                            recop)));
   defH:=numer(ratdiffeq);
   return(defH);
end proc;


## Compute a linear relationship between H_n, H_{n+1}, H_{n+2}, ...
# related to a polynomial in the numerators and denominators
# of a C-fraction, and their derivatives
#
# the recurrence is valid for n large enough only
#
# Input:
# - CF: a C-fraction
#       (which provides the names x, n)
# - defH: a definition of H_n, in terms of n,
#         a_n, x, and P_n, Q_n the numerators and denominators of cf.
# - H, P, Q: names for the involved sequences
#
# Output:
# - a linear recurrence on H(n,x), H(n+1,x), ...
#   (with non-polynomial coefficients, which may depend on a_n, a_{n+1}, ...)

defH_to_recHsymb:=proc(CF, defH, H::name, P::name,Q::name,
                    {verifs:=false}, $)
local cf, x, a, n, alpha,
   recPn, recQn,
   ordmax, recHinit, eqs, reduction,
   diffordmax, reduction_diffs,
   i, diff_idx,# auxiliary values
   recHx, recHxsymbolic, symbolic_generators, recHxsymb,
   recHverif,
   sys, solc, found, recHxexplicit,
   b, c, d, j;

   # names
   cf := op(1,CF);
   x, a, n, alpha := cf:-x, cf:-a, cf:-n, cf:-alpha;
   b, c, d := 'b', 'c', 'd';# formal names

   # recursive definition of the convergents (valid for n large enough)
   if not contfrac_type=':-simregular' then
userinfo(1,'gfuncontfrac','NoName',"only simregular fractions supported.");
      return FAIL;
   fi;

   recPn := eval( subs(n=n+2, recP(P,n,x,a,alpha)), alpha=proc() cf:-asympt_alpha end);
   recQn:=subs(P=Q,recPn);

   # using the general recurrence
   # recHxexplicit :=
   # eval( diff(a(k+4,x),x)*H(k+4,x) + (a(k+3,x)/diff(a(k+3,x),x)-(a(k+4,x)+1)/diff(a(k+4,x),x))*H(k+3,x) - (a(k+3,x)*(a(k+3,x)+1)/diff(a(k+3,x),x)+a(k+4,x)*(a(k+4,x)+1)/diff(a(k+4,x),x))*H(k+2,x) - ((a(k+2,x)+1)/diff(a(k+2,x),x)-a(k+4,x)/diff(a(k+4,x),x))*a(k+2,x)^2*H(k+1,x) + a(k+1,x)^2*a(k+2,x)^2/diff(a(k,x),x)*H(k,x));

   # maximum order needed
   ordmax:=8;                        # todo: dynamic calculation of this with formal reduction,
   recHinit:=add(c[i]*H(n+i,x),i=0..ordmax);

   # recurrences for P_n,Q_n and their derivatives.
   eqs[0]:={recPn,recQn};
   reduction:=eqs[0]:
   for i to ordmax do
      eqs[i]:=subs(n=n+1,eqs[0],eqs[i-1]);
      reduction:=reduction union eqs[i];
   od:
   diffordmax:=2;
   reduction_diffs[0]:=reduction;
   for i to diffordmax do reduction_diffs[i]:=map(diff,reduction_diffs[i-1],x); od;
   reduction_diffs:=seq(op(reduction_diffs[j]),j=0..diffordmax):

   # Recurrence with indeterminate coefficients, with symbolic products (ex:Q'(n+2,x)=b[2,1]).
   recHx:=subs(reduction_diffs,eval(recHinit,H=proc(m) subs(n=m,defH) end)):
   diff_idx:=[seq(diffordmax-i,i=0..diffordmax)];

   # replacing products of P and Q (possibly differentiated, and possibly shifted by 1) by symbolic variables.
   recHxsymbolic:=subs(seq(seq(diff(Q(n+j,x),[x$i])=b[i,j],j=0..1),i=diff_idx),
                       seq(seq(eval(diff(Q(n+j,x),[x$i]))=b[i,j],j=0..1),i=diff_idx),
                       seq(seq(diff(P(n+j,x),[x$i])=d[i,j],j=0..1),i=diff_idx),
                       seq(seq(eval(diff(P(n+j,x),[x$i]))=d[i,j],j=0..1),i=diff_idx),
                       expand(recHx));
   symbolic_generators:= [seq(seq(d[i,j],j=0..1),i=diff_idx),seq(seq(b[i,j],j=0..1),i=diff_idx)];
   if verifs then
      recHverif:=subs([seq(seq(b[i,j]=diff(Q(n+j,x),[x$i]),j=0..1),i=diff_idx),
                      seq(seq(d[i,j]=diff(P(n+j,x),[x$i]),j=0..1),i=diff_idx)],
                      recHxsymbolic);
      if not expand(recHverif-recHx)=0 then
         WARNING("This is probably a bug: symbolic recurrence different from original one!");
      fi;
   fi;
   # Looking for the coefficients with linear solving
   recHxsymb:= expand(recHxsymbolic):
   recHxsymb:= collect(recHxsymb,symbolic_generators,':-distributed');
   sys:={coeffs(recHxsymb,symbolic_generators)}:
   # Eliminating unnecessary terms
   solc:=solve(sys,{seq(c[i],i=0..ordmax)}):
   found:=false;
   for i to ordmax do
      if subs(solc,c[i])=c[i] then
         if not found then solc:=subs(c[i]=1,solc) union {c[i]=1}; found:=true;
         else solc:=subs(c[i]=0,solc) union {c[i]=0}; fi;
      fi
   od;
   # check that the denominator is asymptotically non zero.
   if not coeff(collect(denom(subs(solc,recHinit)),[x,a],':-recursive',expand),x,0)=1 then
      WARNING("the recurrence denominator has a constant coeff (wrt. ",x,") <>1"); fi;
   recHxexplicit:=collect(numer(subs(solc,recHinit)),[H,x,a],':-recursive',expand):
userinfo(5,'gfuncontfrac','NoName',H(n,x),"satisfies :",recHxexplicit);
   return recHxexplicit;
end proc;#defH_to_recHsymb


# Check if a given P-recurrence on (H_n) trivially implies the conclusion
# (i.e. valuation(H_n)->infty when n->infty).
# ex: H_{n+1}=x*H_n
#
# Input: a homogeneous linear differential equation.
#
# Output: a boolean.

readvaluationincrease:=proc( recHguessed, h::name, n::name,
                             x::name, expralpha::nonnegint, $)
local REC, ord, i;
   REC:=formatrec([recHguessed,h(n)]);
   if REC[1]<>0 then
userinfo(0,'gfuncontfrac','NoName',"A non-homogeneous equation cannot prove",'val'(H(n,x)),"->",infinity,".");
userinfo(5,'gfuncontfrac','NoName',"rec:",REC);
      return false; fi;
   ord:=nops(REC)-2;
   return evalb(
      ldegree(add(REC[2+i]*x^(expralpha*i),i=0..ord),x)
      >= expralpha*ord );
end proc;


## Given two homogeneous operators for H(n),
# which vanish on H for n even and n odd (respectively),
# compute a two by two recurrence operator for HH_k=H_2k.
#
# Input:
# - x : (reserved) name for the main variable appearing in the recurrence
# - recHeven, recHodd : homogeneous recurrences valid for n even (resp. odd)
# - H(n) : names for the sequence
# - HH(k) : names for the index and sequence seen two by two.
#
# Output:
# - recurrence valid for HH(k), meaning H(2n).

combinerecs:=proc( x, recHeven, recHodd, Hofn, HHofk, $)
local H, n, HH, k,
   rec, rec1, ord, ord1,
   beta, c,
   receven0, receven1, eqseven, nbrewrite,
   i, j,
   recHHeveninit, zero_even, zero_even_symb,
   sys_even, solc_even, solc,
   found, recHHeven, res;

   # Registering the names and recurrences
   getname(Hofn,'H','n');
   getname(HHofk,'HH','k');
   rec,rec1:=formatrec([recHeven,H(n)]), formatrec([recHodd,H(n)]);
   if not rec[1]=0 and rec1[1]=0 then
      error "non homogeneous recurrences";
   end;
   ord,ord1:=nops(rec)-2,nops(rec1)-2;

   # symbolic variable names
   beta, c := 'beta', 'c';

   # Combining equations assuming n is odd, with linear algebra.
   # Linear operators are expressed  in the canonical basis, 1,S,S^2,...
   # the matrix columns consist of E,S^2E,S^4E,..., SO, S^3O, ...
   # with E and O the operators (for n even and odd respectively)
   receven0 := isolate(recHeven,H(n+ord)):
   receven1 := subs(n=n+1,receven0,isolate(recHodd,H(n+ord1))):
   eqseven[0] := [receven0,receven1]:
   nbrewrite:=iquo(ord+ord1+1,2)+1;
   for i to nbrewrite do
      eqseven[i] := normal(subs(n=n+2,seq(eqseven[j],j=0..i-1),eqseven[i-1]));
   od:
   recHHeveninit := add(c[i]*H(n+2*i),i=0..ord+ord1);
   zero_even := subs([seq(op(eqseven[i]),i=0..nbrewrite)],recHHeveninit):
   zero_even_symb := collect(subs([seq(H(n+j)=beta[j],j=0..ord+ord1)],zero_even),
                             [seq(beta[j],j=0..ord+ord1)],'distributed'):
   sys_even := [coeffs(zero_even_symb,[seq(beta[i],i=0..ord+ord1)])]:
   solc_even:=solve(sys_even,{seq(c[i],i=0..ord+ord1)}):

   # Eliminating unnecessary terms
   solc:=solc_even;
   found:= false;
   for i to ord+ord1 do
      if subs(solc,c[i])=c[i] then
         if not found then solc:=subs(c[i]=1,solc) union {c[i]=1}; found:=true;
         else solc:=subs(c[i]=0,solc) union {c[i]=0}; fi;
      fi
   od;
   recHHeven:=collect(subs(solc,recHHeveninit),H,normal):

   res:=collect(eval(subs(n=2*k,numer(recHHeven)),H=proc(m) HH(m/2) end),[HH,x],normal):

   return res;

end proc;


## Variant

combinerecs2 := proc(
   x::name, recHeven, recHodd, Hofn, HHofk, $)
local H, n, HH, k,
   ord, c,A,B,C,D,E, theci,
   eq0, eqs, i, j,
   gens, coords, cs,
   rec;

   # Registering the names and recurrences
   getname(Hofn,'H','n');
   getname(HHofk,'HH','k');
   c := [A,B,C,D,E];

   # shifting recurrences
   ord := 4;# todo: do this in a more generic way
   # in the two next lines, the option factor saves memory and time,
   # in particular for the case of ratios of contiguous hypergeometric functions,
   # with 3 parameters.
   eq0 := collect( isolate(recHeven,H(n+ord)), [H,x], factor );
   eqs := {eq0, collect( isolate(subs(n=n+1,eq0,recHodd),H(n+ord+1)),
                         [H,x], factor)};

   for i to ord-1 do
      eqs := eqs union subs(n=n+2,eqs,eqs)
   od:

   gens:=[seq(H(n+j),j=0..ord-1)]:
   coords:=coeffs( collect(subs(eqs, add(c[i+1]*H(n+2*i),i=0..ord)), gens), gens ) :
   cs:=solve( {coords}, {seq(c[j+1],j=0..ord)} ):
   theci:=op(select(has,indets(op([1,2],cs)),{op(c)}));
   rec := collect(
      numer( normal(subs( cs, theci=1, add(c[j+1]*H(n+2*j),j=0..ord) ) )),
      [H,x] ):

   return subs( n=2*k, eval(rec,H=proc(n) HH(n/2) end) );

end proc;


## Prove that the valuation of a remainder (H_n) increases
# - H_n is defined by a linear recurrence with coefficients
#      which depend on the partial numerators (pnumer_n) of the C-fraction given
# - explicit rational formulas defining (pnumer_n) are used
#      as specified in `type/CFrac`
#      (the starting index does not matter here)
#
# Input:
# - CF: a continued fraction
#      which partial numerators are denoted: (pnumer_n) = (a(n)*x^alpha(n)).
#      remember it gives access to the names a, alpha and n.
# - listH: a procedure providing the values H_n
#      listH(n) = [H_0, ..., H_n]
# - recHproved: a (proved) linear recurrence on H_n
#      with coefficients depending on {pnumer_n, pnumer_{n+1}, ..., pnumer_{n+ord}}
#      where ord is the order of the recurrence
#      rq : the same n as in CF has to be used as index.
# - H: the sequence name
# - (optional) names: list of additional names, [HH,k] with HH(k) = H(2k).
# - prooftable (optional): table in which proof elements will be stored.
#
# Output:
#   true or false, wheather the procedure could prove the valuation increase.
#
# Side effects: if the result if true, the following proof elements are added as entries to prooftable, with evocative names:
#   + rem_seq: names H(n)
#  plus, only if the period of CF is 1:
#   + smallrec: recurrence of small order with rational coefficients satisfied by H(n)
#   + (in the format of gfun recurrences)
#  and only if the period of CF is 2:
#   + rem_seq2: equation 'HH(k)=H(2*n)', giving new names and their meaning for a two by two sequence,
#   + rec2: recurence satisfied by HH(k), with rational coefficients,
#   + smallrec2: smaller order recurrence for HH(k).

`valuationincrease/rational` := proc( CF, listH, recHproved, H,
                                      names::list:=[default_HH,default_k], prooftable::table:=NULL, $)

   local cf, x, a, alpha, n, exprsa, period,# about cf
   procH, procH2, ordmax,
   recHprovedexplicit,
   recHproved2, recH_small,
   recHeven, recHodd,
   recHHeven, ini,
   tmp, i, HH, k,
   recHHeven_small;

   # getting the names in short form
   cf := op(1,CF);
   x, a, alpha, n := cf:-x, cf:-a, cf:-alpha, cf:-n;
   exprsa := cf:-exprsa;
   HH, k := op(names);

   procH:=proc(N, $) listH(N)[-1] end;
   ordmax:=2;

   # proving the valuation increase depending on the period of cf
   period := nops(cf:-exprsa);

   if period = 1 then

      recHprovedexplicit :=
         eval( recHproved,
               [a = unapply(exprsa[1],n), alpha=unnaply(cf:-asympt_alpha,n)] );
      tmp := `goodinitvalues/rec`( formatrec( [recHprovedexplicit,H(n)] ), H, n, {}, false );
      recHproved2 := { numer(recHprovedexplicit), seq( lhs(ini) = procH(op(1,lhs(ini))), ini in tmp) };

userinfo( 1, 'gfuncontfrac','NoName', sprintf("- reducing the recurrence order for %a...", H(n,x)));
      procH2 := rectoproc( recHproved2, H(n), remember , 'evalfun'=normal );#todo: normal?
      recH_small := reducerecorder( recHproved2, H, n, infinity, procH2, 'nmin'=6 );
      if recH_small = FAIL then
userinfo(0,'gfuncontfrac','NoName',"recurrence order could not be reduced.");
         return false;
      fi;

userinfo( 1, 'gfuncontfrac','NoName', sprintf("- ... done."), print(
         map(collect, subs(k=n,recH_small), [H,x], factor) ) );


      # the conclusion can now (hopefully) be read on the
      # coefficients valuations directly
      if not readvaluationincrease(recH_small, H, n, x, cf:-asympt_alpha) then return false fi;

userinfo(1,'gfuncontfrac','NoName',"QED.");

      if prooftable <> NULL then
         prooftable['small_rec'] := recH_small;
      end if;
      return true;

   elif period = 2 then

userinfo(1,'gfuncontfrac','NoName',sprintf("- computing a P-recurrence for %a using the rational formulas.",H(2*n,x)));

      # getting explicit recurrence relations
      # depending on the parity of n
      recHeven := numer(subs(
         [seq( subs(n=n+2*i, a(n)=exprsa[1]), i=0..ordmax),
          seq( subs(n=n+2*i+1, a(n)=exprsa[2]), i=0..ordmax)],
          recHproved));
      recHodd := numer(subs(
         [seq( subs(n=n+2*i, a(n)=exprsa[2]), i=0..ordmax),
          seq( subs(n=n+2*i+1, a(n)=exprsa[1]), i=0..ordmax)],
          recHproved));

userinfo(5,'gfuncontfrac','NoName',"debug: zeros only?",
               seq(normal(
                  eval( [subs(n=2*i,recHeven), subs(n=2*i+1,recHodd)],
                        H=procH)),
                   i=[1,2,3]));

      # combining them into a two by two recurrence, for HH(k) = H(2k).
      recHHeven := combinerecs2( x, recHeven, recHodd, H(n), HH(k) );

      # We will now focus only on H(2n)->0.
      #    (or H(2n+1)->0, depending on the offset),
      # this is enough to conclude because:
      #    the C-fraction converges to a series,
      #    and H_2n -> 0, hence cf_2n converges to the function, so does cf_n.

      tmp:=`goodinitvalues/rec`(formatrec([recHHeven,HH(k)]),HH,k,{},false);
      #goodiniteven:=map(proc(ini) lhs(ini)=eval(lhs(ini),HH=proc(k) procH(2*k) end) end, tmp);

      recHHeven := { recHHeven, seq(lhs(ini)=simplify(procH(2*op(1,lhs(ini)))), ini in tmp) };

userinfo( 1, 'gfuncontfrac','NoName', "  (does not conclude either)" );
userinfo( 1, 'gfuncontfrac','NoName', sprintf("- reducing equation for %a...", H(2*n,x)) );

      procH2 := rectoproc( recHHeven, HH(k), remember , ':-evalfun'=factor );

      recHHeven_small := collect(
         reducerecorder( recHHeven, HH, k, infinity, procH2, 'nmin'=6 ),
         [HH,x]);

      if recHHeven_small = FAIL then
userinfo(0,'gfuncontfrac','NoName',"recurrence order could not be reduced.");
         return false;
      fi;

userinfo( 1, 'gfuncontfrac','NoName', sprintf("- ... done."), print(
         map( collect, eval( subs(k=n,recHHeven_small), HH=proc(k) H(2*k) end),
                  [H,x], factor) ) );

      if not readvaluationincrease( recHHeven_small, HH, k, x, cf:-asympt_alpha ) then
         return false
      end if;

userinfo(1,'gfuncontfrac','NoName',"QED.");

      if prooftable <> NULL then
         prooftable['rem_seq2'] := HH(k) = H(2*k);
         prooftable['rec2'] := recHHeven;
         prooftable['smallrec2'] := recHHeven_small;
      end if;
      return true;

   end if;

end proc;#`valuationincrease/rational`


## given a linear second order equation with polynomial coefficients (in x)
# compute a riccati equation for the logarithmic derivative
#
# Input:
# - diffeq : homogeneous linear differential equation in y(x), of 2nd order
# - y(x): names for function and main variable
# - z: name for the logarithmic derivative
#   (involved in the produced differential equation)
#
# Output:
# - a Riccati differential equation in z(x)
# i.e. non linear, but linear in z', z, z^2, ..., with polynomial coefficients in x.

to_riccati_2ndorder:=proc(diffeq,yofx,z::name,$)
	local y,x,Y,X,ini, dey,ord;

	getname(yofx,y,x);
	dey:=formatdiffeq([diffeq,y(x)],Y,X,ini);
	if dey[1]<>0 then
userinfo(1,'gfuncontfrac','NoName',"non homogeneous equation");
      return FAIL;
   fi;
	ord:=nops(dey)-2;
	if ord <> 2 then
userinfo(1,'gfuncontfrac','NoName',"not 2nd order diffeq.");
      return FAIL;
   fi;
	return dey[4]*(diff(z(x),x)+z(x)^2)+dey[3]*z(x)+dey[2];
end;




# Input:
# - diffeq: a holonomic differential equation (with enough initial conditions)
# - yofx: names y(x) of the function and variable involved
#
# Output: a procedure procS with
# - procS(n,x) = y0(x) mod (x^n) with y0 the solution

diffeq_to_taylor:=proc( diffeq, yofx, $)
   local procc, c, n, procS;
   c, n := 'c', 'n';
   procc:=rectoproc( diffeqtorec(diffeq,yofx,c(n)),
      c(n), ':-remember' );
   procS:=proc(N::nonnegint,x::name,$) add(procc(n)*x^n,n=0..N) end;
   return procS;
end;


## Recurrence relation for the partial numerator (and denominator)
# of a C-fraction

recP := proc(P::name, i::name, x::name, a::name, alpha::name)
   P(i,x) = P(i-1,x) + a(i) * x^alpha(i) * P(i-2,x)
end;


## Evaluation of P and Q using the (same) recurrence formula valid for C-fractions:
# P(n,x)=P(n-1,x) + a_n x^alpha_n P(n-2,x)
# P is obtained using iniconds=[1,0] and Q with [0,1].
#
# Input:
# - x: main variable name
# - [proca, procalpha]: a couple of procedures n->a(n) (resp. alpha(n)) for n>=0.
# - N: a non negative integer
# - iniconds = [ini,ini2] (optional): initial conditions for value at -2 and -1.
# - evalfun: function applied after substitution with
#
# Output:
# - the list of N+1 first values for P(n,x).

get_listP:=proc( x::name, theprocs::list(procedure), N::nonnegint,
  {iniconds:=[1,0], evalfun:=normal}, $)
local P, P_table, i, j, valPj, a, alpha, proca, procalpha;

   # names and procedures
   P, j, a, alpha := 'P', 'j', 'a','alpha';
   proca, procalpha := op(theprocs);

   # recurrence unrolling
   P_table[-2],P_table[-1]:=op(iniconds);
   valPj := subs( recP(P,j,x,a,alpha), P(j,x) );
   for i from 0 to N do
      P_table[i]:=evalfun( eval( valPj,
                          [j=i, P=proc(j) P_table[j] end,
                           a=proca,
                           alpha=procalpha] ) );
   od:
   [seq(P_table[i],i=0..N)] end proc;

get_listQ:=proc(x::name, theprocs::list(procedure), N::nonnegint, {evalfun:=normal}, $)
   get_listP(x, theprocs, N, ':-iniconds'=[0,1], ':-evalfun'=evalfun);
end proc;

## Evaluation of [H_0, .., H_N] using procedures giving lists for P and Q.
get_listH:=proc(defH,P,Q,n,x,listP,listQ,N,{evalfun:=normal},$)
   local LP, LQ, i, res;
   LP:=listP(N);
   LQ:=listQ(N);
   res:=[seq(evalfun(eval(defH,[P(n,x)=LP[i],Q(n,x)=LQ[i]])),i=1..N+1)];
   return res;
end proc;


# List of rational expressions valid for a(n), from a certain index on
#
# Input:
# - n: index name
# - expr: expression in n
# - nbini: index, representing the start of the formula we are interested in
#
# Output:
# - a list of rational expressions for a(n) as described in `type/CFrac`
#      limitation: period 1 or 2 allowed only
# - FAIL if such expressions were not found
periodic_ratexprs := proc( n, expr, $)
   local e1, e2;
   # period 1
   if type(expr, ratpoly) then return [expr] fi;
   # period 2
   e1 := simplify(expr) assuming n::even;
   e2 := simplify(expr) assuming n::odd;
   if type([e1,e2],list(ratpoly)) then return [e1,e2] fi;
   return FAIL;
end proc;


end module:#ContFrac

unprotect('ContFrac:-contfrac_type'):
unprotect('ContFrac:-alpha_max'):
unprotect('ContFrac:-do_solve'):
