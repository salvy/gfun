# Heuristic function change, in order to get a Riccati equation
# see the details in related_riccati/atzero`.
#
# Input:
# - f: holonomic expression
# - y: function name
# - x_eqn: equation of the kind "x=2", giving the expansion point
# - y1: new function name
# - x1: new variable name
#
# output: a list, made of
# - x_of_x1: expression of x in function of the new variable x1.
# - y1_eqn: an equation y1(x1) = expr1 defining a new function y1(x1) - using y and x1 -
#     which may admit a C-fraction expansion formula.
# - deq1: a Riccati equation satisfied by y1(x1).
related_riccati := proc( expr, y, x_eqn :: `=`(name,{rational,identical(infinity)}),
                         y1, x1, $)
local x, v, x_of_x1, expr2, y2, deq2, y1_of_y, deq1;

   x,v := op(x_eqn);
   x_of_x1, expr2 := variable_change( expr, x_eqn, x1 );
   deq2 := holexprtodiffeq( expr2, y2(x1) );
   if deq2=FAIL then
userinfo(0,'gfuncontfrac','NoName',"No differential equation found for", expr2,".");
      return FAIL;
   fi;
   y1_of_y, deq1 := eval(
      `related_riccati/atzero`( deq2, y2(x1), y1 ),
      y2=proc(z) y(v+z) end );
   return [x_of_x1, y1(x1) = y1_of_y, deq1];

end proc;


# variable change (gives x_of_x1 in the previous function)
variable_change := proc( expr, x_eqn, x1 )
local x,v;

   x,v:=op(x_eqn);
   if v=infinity then return 1/x1, subs(x=1/x1,expr);
   else return v+x1, subs(x=v+x1,expr) end if;

end proc;

# Function change, at zero. (see above)

## Given a holonomic Laurent series (at zero), y(x),
# -> as a differential equation, possibly along with a series,
# propose a function and variable change y1(x),
# which heuristically has good chances of having a simple C-fraction expansion.
#
# It is based on Riccati equations,
# and usually gives f itself or its logarithmic	derivative,
# -> shifted if necessary:
#     the negative exponents are suppressed of the corresponding laurent series,
#     by multiplying with a power of x.
# typically: x^k * y(x) - P(x), with deg(P)<k.
#
# Input:
# - diffeq: a differential equation satisfied by y(x)
# - y(x):   names
# - y1: new function name
# - procS (optional): procedure mapping (N,x) onto the polynomial y(x) mod X^(N+1).
#
# Output: a couple
# - expression for y1(x) (in function of y(x))
# - differential equation satisfied by y1(x).
# or FAIL

`related_riccati/atzero` := proc( diffeq, yofx, y1::name, procS:=NULL, $)
local few,
   y, x, procSf,
   ord,
   u, val_u, DIFFEQ, PROCS, val_y1,
   deq, deq2,
   S, monomials, k;

   getname(yofx,y,x);
	deq := formatdiffeq([diffeq,y(x)]);
	ord := nops(deq)-2;

   # procedures for f and diff(f,x)/f
   if procS = NULL then
      procSf := diffeq_to_taylor(diffeq,y(x));
   else
      procSf := procS;
   fi;

   # switching to a Riccati equation, in u
	if ord = 1 then

      PROCS := procSf;
		val_u, DIFFEQ := y(x), subs(y=u, diffeq);

	elif ord = 2 then

      # taking the logarithmic derivative
      PROCS := proc(N,x) series(diff(procSf(N+1,x),x)/procSf(N,x),x=0,N) end;
      val_u, DIFFEQ := diff(y(x),x)/y(x), to_riccati_2ndorder(diffeq,yofx,u);
      if DIFFEQ = FAIL then return FAIL fi;

   fi;

   # multiplying by a power of x to get a Taylor series from this laurent series
   # heuristic: laurent series with offset <= few
   few := 10;
   S := PROCS(few,x);
   coeffs(convert(S,polynom),x,'monomials');

   # computing a minimal exponent for x
   k := min(map(degree,[monomials],x));
   if k <= -few then
userinfo(0,'gfuncontfrac','NoName',"Logarithmic derivative of valuation too small, not handled.");
      return FAIL;
   fi;

   # Factorizing with x^stg to get a power series
   if k<0 then
      # fixme: does not replace u in the initial conditions
      deq2 := PDETools['dchange'](
         u(x) = x^k * y1(x),
         DIFFEQ, [ y1(x) ] );
      val_y1 := x^(-k)*val_u;
   else
      deq2 := subs( u=y1, DIFFEQ );
      val_y1 := val_u;
   fi;

userinfo(4,'gfuncontfrac','NoName',"denom in modified equation:",denom(deq2));
   return val_y1, numer(deq2);

end proc;#`related_riccati/atzero`
