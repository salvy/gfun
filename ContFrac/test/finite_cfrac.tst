# mycfrac
restart; with(gfun): with(gfun:-ContFrac):
kernelopts(opaquemodules=false):
N:=12:
for f in [ tan(z), exp(z)-1, hypergeom([a,1],[b],z)-1 ] do
  de := ContFrac:-guessriccati(f,y,z,N):
  cf := ContFrac:-mycfrac( de, y, z ):
  pnum := map(normal,cf(N));
  cf2 := numtheory[cfrac](f,z,N+1,simregular,quotients);
  pnum2 := map(normal,map2(op,1, cf2[2..N+1]));
  TestTools[Try](1,map(normal,pnum-pnum2),[0$N]);
od:

# used to FAIL
with(gfun):
with(gfun:-ContFrac):
kernelopts(opaquemodules=false);
f := arcsin(z)/sqrt(-z^2+1);
procS := proc(n) MultiSeries:-series(f,z,n) end;
cf := ContFrac:-finite_cfrac( procS, z ):
cf(25);
