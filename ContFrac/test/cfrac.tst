# C-fractions guessing
# Continued fractions format as defined in ContFrac/cfrac.mm

procf[1] := proc(n) [1, seq( [x,1], i=1..n)] end proc:
procf[2] := proc(n) [2, [1,1], [x,1], seq( [x^4/i^2,1], i=3..n)] end proc:
procf[3] := proc(n)
   [3, seq([x,1],i=1..min(n,9)), seq( [x*(i+1)/(i)^2,1], i=10..n)]
end proc:
cf[1] := cfrac(Record(x = x, a = a, alpha = alpha, n = n,
                b0 = 1, init = [],
                reca = {-a(n)+a(n+1), a(0) = 1},
                exprsa = [1], asympt_alpha = 1)):
cf2[1] := cfrac(Record(x = x, a = a, alpha = alpha, n = n,
                b0 = 1, init = [],
                reca = {-a(n)+a(n+1), a(0) = 1},
                exprsa = [1], asympt_alpha = 1)):
cf[2] := cfrac(Record(x = x, a = a, alpha = alpha, n = n,
                b0 = 2, init = [1, x],
                reca = {n*(-n^3-n^2+n+1)*a(n)+n*(n^3+3*n^2-4)*a(n+1), a(2) = 1/9},
                exprsa = [1/(n+1)^2], asympt_alpha = 4)):
cf[3] := cfrac(Record(x = x, a = a, alpha = alpha, n = n,
                b0 = 3, init = [],
                reca = {-a(n)+a(n+1), a(0) = 1},
                exprsa = [1], asympt_alpha = 1)):
cf_aux[3] := cfrac(Record(x = x, a = a, alpha = alpha, n = n,
                    b0 = 3, init = [x, x, x, x, x, x, x, x, x,
                                    (11/100)*x, (12/121)*x, (13/144)*x],
                    reca = {(-n^4+38*n^3-539*n^2+3382*n-7920)*a(n)+
                            (n^4-38*n^3+539*n^2-3382*n+7920)*a(n+1), a(12) = 14/169},
                    exprsa = [14/169], asympt_alpha = 1)):
for i to 3 do
   output[i] := [ gfun:-ContFrac[guess_cfrac](
      procf[i], x, a, alpha, n) ];
   TestTools:-Try( i,
                   verify( output[i][2], cf[i], record ),
                   true );
od;

output_aux[3] := [ gfun:-ContFrac[guess_cfrac](
                  procf[3], x, a, alpha, n, ':-minnbused'=13) ];

TestTools:-Try(4,
               verify( output_aux[3][2], cf_aux[3], record ), true);

