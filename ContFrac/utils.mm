# Utilities for guessing equations, and expanding expressions as correpsonding continued fractions.

# Guessing a Riccati equation for y(z), using series at order N.
`guessriccati/simple` := proc(bigS, y::name, z::name, N::posint)
  local app,S,i;
  S:=map(simplify,eval(mytaylor(bigS,z,N),
                       O=proc(u) if not has(u,z) then O(1) else O(z^degree(u,z)) fi end));
  S:=map(mytaylor,[diff(S,z),1,S,S^2],z,N);
  app:=gfun:-pade2(S,z,N-1);

  diff(y(z),z) - subs(isolate(
    app[1]*diff(y(z),z)+add(app[i+2]*y(z)^i,i=0..2),
    diff(y(z),z) ), diff(y(z),z));
end:

# Guess a Riccati equation on the first terms of a series.
# small hack for the case where Pi occurs in the variables
# (could extend to any transcendantal constant)
guessriccati := proc(bigS, y::name, z::name, N::posint, nrat::posint:=5)
local app,aux,S,i,k,bli;
   S:=map(simplify,eval(mytaylor(bigS,z,N),
                        O=proc(u) if not has(u,z) then O(1) else O(z^degree(u,z)) fi end));
   S:=map( proc(t) map(simplify,mytaylor(t,z,N)) end, [diff(S,z),1,S,S^2] );

   if has(S,Pi) then
      for i to nrat do
         aux[i]:=gfun:-pade2(subs(Pi=i,S),z,N-1);
         # normalization
         for k to 4 do
            aux[i][k] := aux[i][k]/aux[i][1];
         od:
      od:
      app := subs( bli=Pi, [ 1, seq(
         CurveFitting['RationalInterpolation']( [seq([i,aux[i][k]],i=1..nrat)], 'bli' ),
         k = 2..4) ] );
   else
      app:=gfun:-pade2(S,z,N-1);
   fi;

  diff(y(z),z) - subs(isolate(
    app[1]*diff(y(z),z)+add(app[i+2]*y(z)^i,i=0..2),
    diff(y(z),z) ), diff(y(z),z));
end:

# guessing a difference equation
guessdifference := proc( bigT, T::name, s::name, N::posint, s2:=NULL )
local T1,cffs,app,aux,i;
   if s2=NULL then s2:=s+1 fi;
   T1:=map(simplify,eval(mytaylor(bigT,s,N+1),O=proc(u) if not has(u,s) then O         (1) else O(s^degree(u,s)) fi end));

   cffs:=[T(s)*T(s2),T(s),T(s2),1];

   aux:=map(mytaylor,
            eval(cffs, T=unapply(T1,s) ), s,N+1);
   app:=gfun:-pade2( aux, s, N);

   lprint("difference equation computed");

   add( app[i]*cffs[i], i=1..nops(cffs) );
end;

# Custom Taylor expansion
mytaylor:=proc( f, z::name, n::posint, {simple:=false}, $ ) local i,s,ord;
  s:=0:
  i:=0;
  while i<5 and degree(s,z)<n-1 do
    ord := (n+1)*2^i;
    if simple then
      s:=convert(series(f,z,ord),polynom);
    else
      s:=convert(MultiSeries:-series(f,z,ord),polynom);
    fi;
    ord:=2*ord;
    i := i+1;
  od:
  s;
end;


# Guess and prove a Corresponding Continued Fraction formula for an expression.
# It should satisfy a Riccati equation, which is heuristically guessed.
#
# Input:
# - expr: an expression depending on z,
# - y,z : names
# - N,Nseries: positive integers
# - guess_only (default false): boolean
# - names (optional, named): names for the continued fractions constituants (see riccati_to_cfrac in main.mm),
# - riccati (optional, named): Riccati equation satisfied by f,
# - display (optional, default true): pretty print the result on main output. # todo: remove this.
#
# Output:
# - a C-fraction formula for a Riccati equation satisfied up to order N by the series.
#
# Display: use infolevel[demo] to print information along the procedure, and
#   use infolevel[gfuncontfrac] for details on how the proof is performed.
#
# Description: the following steps are performed:
#   - compute a series for expr at z=0, at order Nseries;
#   - guess a Riccati equation for the function, true up to order N;
#   - compute a continued fraction expansion of the solution of the Riccati equation;
#   - prove the expansion is correct (except if option 'guess_only' is used)

expr_to_cfrac:=proc( expr, y::name, z::name,
  N::posint:=20, Nseries::posint:=32,
  guess_only::truefalse := false,
  {names :: list(name) := NULL, riccati := NULL}, $)
global st;
local
   n,a,alpha,P,Q,H,
   Riccati, optarg, bigS,
   Ser, myargs, cf;

   if names<>NULL then
      n,a,alpha,P,Q,H := op(names);
   end if;

   gfun:-Parameters('optionsgf'=[':-ogf']);

userinfo(1,'demo','NoName',"computing series...");
   bigS := MultiSeries:-series(expr,z,Nseries+2);
   if bigS=FAIL then return "arg, noseries"; fi;
userinfo(1,'demo','NoName',"... done.");

   if riccati = NULL then
userinfo(1,'demo','NoName',"computing a Riccati equation (using a guessing approach)...");
      Riccati := guessriccati(bigS,y,z,N);
userinfo(1,'demo','NoName',"... done.",
               print({Riccati, y(0)=eval(expr,z=0)}));
   else
      Riccati := riccati
   fi;

   Ser :=  proc(n,x) MultiSeries:-series(subs(z=x,expr),x,n+2) end;
   myargs := Riccati, y(z), Ser;

   optarg := ();
   if guess_only then optarg := ':-guess_only' fi;

userinfo(1,'demo','NoName',"computing a C-fraction expansion...");
   st:=time();
   cf := gfun:-ContFrac:-riccati_to_cfrac(
      myargs, [n,a,alpha,P,Q,H] );

   if cf=FAIL then return FAIL; fi;

userinfo(1,'demo','NoName',sprintf("... done. (in %a seconds)",time()-st));

   return cf;
end;


# riccati_to_series
# this is theoretically useful, although it seems too long in most cases.
# input: deq, y, z, N (a Riccati equation in y(z) and an order)
# output: the series solution s.t. y(0)=0.
# todo: correct this code.

# riccati_to_series := proc(deq,y,z,N)
#    for j from 2 to N do
#       e[j]:=1; # exponent
#       do
#          tmp := numer( expand( subs(y(Z)=c[j]*Z^e[j]/(1+Z),deq[j-1]) ) );
#          aux := collect( select(has,tmp,c[j]), Z );
#          val:=solve(tcoeff(aux,Z),c[j]);
#          if val=0 then e[j]:=e[j]+1; continue;
#          elif e[j]>emax then error "exponent too large."
#          else
#             c[j]:=val;
#             deq[j] := collect( numer(
#                subs( y(Z)=c[j]*Z^e[j]/(1+y(Z)), deq[j-1] ) ), Z,factor );
#             break fi;
#       od;
#    od:
# end proc;
